
jQuery(function ($) {

  jQuery('.ms-core-listMenu-verticalBox ul.root > li > a').each(function () {
    jChildren = jQuery(this).next('ul');
    if (jChildren.length != 0) {
      $(this).find("span.menu-item-text").prepend("<img src='https://meralco.sharepoint.com/sites/nccardev/networks/SiteAssets/Styles/Images/plusarrow.png' border='0' class='imgAlign' />");
    }
  });
  $($('.ms-core-listMenu-verticalBox ul.root > li > a').find("span.menu-item-text")[6]).prepend("<img src='https://meralco.sharepoint.com/sites/nccardev/networks/SiteAssets/Styles/Images/list.png' border='0' class='imgAlign' />");
  $($('.ms-core-listMenu-verticalBox ul.root > li > a').find("span.menu-item-text")[1]).prepend("<img src='https://meralco.sharepoint.com/sites/nccardev/networks/SiteAssets/Styles/Images/list.png' border='0' class='imgAlign' />");
   $($('.ms-core-listMenu-verticalBox ul.root > li > a').find("span.menu-item-text")[2]).prepend("<img src='https://meralco.sharepoint.com/sites/nccardev/networks/SiteAssets/Styles/Images/add.png' border='0' class='imgAlign' />");
  $($('.ms-core-listMenu-verticalBox ul.root > li > a').find("span.menu-item-text")[3]).prepend("<img src='https://meralco.sharepoint.com/sites/nccardev/networks/SiteAssets/Styles/Images/add.png' border='0' class='imgAlign' />");
  $($('.ms-core-listMenu-verticalBox ul.root > li > a').find("span.menu-item-text")[4]).prepend("<img src='https://meralco.sharepoint.com/sites/nccardev/networks/SiteAssets/Styles/Images/add.png' border='0' class='imgAlign' />");
  $($('.ms-core-listMenu-verticalBox ul.root > li > a').find("span.menu-item-text")[0]).prepend("<img src='https://meralco.sharepoint.com/sites/nccardev/networks/SiteAssets/Styles/Images/home.png' border='0' class='imgAlign' style='height: 9px;'/>");
  var jChildrens = jQuery('.ms-core-listMenu-verticalBox ul.root li.selected');
  //Expand Active Parent node  
   var selectedIndex = -1;
  for (var i = 0; i < jChildrens.length; i++) {
    var jChidlren = jQuery(jChildrens[i]);
    if (jChidlren.find('li.selected').length > 0) {
      selectedIndex = i;
      break;
    }
  }

  jQuery('.ms-core-listMenu-verticalBox ul.root > li > a').click(function (e) {
    jChildren = jQuery(this).next('ul');
    if (jChildren.length != 0) {
      //e.preventDefault();
      $(this).find("img.imgAlign").attr("src", "https://meralco.sharepoint.com/sites/nccardev/networks/SiteAssets/Styles/Images/plusarrow.png");
      //jChildrens.slideUp();
      if (jChildren.is(':visible') == false) {
        $(this).find("img.imgAlign").attr("src", "https://meralco.sharepoint.com/sites/nccardev/networks/SiteAssets/Styles/Images/minusarrow.png");
        //jChildren.slideDown();
         jChildrens.show();
      }
    }
  });

  $('#ctl00_PlaceHolderLeftNavBar_QuickLaunchNavigationManager').prepend('<div><a href="https://meralco.sharepoint.com/sites/nccardev/networks"><img data-themekey="#" src="https://meralco.sharepoint.com/sites/nccardev/networks/SiteAssets/Styles/Images/meco.png" style=" height: 61px; display: block; margin: 0 auto; padding: 30px 0 5px; "> <span style=" color: #3C2f2f; font-style: italic; font-size: 18px; display: block; text-align: center; padding-bottom: 9px; ">Networks NCCAR</span><a/></div>');


  disabledRibbon();

  function disabledRibbon() {

    var xmlhttp = new XMLHttpRequest();

    xmlhttp.open("GET", _spPageContextInfo.webAbsoluteUrl + '/_api/web/currentuser?$expand=Groups');

    xmlhttp.setRequestHeader("Accept", "application/json;odata=verbose");

    xmlhttp.onreadystatechange = function () {
      if (xmlhttp.readyState == XMLHttpRequest.DONE) {
        console.log("disabling ribbon function");
        if (xmlhttp.status == 200) {
          var response = xmlhttp.responseText;
          var jsonitem = JSON.parse(response);
          var isSiteAdmin = jsonitem["d"]["IsSiteAdmin"]
          var groups = jsonitem["d"]["Groups"]["results"]

          var isGroupAdmin = groups.some(function (g) {
            return g["Title"] == "Administrator";
          })


          if (isSiteAdmin || isGroupAdmin) {
            /*  $('li[id = "Ribbon.List.CustomViews"]').css("display", 'block');
             $('li[id = "Ribbon.List.Settings"]').css("display", 'block'); */
            $('head').append("<style> [id = 'Ribbon.List.Settings']{ display: block; } </style>");
            $($('.ms-core-listMenu-verticalBox ul.root > li > a')[7]).css('display','block')
          }
          else {
            console.log("Not Site Admin disabled ribbon function");
            $('head').append("<style> [id = 'Ribbon.List.Settings']{ display: none; }  </style>");
            $($('.ms-core-listMenu-verticalBox ul.root > li > a')[7]).css('display','none')
            /* $('li[id = "Ribbon.List.CustomViews"]').css("display", 'none');
            $('li[id = "Ribbon.List.Settings"]').css("display", 'none'); */

          }

        }
        else {
          console.log('Error: ' + xmlhttp.statusText)
        }
      }
    }

    xmlhttp.send();
  }
});


