SP.SOD.loadMultiple(['SP.js', 'SP.Runtime.js', 'clientform.js', 'clientpeoplepicker.js', 'autofill.js'], function () {});

var SharePoint;
(function (SharePoint) {

    var Utility;
    (function (Utility) {
        var currentContext;

        //Fabric Dropdown with source



        // People Picker actions
        function GetPeoplePicker(peoplePickerName) {
            var divPeoplePicker = $("div.sp-peoplepicker-topLevel[id^='" + peoplePickerName + "']");
            var editorPeoplePicker = divPeoplePicker.find("[id$='EditorInput']");
            var spPeoplePicker = SPClientPeoplePicker.SPClientPeoplePickerDict[divPeoplePicker[0].id];

            return {
                div: divPeoplePicker,
                editor: editorPeoplePicker,
                sp: spPeoplePicker
            };
        }

        // Assign a user on the people picker field
        var SetPeoplePickerUser = function (peoplePickerName, user) {
            var peoplePicker = GetPeoplePicker(peoplePickerName);

            peoplePicker.editor.val(user);
            peoplePicker.sp.AddUnresolvedUserFromEditor(true);
        };
        Utility.SetPeoplePickerUser = SetPeoplePickerUser;

        // Remove people picker's list of users
        var ClearPeoplePicker = function (peoplePickerName) {
            var peoplePicker = GetPeoplePicker(peoplePickerName);
            var resolvedUsers = $(document.getElementById(peoplePicker.sp.ResolvedListElementId)).find("span[class='sp-peoplepicker-userSpan']");

            if (resolvedUsers) {
                $(resolvedUsers).each(function (index) {
                    peoplePicker.sp.DeleteProcessedUser(this);
                });
            }
        };
        Utility.ClearPeoplePicker = ClearPeoplePicker;

        var GetPeoplePickerUserName = function (peoplePickerName) {
            var userDisplayName = "";
            var peoplePicker = GetPeoplePicker(peoplePickerName);
            var resolvedUsers = $(document.getElementById(peoplePicker.sp.ResolvedListElementId)).find("span[class='sp-peoplepicker-userSpan']");
            if (resolvedUsers.length != 0) {
                userDisplayName = resolvedUsers.find("[id$='UserDisplay']")[0].textContent;
            }
            return userDisplayName;
        };
        Utility.GetPeoplePickerUserName = GetPeoplePickerUserName;

        // Assign the current user on the people picker field
        var SetPeoplePickerCurrentUser = function (peoplePickerName) {
            SetPeoplePickerUser(peoplePickerName, _spPageContextInfo.userDisplayName);
        };
        Utility.SetPeoplePickerCurrentUser = SetPeoplePickerCurrentUser;

        // Disable PeoplePicker
        var DisablePeoplePicker = function (peoplePickerName) {
            $("input.sp-peoplepicker-editorInput[id^='" + peoplePickerName + "']").prop('disabled', true);
            $("a.sp-peoplepicker-delImage[id^='" + peoplePickerName + "']").hide();
            $("div.sp-peoplepicker-topLevel[id^='" + peoplePickerName + "']").addClass("sp-peoplepicker-topLevelDisabled");
            $("span.ms-entity-resolved[id^='" + peoplePickerName + "']").css('cursor', 'text');
        }
        Utility.DisablePeoplePicker = DisablePeoplePicker;

        // Enable PeoplePicker
        var EnablePeoplePicker = function (peoplePickerName) {
            $("input.sp-peoplepicker-editorInput[id^='" + peoplePickerName + "']").prop('disabled', false);
            $("a.sp-peoplepicker-delImage[id^='" + peoplePickerName + "']").show();
            $("div.sp-peoplepicker-topLevel[id^='" + peoplePickerName + "']").removeClass("sp-peoplepicker-topLevelDisabled");
            $("span.ms-entity-resolved[id^='" + peoplePickerName + "']").css('cursor', 'pointer');
        }
        Utility.EnablePeoplePicker = EnablePeoplePicker;
        // End of People Picker actions

        // Command buttons
        var AddButtonBesideSave = function (buttonInfo) {
            $($("input[id$='diidIOSaveItem']")[1]).after("<input type='button' value='" + buttonInfo.label + "' id='" + buttonInfo.id + "'>");

            // add the onclick event
            $("input#" + buttonInfo.id).on('click', buttonInfo.Action);
        };
        Utility.AddButtonBesideSave = AddButtonBesideSave;

        // Change the value of select option
        var ChangeSelectOption = function (selectField, optionValue) {
            $("select[id^='" + selectField + "']").val(optionValue).change();
        };
        Utility.ChangeSelectOption = ChangeSelectOption;

        // Set the select option value before saving the form
        var ChangeSelectOptionThenSave = function (selectField, optionValue) {
            ChangeSelectOption(selectField, optionValue);
            $($("input[id$='diidIOSaveItem']")[1]).click();
        };
        Utility.ChangeSelectOptionThenSave = ChangeSelectOptionThenSave;

        // Hide the main save button
        var HideMainButton = function (buttonId) {
            $($("input[id$='" + buttonId + "']")[1]).hide();
        };
        Utility.HideMainButton = HideMainButton;

        var ShowMainButton = function (buttonId) {
            $($("input[id$='" + buttonId + "']")[1]).show();
        };
        Utility.ShowMainButton = ShowMainButton;
        // End of Command buttons 

        // Hide button in ribbon
        var HideRibbonButton = function (buttonId) {
            document.getElementById(buttonId).style.display = 'none';
        }
        Utility.HideRibbonButton = HideMainButton;
        // End of Ribbon buttons

        // Field properties/attributes
        // Set field's disable property based on status (true | false)
        var SetFieldDisable = function (fieldInfo) {
            $(fieldInfo.type + "[id^='" + fieldInfo.id + "']").prop("disabled", fieldInfo.status);
        };
        Utility.SetFieldDisable = SetFieldDisable;

        // Set Rich Text field's disable property based on status (true | false)
        var SetRichTextFieldDisable = function (fieldInfo) {
            if (fieldInfo.status) {
                $($("[id^='" + fieldInfo.id + "'][id$=inplacerte]")[0]).attr("contenteditable", "false");
                $($("div[id^='" + fieldInfo.id + "'][id$=TextField_topDiv]")[0]).addClass(" ms-inputBoxDisabled");
            } else {
                $($("[id^='" + fieldInfo.id + "'][id$=inplacerte]")[0]).attr("contenteditable", "true");
                $($("div[id^='" + fieldInfo.id + "'][id$=TextField_topDiv]")[0]).removeClass(" ms-inputBoxDisabled");
            }
        };
        Utility.SetRichTextFieldDisable = SetRichTextFieldDisable;

        // Hide or Show Field
        var SetFieldVisibility = function (fieldInfo) {
            if (fieldInfo.isVisible) {
                $(fieldInfo.type + "[id^='" + fieldInfo.id + "']").closest("tr").show();
            } else {
                $(fieldInfo.type + "[id^='" + fieldInfo.id + "']").closest("tr").hide();
            }
        };
        Utility.SetFieldVisibility = SetFieldVisibility;
        // End of Field properties/attributes

        //Query string parameters
        var GetQueryString = function (key) {
            var regex = new RegExp('[\\?&amp;]' + key + '=([^&amp;#]*)');
            var qs = regex.exec(window.location.href);
            return qs[1];
        }
		
        var GetParameterByName = function (name) {
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        }
        Utility.GetParameterByName = GetParameterByName;

        // modal options: 
        var OpenDialog = function (modal) {
            $("#" + modal.attachTo).after("<div id='modalDiv' style='display:none' ><div id='modalBody'>" + modal.message + "</div></div>");

            var modalBody = document.getElementById('modalBody');
            var options = {
                title: modal.title,
                width: modal.width,
                height: modal.height,
                dialogReturnValueCallback: modal.CallbackAction,
                html: modalBody.cloneNode(true)
            };

            SP.UI.ModalDialog.showModalDialog(options);

            $("input#" + modal.cancelButtonId).on("click", modal.CancelAction);
            $("input#" + modal.okButtonId).on("click", modal.OkAction);
        };
        Utility.OpenDialog = OpenDialog;

        
        // SharePoint group manipulation
        var IsCurrentUserMemberOfGroup = function (groupName, CallbackAction) {
            currentContext = new SP.ClientContext.get_current();
            var currentWeb = currentContext.get_web();

            var currentUser = currentContext.get_web().get_currentUser();
            currentContext.load(currentUser);

            var allGroups = currentWeb.get_siteGroups();
            currentContext.load(allGroups);

            var group = allGroups.getByName(groupName);
            currentContext.load(group);

            var groupUsers = group.get_users();
            currentContext.load(groupUsers);

            var OnSuccess = function (sender, args) {
                var userInGroup = false;
                var groupUserEnumerator = groupUsers.getEnumerator();
                while (groupUserEnumerator.moveNext()) {
                    var groupUser = groupUserEnumerator.get_current();
                    if (groupUser.get_id() == currentUser.get_id()) {
                        userInGroup = true;
                        break;
                    }
                }
                CallbackAction(userInGroup, groupName);
            }

            var OnFailure = function (sender, args) {
                console.log('Request failed. ' + args.get_message() +
                    '\n' + args.get_stackTrace());
            }

            currentContext.executeQueryAsync(OnSuccess, OnFailure);
        }
        Utility.IsCurrentUserMemberOfGroup = IsCurrentUserMemberOfGroup;

        var GetCurrentUserGroups = function (CallbackAction) {
            currentContext = new SP.ClientContext.get_current();
            var oWeb = currentContext.get_web();
            var currentUser = oWeb.get_currentUser();
            var allGroups = currentUser.get_groups();
            currentContext.load(allGroups);

            currentContext.executeQueryAsync(OnSuccess, OnFailure);

            function OnSuccess(sender, args) {
                var groups = [];
                var grpsEnumerator = allGroups.getEnumerator();
                while (grpsEnumerator.moveNext()) {
                    //This object will have the group instance
                    //which the current user is part of. 
                    //This is can be processed for further 
                    //operations or business logic.
                    var group = grpsEnumerator.get_current();

                    //This gets the title of each group
                    //while traversing through the enumerator.			
                    groups.push(group.get_title());
                }
                CallbackAction(groups);
            }

            function OnFailure(sender, args) {
                console.log(args.get_message());
            }

        }
        Utility.GetCurrentUserGroups = GetCurrentUserGroups;

        // SharePoint List manipulation
        // Archive Selected Items
        var MoveListItemsTo = function (ListTitle,actionName) {
            GetSelectedItems(function (listItems) {
                GetFields(function (fields, types) {
                    if (listItems.length === 0) {
                        alert("No items selected!");
                    } else {
                        if (confirm('Are you sure to '+actionName.toLowerCase()+' the selected items?')) {
                            for (var i = 0; i < listItems.length; i++) {
                                var currentItem = listItems[i];
                                var create = {
                                    ListTitle: ListTitle,
                                    Columns: {}
                                }
                                for (var j = 0; j < fields.length; j++) {
                                    create.Columns[fields[j]] = currentItem.get_item(fields[j]);
                                }
                                //console.log(create);
                                Utility.CreateListItem(create);
                                Utility.DeleteListItem({
                                    ListTitle: ctx.ListTitle,
                                    Id: currentItem.get_item('ID')
                                });
                            }
                            var statusId = SP.UI.Status.addStatus("Successfully "+actionName+"d!");
                            SP.UI.Status.setStatusPriColor(statusId, 'green');
                            setTimeout(function () {
                                location.reload();
                            }, 1500);
                        }
                    }
                }, ctx.ListTitle);
            });
        }
        SharePoint.Utility.MoveListItemsTo = MoveListItemsTo;

        var GetFields = function (CallbackAction, ListTitle) {
            var RestrictedFields = ["Attachments", "ContentTypeId", "_ModerationComments", "LinkTitleNoMenu", "LinkTitle", "LinkTitle2", "File_x0020_Type", "ComplianceAssetId",
                "ID", "ContentType", "_HasCopyDestinations", "_CopySource", "owshiddenversion", "WorkflowVersion", "_UIVersion", "_UIVersionString",
                "_ModerationStatus", "Edit", "SelectTitle", "InstanceID", "Order", "GUID", "WorkflowInstanceID", "FileRef", "FileDirRef", "FSObjType",
                "SortBehavior", "PermMask", "FileLeafRef", "UniqueId", "SyncClientId", "ProgId", "ScopeId", "HTML_x0020_File_x0020_Type", "_EditMenuTableStart",
                "_EditMenuTableStart2", "_EditMenuTableEnd", "LinkFilenameNoMenu", "LinkFilename", "LinkFilename2", "DocIcon", "ServerUrl", "EncodedAbsUrl",
                "BaseName", "MetaInfo", "_Level", "_IsCurrentVersion", "ItemChildCount", "FolderChildCount", "Restricted", "OriginatorId", "NoExecute",
                "ContentVersion", "_ComplianceFlags", "_ComplianceTag", "_ComplianceTagWrittenTime", "_ComplianceTagUserId", "_IsRecord", "AccessPolicy",
                "_VirusStatus", "_VirusVendorID", "_VirusInfo", "AppAuthor", "AppEditor", "SMTotalSize", "SMLastModifiedDate", "SMTotalFileStreamSize", "SMTotalFileCount",
                "Created_x0020_Date", "Last_x0020_Modified"
            ];
            currentContext = new SP.ClientContext.get_current();
            var list = currentContext.get_web().get_lists().getByTitle(ListTitle);
            var listFields = list.get_fields();
            currentContext.load(listFields);
            currentContext.executeQueryAsync(function () {
                var fieldEnumerator = listFields.getEnumerator();
                var fields = [],
                    types = [];
                while (fieldEnumerator.moveNext()) {
                    var oField = fieldEnumerator.get_current();
                    var fType = oField.get_fieldTypeKind();
                    if (RestrictedFields.indexOf(oField.get_internalName()) == -1) {
                        fields.push(oField.get_internalName());
                        types.push(fType);
                    }
                }
                CallbackAction(fields, types);
            }, function (sender, args) {
                console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
            });
        }
        Utility.GetFields = GetFields;

        // Get selected items in list
        var GetSelectedItems = function (CallbackAction) {
            currentContext = new SP.ClientContext.get_current();
            var listId = SP.ListOperation.Selection.getSelectedList();
            var selectedItemIds = SP.ListOperation.Selection.getSelectedItems(currentContext);
            var list = currentContext.get_web().get_lists().getById(listId);
            var listItems = [];

            for (idx in selectedItemIds) {
                var item = list.getItemById(parseInt(selectedItemIds[idx].id));
                listItems.push(item);
                currentContext.load(item);
            }

            currentContext.executeQueryAsync(function (sender, args) {
                CallbackAction(listItems);
            }, function (sender, args) {
                console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
            });
        }
        Utility.GetSelectedItems = GetSelectedItems;

        
          //Upadate List Item by id
          var UpdateListItem = function (ItemInfo) {
            currentContext = new SP.ClientContext.get_current();
            var list = currentContext.get_web().get_lists().getByTitle(ItemInfo.ListTitle);
            var listItem = list.getItemById(ItemInfo.Id);
            for (var column in ItemInfo.Columns) {
                listItem.set_item(column, ItemInfo.Columns[column]);
            }
            listItem.update();
            currentContext.load(listItem);
            currentContext.executeQueryAsync(function () {
                //console.log('Item Updated.');
            }, function (sender, args) {
                console.log('Request failed. ' + args.get_message() +
                    '\n' + args.get_stackTrace());
            });
        }
        Utility.UpdateListItem = UpdateListItem;

         //Create List Item By GUID
         var CreateListItemById = function (ItemInfo) {
            currentContext = new SP.ClientContext.get_current();
            
            var list = currentContext.get_web().get_lists().getById(ItemInfo.ListTitle);
            var itemCreateInfo = new SP.ListItemCreationInformation();
            var listItem = list.addItem(itemCreateInfo);
            for (var column in ItemInfo.Columns) {
                listItem.set_item(column, ItemInfo.Columns[column]);
            }
            listItem.update();
            currentContext.load(listItem);
            currentContext.executeQueryAsync(function () {
                console.log('Item Created.');
                
            }, function (sender, args) {
                console.log('Request failed. ' + args.get_message() +
                    '\n' + args.get_stackTrace());
            });
        }
        Utility.CreateListItemById = CreateListItemById;

        //Create List Item
        var CreateListItem = function (ItemInfo) {
            currentContext = new SP.ClientContext.get_current();
            var list = currentContext.get_web().get_lists().getByTitle(ItemInfo.ListTitle);
            var itemCreateInfo = new SP.ListItemCreationInformation();
            var listItem = list.addItem(itemCreateInfo);
            for (var column in ItemInfo.Columns) {
                listItem.set_item(column, ItemInfo.Columns[column]);
            }
            listItem.update();
            currentContext.load(listItem);
            currentContext.executeQueryAsync(function () {
                //console.log('Item Created.');
            }, function (sender, args) {
                console.log('Request failed. ' + args.get_message() +
                    '\n' + args.get_stackTrace());
            });
        }
        Utility.CreateListItem = CreateListItem;

         // Retrieve List Items By GUID
         var RetrieveListItemsById = function (ListTitle, ViewQuery, CallbackAction) {
            currentContext = new SP.ClientContext.get_current();
            var list = currentContext.get_web().get_lists().getById(ListTitle);
            var camlQuery = new SP.CamlQuery();
            camlQuery.set_viewXml(ViewQuery);
            var listItems = list.getItems(camlQuery);

            currentContext.load(listItems);

            currentContext.executeQueryAsync(function (sender, args) {
                var listItemEnumerator = listItems.getEnumerator();
                CallbackAction(listItemEnumerator);
            }, function (sender, args) {
                console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
            });
        }
        Utility.RetrieveListItemsById = RetrieveListItemsById;

        // Retrieve List Items
        var RetrieveListItems = function (ListTitle, ViewQuery, CallbackAction) {
            currentContext = new SP.ClientContext.get_current();
            var list = currentContext.get_web().get_lists().getByTitle(ListTitle);

            var camlQuery = new SP.CamlQuery();
            camlQuery.set_viewXml(ViewQuery);
            var listItems = list.getItems(camlQuery);

            currentContext.load(listItems);

            currentContext.executeQueryAsync(function (sender, args) {
                var listItemEnumerator = listItems.getEnumerator();
                CallbackAction(listItemEnumerator);
            }, function (sender, args) {
                console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
            });
        }
        Utility.RetrieveListItems = RetrieveListItems;

        //Upadate List Item
        var UpdateListItem = function (ItemInfo) {
            currentContext = new SP.ClientContext.get_current();
            var list = currentContext.get_web().get_lists().getByTitle(ItemInfo.ListTitle);
            var listItem = list.getItemById(ItemInfo.Id);
            for (var column in ItemInfo.Columns) {
                listItem.set_item(column, ItemInfo.Columns[column]);
            }
            listItem.update();
            currentContext.load(listItem);
            currentContext.executeQueryAsync(function () {
                //console.log('Item Updated.');
            }, function (sender, args) {
                console.log('Request failed. ' + args.get_message() +
                    '\n' + args.get_stackTrace());
            });
        }
        Utility.UpdateListItem = UpdateListItem;

        //Delete List Item
        var DeleteListItem = function (ItemInfo) {
            currentContext = new SP.ClientContext.get_current();
            var list = currentContext.get_web().get_lists().getByTitle(ItemInfo.ListTitle);
            var listItem = list.getItemById(ItemInfo.Id);
            listItem.deleteObject();
            currentContext.executeQueryAsync(function () {
                //console.log('Item Updated.');
            }, function (sender, args) {
                console.log('Request failed. ' + args.get_message() +
                    '\n' + args.get_stackTrace());
            });
        }
        Utility.DeleteListItem = DeleteListItem;

        // Upload file to library
        var UploadFile = function (CallbackAction, FileInfo) {
            DisplayOverlayLoader(true);
            ExecuteHideRibbon();
            // Define the folder path for this example.
            var serverRelativeUrlToFolder = FileInfo.Path;

            // Get test values from the file input and text input page controls.
            var fileInput = FileInfo.File;
            var fileCount = fileInput[0].files.length;
            // Get the server URL.
            var serverUrl = _spPageContextInfo.webAbsoluteUrl;
            var filesUploaded = 0;
            for (var i = 0; i < fileCount; i++) {
                // Initiate method calls using jQuery promises.
                // Get the local file as an array buffer.
                var getFile = getFileBuffer(i);
                getFile.done(function (arrayBuffer, i) {

                    // Add the file to the SharePoint folder.
                    var addFile = addFileToFolder(arrayBuffer, i);
                    addFile.done(function (file, status, xhr) {
                        filesUploaded++;
                        if (fileCount == filesUploaded) {
                            CallbackAction();
                            DisplayOverlayLoader(false);
                            file.value = null;
                            filesUploaded = 0;
                        }
                    });
                    addFile.fail(onError);
                });
                getFile.fail(onError);

            }

            // Get the local file as an array buffer.
            function getFileBuffer(i) {
                var deferred = jQuery.Deferred();
                var reader = new FileReader();
                reader.onloadend = function (e) {
                    deferred.resolve(e.target.result, i);
                }
                reader.onerror = function (e) {
                    deferred.reject(e.target.error);
                }
                reader.readAsArrayBuffer(fileInput[0].files[i]);
                return deferred.promise();
            }

            // Add the file to the file collection in the Shared Documents folder.
            function addFileToFolder(arrayBuffer, i) {
                var index = i;

                // Get the file name from the file input control on the page.
                var fileName = fileInput[0].files[index].name;

                // Construct the endpoint.
                var fileCollectionEndpoint = String.format(
                    "{0}/_api/web/getfolderbyserverrelativeurl('{1}')/files" +
                    "/add(overwrite=true, url='{2}')",
                    serverUrl, serverRelativeUrlToFolder, fileName);

                // Send the request and return the response.
                // This call returns the SharePoint file.
                return jQuery.ajax({
                    url: fileCollectionEndpoint,
                    type: "POST",
                    data: arrayBuffer,
                    processData: false,
                    headers: {
                        "accept": "application/json;odata=verbose",
                        "X-RequestDigest": jQuery("#__REQUESTDIGEST").val()
                    }
                });
            }

            // Display error messages. 
            function onError(error) {
                $("#overlay").fadeOut();
                $("#loader").fadeOut();
                alert("Error: " + error.responseText);
            }
        }
        Utility.UploadFile = UploadFile;
	

        // Delete file in Library
        var DeleteFile = function (fileInfo) {
            var oWebsite;
            var fileUrl;
            currentContext = new SP.ClientContext.get_current();
            oWebsite = currentContext.get_web();
            currentContext.load(oWebsite);
            currentContext.executeQueryAsync(function () {
                fileUrl = oWebsite.get_serverRelativeUrl() +
                    fileInfo.Path + fileInfo.File;
                this.fileToDelete = oWebsite.getFileByServerRelativeUrl(fileUrl);
                this.fileToDelete.deleteObject();
                currentContext.executeQueryAsync(function () {
                    //console.log(fileInfo.File + " has been deleted.")
                }, function (sender, args) {
                    console.log('Request failed. ' + args.get_message() +
                        '\n' + args.get_stackTrace());
                });
            }, function (sender, args) {
                console.log('Request failed. ' + args.get_message() +
                    '\n' + args.get_stackTrace());
            });
        }
        Utility.DeleteFile = DeleteFile;

        // Initialize hide ribbon
        function hideRibbon() {
            try {
                var ribbon = SP.Ribbon.PageManager.get_instance().get_ribbon();
                // Set the tab to the "Browse" tab
                SelectRibbonTab("Ribbon.Read", true);
                // Remove the "Edit" tab from a list from from the ribbon.
            } catch (ex) {
                alert(ex.message);
            }
        }

        // Execute hide ribbon
        var ExecuteHideRibbon = function () {
            SP.SOD.executeOrDelayUntilScriptLoaded(function () {
                var pm = SP.Ribbon.PageManager.get_instance();
                pm.add_ribbonInited(function () {
                    hideRibbon();
                });
                var ribbon = null;
                try {
                    ribbon = pm.get_ribbon();
                } catch (e) {}

                if (!ribbon) {
                    if (typeof (_ribbonStartInit) == "function")
                        _ribbonStartInit(_ribbon.initialTabId, false, null);
                } else {
                    hideRibbon();
                }
            }, "sp.ribbon.js");
        }
        Utility.ExecuteHideRibbon = ExecuteHideRibbon;

        var SearchSPGroup = function (spGroups, key) {

            var obj = spGroups.filter(function (obj) {
                return obj.spGroupName === key
            })[0];

            if (obj) {
                return obj.isMember;
            } else {
                return false;
            }

        };
        Utility.SearchSPGroup = SearchSPGroup;

        // Add field validation message
        
        var AddFieldValidation = function (validationInfo) {
           
            if(validationInfo.type.toUpperCase() == "PEOPLEPICKER"){
                $("span#error" + validationInfo.errorType + validationInfo.fieldID).remove();
                $(validationInfo.type + "[id^='" + validationInfo.fieldID + "']").next().remove();
                $("div[id ='" + validationInfo.fieldID + "']").after('<span id="error' + validationInfo.errorType + validationInfo.fieldID + '" class="ms-formvalidation ms-csrformvalidation" style="display: block;"><span role="alert">' + validationInfo.message + '</span></span>');    
                document.getElementById(validationInfo.fieldID + "_TopSpan_EditorInput").focus();
            }
            else if(validationInfo.type.toUpperCase() == "INPUT"){
                $($("span#error" + validationInfo.errorType + validationInfo.fieldID)[validationInfo.index]).remove();
                //$($(validationInfo.type + "[id^='" + validationInfo.fieldID + "']")[validationInfo.index]).next().remove();
                $($(validationInfo.type + "[id^='" + validationInfo.fieldID + "']")[validationInfo.index]).parent('div').after('<span id="error' + validationInfo.errorType + validationInfo.fieldID + '" class="ms-formvalidation ms-csrformvalidation" style="display: block;"><span role="alert">' + validationInfo.message + '</span></span>');    
                document.getElementById(validationInfo.fieldID).focus();
            }
            else if(validationInfo.type.toUpperCase() == "TEXTAREA"){
                $($("span#error" + validationInfo.errorType + validationInfo.fieldID)[validationInfo.index]).remove();
                $($(validationInfo.type + "[id^='" + validationInfo.fieldID + "']")[validationInfo.index]).next().remove();
                $($(validationInfo.type + "[id^='" + validationInfo.fieldID + "']")[validationInfo.index]).parent('div').after('<span id="error' + validationInfo.errorType + validationInfo.fieldID + '" class="ms-formvalidation ms-csrformvalidation" style="display: block;"><span role="alert">' + validationInfo.message + '</span></span>');    
                document.getElementById(validationInfo.fieldID).focus();
            }
            else if(validationInfo.type.toUpperCase() == "DROPDOWN"){
                $("span#error" + validationInfo.errorType + validationInfo.fieldID).remove();
                $(validationInfo.type + "[id^='" + validationInfo.fieldID + "']").next().remove();
                $("div[id ='" + validationInfo.fieldID + "']").after('<span id="error' + validationInfo.errorType + validationInfo.fieldID + '" class="ms-formvalidation ms-csrformvalidation" style="display: block;"><span role="alert">' + validationInfo.message + '</span></span>');    
                document.getElementById(validationInfo.fieldID).focus();
            }
            else if(validationInfo.type.toUpperCase() == "DATEPICKER"){
                $("span#error" + validationInfo.errorType + validationInfo.fieldID).remove();
                $(validationInfo.type + "[id^='" + validationInfo.fieldID + "']").next().remove();
                //$("div[id ='" + validationInfo.fieldID + "']").after('<span id="error' + validationInfo.errorType + validationInfo.fieldID + '" class="ms-formvalidation ms-csrformvalidation" style="display: block;"><span role="alert">' + validationInfo.message + '</span></span>');    
                $("div[id ='" + validationInfo.fieldID + "']").find('div[class*="is-required"]').after('<span id="error' + validationInfo.errorType + validationInfo.fieldID + '" class="ms-formvalidation ms-csrformvalidation" style="display: block;"><span role="alert">' + validationInfo.message + '</span></span>');    
                document.getElementById(validationInfo.fieldID).focus();
            }
            else if(validationInfo.type.toUpperCase() == "ATTACHMENT"){
                $($("span#error" + validationInfo.errorType + validationInfo.fieldID)[validationInfo.index]).remove();
                //$($("input[id^='" + validationInfo.fieldID + "']")[validationInfo.index]).next('span').remove();
                $($("input[id ='" + validationInfo.fieldID + "']")[validationInfo.index]).next('div').after('<span id="error' + validationInfo.errorType + validationInfo.fieldID + '" class="ms-formvalidation ms-csrformvalidation" style="display: block;"><span role="alert">' + validationInfo.message + '</span></span>');        
                document.getElementById(validationInfo.fieldID).focus();
            }
        
        };
        Utility.AddFieldValidation = AddFieldValidation;

        // Validate Fields
        var Validate = function (validationInfo) {
            $('span[id*="error"]').remove();
            var error = false;
            for (var field in validationInfo.Fields) {
                // /$("span[id^='error'][id$='" + field + "']").remove();
                for (var rules in validationInfo.Fields[field]) {
                    switch (rules) {
                        case 'required':
                            if (validationInfo.Fields[field][rules]) {
                                if(validationInfo.FieldType[field].type.toUpperCase() == "INPUT" || validationInfo.FieldType[field].type.toUpperCase() == "TEXTAREA"){
                                    var inputs = $(validationInfo.FieldType[field].type.toUpperCase() + "[id^='" + field + "']");
                                    for(var i=0; i < inputs.length; i++){
                                        if ($($(validationInfo.FieldType[field].type.toUpperCase() + "[id^='" + field + "']")[i]).val().trim().length == 0) {
                                            AddFieldValidation({
                                                index: i,
                                                type: validationInfo.FieldType[field].type,
                                                fieldID: field,
                                                errorType: 'required',
                                                message: validationInfo.message[field].required
                                            });
                                            error = true;
                                        }
                                    }
                                    
                                }
                                else if(validationInfo.FieldType[field].type.toUpperCase() == "ATTACHMENT"){
                                    var inputs = $("input[id^='" + field + "']");
                                    for(var i=0; i < inputs.length; i++){
                                        if($($(inputs)[i]).siblings('div').find('div > div').length == 0){
                                        //if($($(inputs)[i]).next('div').length == 0 ){
                                            AddFieldValidation({
                                                index: i,
                                                type: validationInfo.FieldType[field].type,
                                                fieldID: field,
                                                errorType: 'required',
                                                message: validationInfo.message[field].required
                                            });
                                            error = true;
                                        }
                                    }
                                }
                                else if(validationInfo.FieldType[field].type.toUpperCase() == "PEOPLEPICKER"){
                                    if($('#'+field).find('input[id *= "_TopSpan_HiddenInput"]').val().replace('[]', '').trim().length == 0){
                                       
                                        AddFieldValidation({
                                            type: validationInfo.FieldType[field].type,
                                            fieldID: field,
                                            errorType: 'required',
                                            message: validationInfo.message[field].required
                                        });
                                        error = true;
                                    }
                                }
                                else if(validationInfo.FieldType[field].type.toUpperCase() == "DROPDOWN"){
                                    if($('#'+ field).find('ul > li.is-selected').index() <= 0){
                                        AddFieldValidation({
                                            type: validationInfo.FieldType[field].type,
                                            fieldID: field,
                                            errorType: 'required',
                                            message: validationInfo.message[field].required
                                        });
                                        error = true;
                                    } 
                                }
                                else if(validationInfo.FieldType[field].type.toUpperCase() == "DATEPICKER"){
                                    if($('#'+field).find('input').val().trim().length == 0){
                                        AddFieldValidation({
                                            type: validationInfo.FieldType[field].type,
                                            fieldID: field,
                                            errorType: 'required',
                                            message: validationInfo.message[field].required
                                        });
                                        error = true;
                                    }
                                }
                            }
                            break;
                        case 'minlength':
                            if ($("input[id^='" + field + "']").val().trim().length > validationInfo.Fields[field][rules]) {
                                AddFieldValidation({
                                    type: "input",
                                    fieldID: field,
                                    errorType: 'minlength',
                                    message: validationInfo.message[field].minlength
                                });
                                error = true;
                            }
                            break;
                        case 'maxlength':
                            if ($("input[id^='" + field + "']").val().trim().length < validationInfo.Fields[field][rules]) {
                                AddFieldValidation({
                                    type: "input",
                                    fieldID: field,
                                    errorType: 'maxlength',
                                    message: validationInfo.message[field].maxlength
                                });
                                error = true;
                            }
                            break;
                        case 'email':
                            if (validationInfo.Fields[field][rules]) {
                                var regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                                if (!regex.test($("input[id^='" + field + "']").val())) {
                                    AddFieldValidation({
                                        type: "input",
                                        fieldID: field,
                                        errorType: 'email',
                                        message: validationInfo.message[field].email
                                    });
                                    error = true;
                                }
                            }
                            break;
                        case 'equalTo':
                            if ($("input[id^='" + field + "']").val().trim() != validationInfo.Fields[field][rules]) {
                                AddFieldValidation({
                                    type: "input",
                                    fieldID: field,
                                    errorType: 'equalTo',
                                    message: validationInfo.message[field].equalTo
                                });
                                error = true;
                            }
                            break;
                        case 'greaterThanDate':
                            if (new Date($("input[id^='" + field + "']").val()) > validationInfo.Fields[field][rules]) {
                                AddFieldValidation({
                                    type: "table",
                                    fieldID: field,
                                    errorType: 'greaterThanDate',
                                    message: validationInfo.message[field].greaterThanDate
                                });
                                error = true;
                            }
                            break;
                        case 'lessThanDate':
                            if (new Date($("input[id^='" + field + "']").val()) < validationInfo.Fields[field][rules]) {
                                AddFieldValidation({
                                    type: "table",
                                    fieldID: field,
                                    errorType: 'lessThanDate',
                                    message: validationInfo.message[field].lessThanDate
                                });
                                error = true;
                            }
                            break;
                    }
                }
            }
            return !error;
        };
        Utility.Validate = Validate;

        //New Validations

        // Auto resize Modal
        var ResizeModal = function () {
            var dlg = SP.UI.ModalDialog.get_childDialog();
            if (dlg != null) {
                dlg.autoSize();
            }
        }
        Utility.ResizeModal = ResizeModal;

        // Initialize overlay loader
        var AddOverlayLoader = function () {
            $("body").after("<div id='overlay'><div id='loader'></div></div>");
            $("#overlay").hide();
            $("#overlay").children().hide();
        }
        Utility.AddOverlayLoader = AddOverlayLoader;

        // Show or hide overlay loader
        var DisplayOverlayLoader = function (display) {
            if (display) {
                $("#overlay").show();
                $("#overlay").children().show();
            } else {
                $("#overlay").fadeOut();
                $("#loader").fadeOut();
            }
        }
        Utility.DisplayOverlayLoader = DisplayOverlayLoader;

        // Get Icon for files
        function getIcon(file) {
            switch (file.replace(/^.*\./, '')) {
                case "xlsx":
                case "xlsm":
                case "xltx":
                case "xltm":
                case "xls":
                case "xlt":
                case "xlm":
                    return "/_layouts/15/images/icxls.png";
                case "doc":
                case "dot":
                case "wbk":
                case "docx":
                case "docm":
                case "dotx":
                case "dotm":
                case "docb":
                    return "/_layouts/15/images/icdocx.png";
                case "ppt":
                case "pot":
                case "pps":
                case "pptx":
                case "pptm":
                case "potx":
                case "potm":
                case "ppam":
                case "ppsx":
                case "ppsm":
                case "sldx":
                case "sldm":
                    return "/_layouts/15/images/icpptx.png";
                case "pdf":
                    return "/_layouts/15/images/icpdf.png";
                case "png":
                case "jpeg":
                case "jpg":
                case "png":
                case "gif":
                    return "/_layouts/15/images/icjpg.gif";
                default:
                    fileTypeError = true;
                    return "/_layouts/15/images/icgen.gif";

            }
        }

        
        // File Uploader Manipulation
        // Create file uploader field
        var CreateFileUploader = function (fieldInfo) {
            // Configure field
            $($("[id^=" + fieldInfo.Id + "][id$=inplacerte]")[0]).attr("contenteditable", "false");
            $("div[id^='" + fieldInfo.Id + "']").css("border", "none");
            $(document).on("click", "[id^=" + fieldInfo.Id + "][id$=inplacerte]", function () {
                ExecuteHideRibbon();
            });
            if (fieldInfo.inNewForm) {
                $($("[id^=" + fieldInfo.Id + "][id$=inplacerte]")[0]).html("<table id='Attachments'></table>");
            } else {
                $("a#versionHistoryLink").hide();
            }

            var multipleAttachment = "";
            if (fieldInfo.multiple) {
                multipleAttachment = "multiple";
            }

            $("span#" + fieldInfo.Id).parent().next().children().before('<input type="file" id="file" ' + multipleAttachment + '>');

            $('input#file').change(function () {
				var layout = "";
                $("tr#dummyLink").remove();
                for (var i = 0; i < $(this).get(0).files.length; i++) {
                    var fileName = $(this).get(0).files[i].name;
                    layout += '<tr id="dummyLink">';
                    layout += '<td><a href="#"><img class="ms-asset-icon ms-rtePosition-4" src="' + getIcon(fileName) + '">' + fileName + '</a></td>';
                    layout += '</tr>';
                }
                $("table#Attachments").append(layout);
            });
        }
        Utility.CreateFileUploader = CreateFileUploader;
			

        // Set delete links on each file
        var SetDeleteLinks = function (CallbackAction) {
            $("table#Attachments tr").each(function () {
                $($(this)[0]).children(':eq(1)').append("<a id='deleteLink' href='#'><img src='/_layouts/15/images/rect.gif?rev=44'> Delete</a>");
            });
            $("a#deleteLink").on("click", function () {
                var Attachment = $(this).parent().prev().children('a').text();
                if (confirm('Are you sure to delete this item?')) {
                    $(this).parent().parent().remove();
                    CallbackAction(Attachment);
                }
            });
        }
        Utility.SetDeleteLinks = SetDeleteLinks;

        // Update placeholder of files
        var UpdateFilePlaceholder = function (fileInfo) {
            var layout = "";
            $("tr#dummyLink").remove();
            $("a#versionHistoryLink").show();
            $("a#deleteLink").remove();
            for (var i = 0; i < fileInfo.fileInput[0].files.length; i++) {
                var fileName = fileInfo.fileInput[0].files[i].name;
                var exist = $("table#Attachments td").filter(function () {
                    return $(this).text() == fileName;
                })
                if (exist.length == 0) {
                    layout += '<tr>';
                    layout += '<td><a href="' + fileInfo.filePath + fileName + '?Web=1" target="_blank"><img class="ms-asset-icon ms-rtePosition-4" src="' + getIcon(fileName) + '">' + fileName + '</a></td>';
                    layout += '<td style="padding-left:5px;"><a id="versionHistoryLink" href="' + _spPageContextInfo.webAbsoluteUrl + '/_layouts/15/Versions.aspx?FileName=' + fileInfo.filePath + fileName + '" target="_blank">View version history</a></td>';
                    layout += '</tr>';
                }
            }
            $("table#Attachments").append(layout);
        }
        Utility.UpdateFilePlaceholder = UpdateFilePlaceholder;
		
		

        var LoadMain = function (callback, contentId, timer) {
            $("body").after("<div id='loader'></div>");
            $("#" + contentId).addClass("ms-hide");
            setTimeout(function () {
                callback();
                $("#loader").hide();
                $("#" + contentId).removeClass("ms-hide");
            }, timer);
        };
        Utility.LoadMain = LoadMain;
		
		
		// Clone a form
		var CloneForm = function(formAttributes){
            var temp = $('div[id^=' + formAttributes.FormId + ']:last'); //select the form you want to duplicate
			var num = $('div[id^=' + formAttributes.FormId + ']' ).length + 1;
			var newId = temp.clone().prop('id', formAttributes.FormId + num); //assign new id
			var destination = $('div[id^=' + formAttributes.FormDestination + ']:last');
			destination.after(newId);
			if(formAttributes.AddLine){
				var line = '<br><hr style="border-top: 2px solid; color: #0002046b"/><br>';
				temp.append(line);
			}
     
			if(formAttributes.isNewForm){ //Clear the textbox,textarea
				var lastindex = formAttributes.CurrentIndex;
				var id = formAttributes.FormId + num;
                $($("#" + id).find("input[type=text], textarea")).val('');
                $("#" + id).find("span[id*='error']").remove();
			}		
		}
		Utility.CloneForm = CloneForm;
		
		//Horizontal Radio
		var HorizontalRadio = function(radioAttributes){
			if(radioAttributes.AllRadio){ //horizontal all radio button
				var temp = $("table[id $='ChoiceRadioTable']");
				for(var i=0; i<temp.length; i++){
					var firstRadio = $("table#" + temp[i].id + " tbody tr td .ms-RadioText:eq(0)");
					$("table#" + temp[i].id + " tbody tr td .ms-RadioText:gt(0)").prepend("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").appendTo($(firstRadio));	
				}	
			}
			else{
				//horizontal specific table with radio button
				var tableName = radioAttributes.tableName;
				var temp = $("table[id ^='" + tableName + "']");
				for(var i=0; i<temp.length; i++){
					var firstRadio = $("table#" + temp[i].id + " tbody tr td .ms-RadioText:eq(0)");
					$("table#" + temp[i].id + " tbody tr td .ms-RadioText:gt(0)").prepend("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").appendTo($(firstRadio));	
				}
			}
			

		}
		Utility.HorizontalRadio = HorizontalRadio;
		
    })((Utility = SharePoint.Utility) || (SharePoint.Utility = {}));

})(SharePoint || (SharePoint = {}));