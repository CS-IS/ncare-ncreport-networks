var SharePoint;
var Utility
(function (SharePoint) {
    Utility = SharePoint.Utility;
    function getFileExists(fileUrl, complete, error) {
        var ctx = SP.ClientContext.get_current();
        var file = ctx.get_web().getFileByServerRelativeUrl(fileUrl);
        ctx.load(file);
        ctx.executeQueryAsync(function () {
            complete(true);
        },
            function (sender, args) {
                if (args.get_errorTypeName() === "System.IO.FileNotFoundException") {
                    complete(false);
                }
                else {
                    error(args);
                }
            });
    }


    function createFolder(parentFolder, subFolder) {
        var clientContext;
        var oWebsite;
        var oWebsite1;
        var oList;
        var itemCreateInfo;
        var parentFolderURL;
        var url;
        var folder;


        clientContext = new SP.ClientContext.get_current();
        oWebsite = clientContext.get_web();
        oList = oWebsite.get_lists().getByTitle("Attachments");
        url = clientContext.get_url() + "/" + parentFolder + "/"

        //check folder if doesn't exist will error if not existing
        folder = clientContext.get_web().getFolderByServerRelativeUrl(url);

        //change context to  url of clientid
        oWebsite1 = clientContext.get_web(url);

        itemCreateInfo = new SP.ListItemCreationInformation();
        itemCreateInfo.set_underlyingObjectType(SP.FileSystemObjectType.folder);
        itemCreateInfo.set_leafName(subFolder);
        this.oListItem = oList.addItem(itemCreateInfo);
        this.oListItem.update();

        clientContext.load(this.oListItem);
        clientContext.executeQueryAsync(
            Function.createDelegate(this, successHandler),
            Function.createDelegate(this, errorHandler)
        );


        function successHandler() {
            console.log('success creating a folder: ' + url);
        }

        function errorHandler(msg, args) {
            console.log('failed creating a folder: ' + url + " " + args.get_message());
        }
    }


    function getIcon(file) {
        switch (file.replace(/^.*\./, '')) {
            case "xlsx":
            case "xlsm":
            case "xltx":
            case "xltm":
            case "xls":
            case "xlt":
            case "xlm":
                return "/_layouts/15/images/icxls.png";
            case "doc":
            case "dot":
            case "wbk":
            case "docx":
            case "docm":
            case "dotx":
            case "dotm":
            case "docb":
                return "/_layouts/15/images/icdocx.png";
            case "ppt":
            case "pot":
            case "pps":
            case "pptx":
            case "pptm":
            case "potx":
            case "potm":
            case "ppam":
            case "ppsx":
            case "ppsm":
            case "sldx":
            case "sldm":
                return "/_layouts/15/images/icpptx.png";
            case "pdf":
                return "/_layouts/15/images/icpdf.png";
            case "png":
            case "jpeg":
            case "jpg":
            case "png":
            case "gif":
                return "/_layouts/15/images/icjpg.gif";
            default:
                fileTypeError = true;
                return "/_layouts/15/images/icgen.gif";

        }
    }



    function createListItem(itemList) { //save items to list copied from util


        var clientContext = new SP.ClientContext.get_current();
        var oList = clientContext.get_web().get_lists().getById('EC206D02-6BF3-4D67-8F89-1B83A1675E3C');
        for (var i = 0; i < itemList.length; i++) {
            var oListItem;
            var itemCreateInfo = new SP.ListItemCreationInformation();
            oListItem = oList.addItem(itemCreateInfo);
            oListItem.set_item('Title', itemList[i]["Title"]);
            oListItem.set_item('DateCreated', itemList[i]["DateCreated"]);
            oListItem.set_item('AuditDate', itemList[i]["AuditDate"]);
            oListItem.set_item('AreaAudited', itemList[i]["AreaAudited"]);
            oListItem.set_item('NCEndorser', itemList[i]["NCEndorser"]);
            oListItem.set_item('NCCreator', itemList[i]["NCCreator"]);
            oListItem.set_item('Source', itemList[i]["Source"]);
            oListItem.set_item('NCRecipient', itemList[i]["NCRecipient"]);
            oListItem.set_item('NCNumber', itemList[i]["NCNumber"]);
            oListItem.set_item('Status', itemList[i]["Status"]);
            oListItem.set_item('Subject', itemList[i]["Subject"]);
            oListItem.set_item('Description', itemList[i]["Description"]);
            oListItem.set_item('NonConformance', itemList[i]["NonConformance"]);
            oListItem.set_item('FileAttachments', itemList[i]["FileAttachments"]);
            oListItem.update();
            clientContext.load(oListItem);

        }

        clientContext.executeQueryAsync(onQuerySucceeded, onQueryFailed);



        function onQuerySucceeded() {
            var fileArray = [];
            createFolder("Attachments", $('#txtReportId').val()); //create a new folder for the new item 
            var fileUploaderCounter = 0;
            var fileAttachmentCounter = 0;
            var fileAttachmentUpload = 0;
            var numberOfAttachments = $("input#txtAttachments").length;

            for (var i = 0; i < $("input#txtAttachments").length; i++) { // upload files to the folder
                fileArray = $("input#txtAttachments")[i].files;
                if (fileArray != undefined || file != null) {
                    for (var j = 0; j < fileArray.length; j++) {
                        fileAttachmentCounter++;    //get the number of attachments;
                    }
                }
            }

            console.log("Number of attachments: " + fileAttachmentCounter);

            for (var i = 0; i < $("input#txtAttachments").length; i++) { // upload files to the folder
                fileArray = $("input#txtAttachments")[i].files;
                if (fileArray != undefined || file != null) {
                    for (var j = 0; j < fileArray.length; j++) {

                        $pnp.sp.web.getFolderByServerRelativeUrl("/sites/SharePointdev/Attachments/" + $('#txtReportId').val() + "/").files.add(fileArray[j].name, fileArray[j], true).then(
                            function (data) {
                                fileAttachmentUpload++;
                                if (fileAttachmentUpload == fileAttachmentCounter) {
                                    console.log("Next Page . . .");
                                    window.location = "/sites/SharePointdev/Lists/NCReport/AllItems.aspx";
                                }
                                else {
                                    console.log("Continue . . .");
                                }
                            }
                        );

                    }
                }
            }
        }
        function onQueryFailed(sender, args) {
            alert('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
        }

    }

    function initializePeoplePicker(peoplePickerElementId) {
        var schema = {};
        schema['PrincipalAccountType'] = 'User,DL,SecGroup,SPGroup';
        schema['SearchPrincipalSource'] = 15;
        schema['ResolvePrincipalSource'] = 15;
        schema['AllowMultipleValues'] = false;
        schema['MaximumEntitySuggestions'] = 50;
        this.SPClientPeoplePicker_InitStandaloneControlWrapper(peoplePickerElementId, null, schema);
    }

    function getUserInfo(peoplePickerId) {
        var peoplePicker = this.SPClientPeoplePicker.SPClientPeoplePickerDict[peoplePickerId + '_TopSpan'];
        var users = peoplePicker.GetAllUserInfo();
        var userInfo = '';
        var name = '';
        for (var i = 0; i < users.length; i++) {
            var user = users[i];
            for (var userProperty in user) {
                userInfo += userProperty + ':  ' + user[userProperty] + '<br>';
                name = user["DisplayText"];
            }
        }
        console.log("name: " + name);
        // $('#resolvedUsers').html(userInfo);
        //  var keys = peoplePicker.GetAllUserKeys();
        // $('#userKeys').html(keys);
        return name;
        // getUserId(users[0].Key);
    }

    function getUserId(loginName) {
        var context = new SP.ClientContext.get_current();
        this.user = context.get_web().ensureUser(loginName);
        context.load(this.user);
        context.executeQueryAsync(
            Function.createDelegate(null, function () {
                $('#userId').html(this.user.get_id());
            }),
            Function.createDelegate(null, function () {
                alert('Query failed. Error: ' + args.get_message());
            })
        );
    }

    function SetCurrentUsernameToPeoplePicker() {
        var ctx = new SP.ClientContext.get_current();
        this.website = ctx.get_web();
        this.currentUser = website.get_currentUser();
        ctx.load(currentUser);
        ctx.executeQueryAsync(Function.createDelegate(this, function (sender, args) {
            var loginName = currentUser.get_loginName();
            var form = $("div[id='peoplePickerDiv']");

            var userField = form.find("input[id$='peoplePickerDiv_TopSpan_EditorInput']").get(0);
            var peoplepicker = SPClientPeoplePicker.PickerObjectFromSubElement(userField);
            peoplepicker.AddUserKeys(loginName);
        }),
            Function.createDelegate(this, function (sender, args) {
                alert('request failed ' + args.get_message() + '\n' + args.get_stackTrace());
            }));
    }
    var g_AllTerms;

    function getSourceTerms() {
        var clientContext = SP.ClientContext.get_current();
        var taxonomySession = SP.Taxonomy.TaxonomySession.getTaxonomySession(clientContext);
        var termStores = taxonomySession.get_termStores();
        //Name of the Term Store from which to get the Terms.  
        var termStore = termStores.getByName("Taxonomy_NXoiZ8+XvIL1gqXB/1MIJQ==");
        //GUID of Term Set from which to get the Terms.  
        var termSet = termStore.getTermSet("6d0c9821-e1fb-4821-b838-c5ee6d542182");
        var terms = termSet.getAllTerms();
        clientContext.load(terms, 'Include(IsRoot, Labels, TermsCount, CustomSortOrder, Id, IsAvailableForTagging, Name, PathOfTerm, Parent, TermSet.Name)');
        clientContext.executeQueryAsync(function onSuccess() {

            var termsEnumerator = terms.getEnumerator(),
                tree = {
                    term: terms,
                    children: []
                };

            var data = [];
            while (termsEnumerator.moveNext()) {
                var currentTerm = termsEnumerator.get_current();
                var currentTermPath = currentTerm.get_pathOfTerm().split(';');
                var children = tree.children;

                for (var i = 0; i < currentTermPath.length; i++) {
                    var foundNode = false;
                    for (var j = 0; j < children.length; j++) {
                        if (children[j].name === currentTermPath[i]) {
                            foundNode = true;
                            break;
                        }
                    }

                    var term = foundNode ? children[j] : { name: currentTermPath[i], children: [] };

                    if (i === currentTermPath.length - 1) {
                        term.term = currentTerm;
                        term.title = currentTerm.get_name();
                        term.guid = currentTerm.get_id().toString();
                    }

                    if (foundNode) {
                        children = term.children;
                    }
                    else {
                        children.push(term);

                        if (i !== currentTermPath.length - 1) {
                            children = term.children;
                        }
                    }
                }
            }


            var html = '';

            // Kick off the term rendering
            for (var i = 0; i < tree.children.length; i++) {
                html += renderTerm(tree.children[i]);
            }

            // Append the create HTML to the bottom of the page
            var list = document.createElement('ul');
            list.setAttribute("id", "tree");
            list.innerHTML = html;
            /* list.style.height = "300px";
            list.style.overflow = "auto"; */
            //$('#btnSaveDraft').after(list);
            $('input#txtNonconformance').after(list);
            function renderTerm(term) {

                var html = '<li><div>' + term.title + '</div>';

                if (term.children && term.children.length) {
                    html += '<ul>';

                    for (var i = 0; i < term.children.length; i++) {
                        html += renderTerm2(term.children[i]);
                    }

                    html += '</ul>';
                }


                return html + '</li>';
            }

            function renderTerm2(term) {

                var html = '<li><div>' + term.title + '</div>';

                if (term.children && term.children.length) {
                    html += '<ul>';

                    for (var i = 0; i < term.children.length; i++) {
                        html += renderTerm(term.children[i]);
                    }

                    html += '</ul>';
                }


                return html + '</li>';
            }

            setTimeout(createTreeView(), 1000);


        }, function onFailure(args, msg) {
            alert('Error: ' + msg.get_message());
        });

    }

    function getTerms() {
        var clientContext = SP.ClientContext.get_current();
        var taxonomySession = SP.Taxonomy.TaxonomySession.getTaxonomySession(clientContext);
        var termStores = taxonomySession.get_termStores();
        //Name of the Term Store from which to get the Terms.  
        var termStore = termStores.getByName("Taxonomy_NXoiZ8+XvIL1gqXB/1MIJQ==");
        //GUID of Term Set from which to get the Terms.  
        var termSet = termStore.getTermSet("2655bec8-d9f6-436a-858b-e71b73836718");
        var terms = termSet.getAllTerms();
        clientContext.load(terms);
        clientContext.executeQueryAsync(function onSuccess() {
            var title = [];
            var txtTitle = [];
            var enumerator = terms.getEnumerator();
            while (enumerator.moveNext()) {
                var spTerm = enumerator.get_current();
                var items2 = spTerm.get_pathOfTerm().split(';');
                if (items2[0] == null) {
                    items2[0] = "";
                }
                if (items2[1] == null) {
                    items2[1] = "";
                }
                txtTitle.push({ "Label": items2[1], "category": items2[0] });

            }
            InitializeInput(txtTitle, "txtAreaAudited");

        }, function onFailure(args, msg) {
            alert('Error: ' + msg.get_message());
        });
    }

    function InitializeInput(data, inputId) {
        $(function () {
            $.widget("custom.catcomplete", $.ui.autocomplete, {
                _create: function () {
                    this._super();
                    this.widget().menu("option", "items", "> :not(.ui-autocomplete-category)");
                },
                _renderMenu: function (ul, items) {
                    var that = this,
                        currentCategory = "";
                    $.each(items, function (index, item) {
                        var li;
                        if (item.category != currentCategory) {
                            ul.append("<li class='ui-autocomplete-category'>" + item.category + "</li>");
                            ul.addClass("ms-Dropdown-items");
                            ul.addClass("width", "424px");
                            currentCategory = item.category;
                        }
                        li = that._renderItemData(ul, item).addClass(item.category);
                        if (item.category) {
                            li.attr("aria-label", item.category + " : " + item.label);
                            li.addClass("ms-Dropdown-item");
                        }
                    });
                }
            });
            var items = [];
            for (var i = 0; i < data.length; i++) {
                if (data[i]["Label"] != "") {
                    items.push({ label: data[i]["Label"], category: data[i]["category"] });
                }
            }
            $("#" + inputId).catcomplete({
                delay: 0,
                source: items,
                messages: {
                    noResults: '',
                    results: function () { }
                }
            });
        });

    }

    function createTreeView() {
        var tree = new treefilter($("#tree"), {
            searcher: $("input[id^='txtNonconformance']"),
            expanded: true,
            multiselect: false
        });

        $("input[id^='txtNonconformance']").focusin(function () {
            $(this).next('ul[id^="tree"]').css("display", "block");
            //$("#tree").css("display", "block");
        })

        $('#tree li').click(function (e) {

            if ($(this).children("ul").length > 0) {
                // e.stopPropagation();
                //$(this).children('ul').toggle();
            }
            else {
                var sample = $(this).children().clone()    //clone the element
                    .children() //select all the children
                    .remove()   //remove all the children
                    .end()  //again go back to selected element
                    .text();
                var index = $(this).parent('ul[id^="tree"]')[0];
                $($(this).parents('ul[id="tree"]')[0]).prev('input').val(sample)
                //  $($("input#txtNonconformance")[0]).val(sample);
                $("#tree").css("display", "none");
            }
        })

        $('#tree').hover(function () {
            $(this).css("display", "block");
        }, function () {
            $(this).css("display", "none");
        })
    }

    SharePoint.InitializeComponents = function () {
        /*Retrieve Last Id from the List and create a unique ID and set to hidden HTML*/
        var caml = "<View><Query><OrderBy><FieldRef Name='ID' Ascending='False' /></OrderBy></Query><ViewFields><FieldRef Name='Title' /></ViewFields><RowLimit>1</RowLimit></View>"
        Utility.RetrieveListItemsById("EC206D02-6BF3-4D67-8F89-1B83A1675E3C", caml,
            function (listItemEnumerator) {
                var ReportId = "0000000";
                while (listItemEnumerator.moveNext()) {
                    var listItem = listItemEnumerator.get_current();
                    ReportId = listItem.get_item('Title');
                };
                var id = ReportId;
                var padding = '0000000'
                var num = "" + (parseInt(id.match(/\d+/g), 10) + 1);
                var newId = 'SharePoint-' + padding.substring(0, padding.length - num.length) + num;
                console.log("Report ID: " + newId);
                $('input[id^="txtReportId"]').val(newId);
            });

        /*Initialize fabric components*/

        if ($.fn.DatePicker) {
            $('.ms-DatePicker').DatePicker({
                format: 'mm/dd/yyyy'
            });
        }
        initializePeoplePicker('peoplePickerDiv');
        initializePeoplePicker('peoplePickerDiv2');
        initializePeoplePicker('peoplePickerDiv3');
        $('div[id $= "_TopSpan"]').addClass('ms-TextField-field');
        $('#btnDateCreatedToday').click();
        getTerms();
        getSourceTerms();
    }
    SharePoint.InitializeComponentsEvents = function () {
        $('#txtAttachments').change(function () {
            var layout = "";
            $(this).siblings('div[id^="dummyLink"]').remove();
            for (var i = 0; i < $(this).get(0).files.length; i++) {
                var fileName = $(this).get(0).files[i].name;
                layout += '<div id="dummyLink">';
                layout += '<span><a href="#"><img class="ms-asset-icon ms-rtePosition-4" src="' + getIcon(fileName) + '">' + fileName + '</a></span>';
                layout += '</div>';
            }
            $(this).after(layout);
        });

        $('input#btnSaveDraft').click(function () { //button Draft click event

            SP.SOD.executeOrDelayUntilScriptLoaded(function () {
                SP.UI.ModalDialog.showWaitScreenWithNoClose("Saving Reports", "Please wait for the files to Upload...");
            }, 'sp.js', 'SP.ClientContext');

            var Utility = SharePoint.Utility;
            var validation;
            var radioClick = $($('#choicefieldgroup').find('ul .ms-RadioButton').find('label')[0]).hasClass("is-checked")
            if (radioClick) {
                validation = {
                    Fields: {
                        datePickerAuditDate: {
                            required: true
                        },
                        peoplePickerDiv: {
                            required: true
                        },
                        peoplePickerDiv2: {
                            required: true
                        },
                        peoplePickerDiv3: {
                            required: true
                        },
                        txtSource: {
                            required: true
                        }
                    },
                    FieldType: {
                        datePickerAuditDate: {
                            type: "datePicker"
                        },
                        peoplePickerDiv: {
                            type: "peoplePicker"
                        },
                        peoplePickerDiv2: {
                            type: "peoplePicker"
                        },
                        peoplePickerDiv3: {
                            type: "peoplePicker"
                        },
                        txtSource: {
                            type: "input"
                        }
                    },
                    message: {
                        datePickerAuditDate: {
                            required: "This field is required",
                        },
                        peoplePickerDiv: {
                            required: "This field is required"
                        },
                        peoplePickerDiv2: {
                            required: "This field is required"
                        },
                        peoplePickerDiv3: {
                            required: "This field is required"
                        },
                        txtSource: {
                            required: "This field is required"
                        }
                    }

                }

            }
            else {

            }

            if (Utility.Validate(validation)) {
                var NCReportCount = $('input[id^="txtNCNumber"').length;
                var NCReport = $('input[id^="txtReportId"]').val();
                var ReportList = [];
                var AreaAudited = "";
                if (radioClick) {
                    AreaAudited = $('#txtAreaAudited').val();
                }
                else {
                    AreaAudited = $('#txtSpecAreaAudited').val();
                }

                for (var i = 0; i < NCReportCount; i++) {

                    var dateCreated = new Date($('input[id^="dtCreatedDate"').val());
                    var AuditDate = new Date($('input[id^="dtAuditDate"]').val());
                    var endorser = getUserInfo("peoplePickerDiv2");
                    var creator = getUserInfo("peoplePickerDiv");
                    var recipient = getUserInfo("peoplePickerDiv3");
                    var source = $('input[id^="txtSource"').val();
                    var ncnumber = $($('input[id^="txtNCNumber"]')[i]).val();
                    var status = 'NC-Draft';
                    var subj = $($('input[id^="txtSubject"]')[i]).val();
                    var description = $($('textarea[id^="txtDescription"]')[i]).val();
                    var nonconf = $($('input[id^="txtNonconformance"]')[i]).val();
                    var attachments = "";

                    var attachments = "";

                    var link = $('div[id^="dummyLink"]');
                    for (var j = 0; j < link.length; j++) {
                        attachments += "<br/>";
                        attachments += $($('div[id^="dummyLink"]')[j]).html();
                    }



                    ReportList.push(
                        {
                            "Title": NCReport,
                            "DateCreated": dateCreated,
                            "AuditDate": AuditDate,
                            "AreaAudited": AreaAudited,
                            "NCEndorser": SP.FieldUserValue.fromUser(endorser),
                            "NCCreator": SP.FieldUserValue.fromUser(creator),
                            "Source": source,
                            "NCRecipient": SP.FieldUserValue.fromUser(recipient),
                            "NCNumber": ncnumber,
                            "Status": status,
                            "Subject": subj,
                            "Description": description,
                            "NonConformance": nonconf,
                            "FileAttachments": attachments,
                        }
                    );
                }
                var ListInfo = {
                    ListTitle: 'EC206D02-6BF3-4D67-8F89-1B83A1675E3C',
                    Columns: ReportList
                }
                createListItem(ReportList);
            }
            else {
                return false;
            }

        });

        $('input#btnSaveReport').click(function () { //button Save Report click event

            var Utility = SharePoint.Utility;
            var validation;
            var radioClick = $($('#choicefieldgroup').find('ul .ms-RadioButton').find('label')[0]).hasClass("is-checked")
            if (radioClick) {
                validation = {
                    Fields: {
                        datePickerAuditDate: {
                            required: true
                        },
                        peoplePickerDiv: {
                            required: true
                        },
                        peoplePickerDiv2: {
                            required: true
                        },
                        peoplePickerDiv3: {
                            required: true
                        },
                        txtSource: {
                            required: true
                        }
                    },
                    FieldType: {
                        datePickerAuditDate: {
                            type: "datePicker"
                        },
                        peoplePickerDiv: {
                            type: "peoplePicker"
                        },
                        peoplePickerDiv2: {
                            type: "peoplePicker"
                        },
                        peoplePickerDiv3: {
                            type: "peoplePicker"
                        },
                        txtSource: {
                            type: "input"
                        }
                    },
                    message: {
                        datePickerAuditDate: {
                            required: "This field is required",
                        },
                        peoplePickerDiv: {
                            required: "This field is required"
                        },
                        peoplePickerDiv2: {
                            required: "This field is required"
                        },
                        peoplePickerDiv3: {
                            required: "This field is required"
                        },
                        txtSource: {
                            required: "This field is required"
                        }
                    }

                }

            }
            else {

            }

            if (Utility.Validate(validation)) {
                var NCReportCount = $('input[id^="txtNCNumber"').length;
                var NCReport = $('input[id^="txtReportId"]').val();
                var ReportList = [];
                var AreaAudited = "";
                if (radioClick) {
                    AreaAudited = $('#txtAreaAudited').val();
                }
                else {
                    AreaAudited = $('#txtSpecAreaAudited').val();
                }

                for (var i = 0; i < NCReportCount; i++) {

                    var dateCreated = new Date($('input[id^="dtCreatedDate"').val());
                    var AuditDate = new Date($('input[id^="dtAuditDate"]').val());
                    var endorser = getUserInfo("peoplePickerDiv2");
                    var creator = getUserInfo("peoplePickerDiv");
                    var recipient = getUserInfo("peoplePickerDiv3");
                    var source = $('input[id^="txtSource"').val();
                    var ncnumber = $($('input[id^="txtNCNumber"]')[i]).val();
                    var status = 'NC-For Review';
                    var subj = $($('input[id^="txtSubject"]')[i]).val();
                    var description = $($('textarea[id^="txtDescription"]')[i]).val();
                    var nonconf = $($('input[id^="txtNonconformance"]')[i]).val();
                    var attachments = "";

                    var attachments = "";

                    var link = $('div[id^="dummyLink"]').find('span');
                    for (var j = 0; j < link.length; j++) {
                        attachments += $($('div[id^="dummyLink"]').find('span')[j]).html();
                        attachments += "<br/>"
                    }



                    ReportList.push(
                        {
                            "Title": NCReport,
                            "DateCreated": dateCreated,
                            "AuditDate": AuditDate,
                            "AreaAudited": AreaAudited,
                            "NCEndorser": SP.FieldUserValue.fromUser(endorser),
                            "NCCreator": SP.FieldUserValue.fromUser(creator),
                            "Source": source,
                            "NCRecipient": SP.FieldUserValue.fromUser(recipient),
                            "NCNumber": ncnumber,
                            "Status": status,
                            "Subject": subj,
                            "Description": description,
                            "NonConformance": nonconf,
                            "FileAttachments": attachments,
                        }
                    );
                }
                var ListInfo = {
                    ListTitle: 'EC206D02-6BF3-4D67-8F89-1B83A1675E3C',
                    Columns: ReportList
                }

                createListItem(ReportList);
            }
            else {
                return false;
            }

        });

        $('input#btnAddReport').click(function () {

            var currentIndex = $("input[id^='txtNCNumber']").length;
            var formAttributes = {
                FormId: 'NCReport',
                FormDestination: 'NCReport',
                AddLine: true,
                isNewForm: true,
                CurrentIndex: currentIndex
            };
            Utility.CloneForm(formAttributes);

            var id = $($('input[id^="txtNCNumber"]')[currentIndex - 1]).val();
            var padding = '000000'
            var num = "" + (parseInt(id.match(/\d+/g), 10) + 1);
            EmpId = 'NC-SCLM-' + padding.substring(0, padding.length - num.length) + num;
            $($('input[id^="txtNCNumber"]')[currentIndex]).val(EmpId);
            $('input[id^="txtStatus"]').val('New');

            $($('input[id^="txtNonconformance"]')[currentIndex]).val('');
            $($('input[id^="txtAttachments"]')[currentIndex]).siblings('div[id^="dummyLink"]').remove();
            $($('input[id^="txtAttachments"]')[currentIndex]).val('');
            return false;

        });

        ExecuteOrDelayUntilScriptLoaded(SetCurrentUsernameToPeoplePicker, "sp.js");

    }

})(SharePoint || (SharePoint = {}));
$(document).ready(function () {
    SP.SOD.executeFunc('sp.js', 'clientpeoplepicker.js', 'SP.ClientContext', SharePoint.InitializeComponents);
    SP.SOD.executeFunc('sp.js', 'clientpeoplepicker.js', 'SP.ClientContext', SharePoint.InitializeComponentsEvents);
})