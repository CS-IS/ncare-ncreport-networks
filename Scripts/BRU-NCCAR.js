var Utility = SharePoint.Utility;
var dialog;
var ValidFiles = Application.ValidFiles;
var Application = Application.AppConfig;
var BRUAllItemLink = Application["Link"]["BRU"]
var IQAAllItemLink = Application["Link"]["IQA"];
var BRUDraftLink = Application["Link"]["BRUDraft"];
var IQADraftLink = Application["Link"]["IQADraft"];
var globalStatus = "";


function UnableToView(Message, RedirectLink) {
  var html = document.createElement('div');
  html.innerHTML = '<h2>' + Message + '</h2>\
            <br>\
            <div class="ms-core-form-section"><div class="ms-core-form-bottomButtonBox"><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.OK); return false;">OK</button>\
            </div></div>'
  OpenPopUpPageWithDialogOptions({
    title: "Warning",
    html: html,
    showClose: false,
    dialogReturnValueCallback: function (dialogResult) {
      if (dialogResult == 1) {
        window.location = RedirectLink;

      } else {
        window.location = RedirectLink;
      }
    }
  });
}

function InitiateProgressMeter(Initiator) {
  var caml = "<View><Query> <OrderBy> <FieldRef Name='Order0' Ascending='True' /> </OrderBy> </Query></View>";
  var ListId = "";
  if (Initiator.toUpperCase() != "BRU") {
    ListId = Application["List"]["IQAStatus"];;
  } else {
    ListId = Application["List"]["BRUStatus"];;
  }

  Utility.RetrieveListItemsById(ListId, caml,
    function (listItemEnumerator) {
      var items = [];
      while (listItemEnumerator.moveNext()) {
        var listItem = listItemEnumerator.get_current();
        items.push({
          "Status": listItem.get_item("Title"),
          "ProgressMark": listItem.get_item("ProgressMark")
        })
      };
      CreateProgressMeter(items);
    });

  function CreateProgressMeter(ItemRecord) {
    var layout = "";

    for (var i = 0; i < ItemRecord.length; i++) {
      layout += "<li id='" + ItemRecord[i]["Status"] + "'><span>" + ItemRecord[i]["ProgressMark"] + "<span></li>";
    }
    $('#ProgressMeter').append(layout);
    var status = $('input[id^="txtStatus"]').val();
    $('#ProgressMeter > li[id^="' + status + '"]').prevAll().addClass('inactive');
    $('#ProgressMeter > li[id^="' + status + '"]').addClass('active');
  }
}

function SaveBRUReport(ReportStatus) {
  var Utility = SharePoint.Utility;
  var validation;
  //var radioClick = $($('#choicefieldgroup').find('ul .ms-RadioButton').find('label')[0]).hasClass("is-checked")
  validation = {
    Fields: {
      datePickerAuditDate: {
        required: true
      },
      peoplePickerDiv: {
        required: true
      },
      peoplePickerDiv4: {
        required: true
      },
      txtSource: {
        required: true
      },
      txtSubject: {
        required: true
      },
      txtDescription: {
        required: true
      },
      txtNonconformance: {
        required: true
      },
      txtAreaAudited: {
        required: true
      },
      txtCategory: {
        required: true
      },
      txtAuditor: {
        required: true
      },
      txtRiskCategory: {
        required: true
      }

    },
    FieldType: {
      datePickerAuditDate: {
        type: "datePicker"
      },
      peoplePickerDiv: {
        type: "peoplePicker"
      },
      peoplePickerDiv4: {
        type: "peoplePicker"
      },
      txtSource: {
        type: "input"
      },
      txtSubject: {
        type: "input"
      },
      txtDescription: {
        type: "textarea"
      },
      txtNonconformance: {
        type: "input"
      },
      txtAreaAudited: {
        type: "input"
      },
      txtCategory: {
        type: "input"
      },
      txtAuditor: {
        type: "input"
      },
      txtRiskCategory: {
        type: "input"
      }
    },
    message: {
      datePickerAuditDate: {
        required: "This field is required",
      },
      peoplePickerDiv: {
        required: "This field is required"
      },
      peoplePickerDiv4: {
        required: "This field is required"
      },
      txtSource: {
        required: "This field is required"
      },
      txtSubject: {
        required: "This field is required"
      },
      txtDescription: {
        required: "This field is required"
      },
      txtNonconformance: {
        required: "This field is required"
      },
      txtAreaAudited: {
        required: "This field is required"
      },
      txtCategory: {
        required: "This field is required"
      },
      txtAuditor: {
        required: "This field is required"
      },
      txtRiskCategory: {
        required: "This field is required"
      }
    }

  }

  if (Utility.Validate(validation)) {
    try {
      dialog = SP.SOD.execute('sp.ui.dialog.js', 'SP.UI.ModalDialog.showWaitScreenWithNoClose', "Saving Report", "Please Wait");
      var NCReportCount = $('input[id^="txtNCNumber"').length;
      var NCReport = $('input[id^="txtReportId"]').val();
      var ReportList = [];
      var AreaAudited = "";
      var agingdate = new Date();
      AreaAudited = $('#txtAreaAudited').val();


      for (var i = 0; i < NCReportCount; i++) {

        var dateCreated = new Date($('input[id^="dtCreatedDate"').val());
        var AuditDate = new Date($('input[id^="dtAuditDate"]').val());
        var endorser = getUserInfo("peoplePickerDiv");
        var creator = getUserInfo("peoplePickerDiv");
        var recipient = getUserInfo("peoplePickerDiv");
        var source = $('input[id^="txtSource"').val();
        var ncnumber = $($('input[id^="txtNCNumber"]')[i]).val();
        var status = ReportStatus;
        var subj = $($('input[id^="txtSubject"]')[i]).val();
        var description = $($('textarea[id^="txtDescription"]')[i]).val();
        var nonconf = $($('input[id^="txtNonconformance"]')[i]).val();
        var attachments = "";
        var reporttype = $('input[id^="txtReportType"]').val();
        var ncassignee = getUserInfo("peoplePickerDiv4");
        var attachments = $($($('input[id^="txtAttachments"]')[i]).siblings('div')[0]).html().trim();
        var cc = getUserInfoMulti("peopleCC");
        agingdate.setDate(agingdate.getDate());
        var category = $($('input[id^="txtCategory"]')[i]).val();
        var referencerecord = $('input[id^="txtRefRecord"]').val().trim();
        var executingpersonnel = $('input[id^="txtExecutingPersonnel"]').val().trim();
        var auditor = $('input[id^="txtAuditor"]').val().trim();
        var riskcategory = $('input[id^="txtRiskCategory"]').val().trim();
        var actor = null;
        if (ReportStatus.toUpperCase() == "NC-FOR REVIEW") {
          actor = endorser;
        } else {
          actor = creator;
        }

        if (cc.length > 0) {
          var emailCC = [];
          for (var ii in cc) {
            var lookupValue = SP.FieldUserValue.fromUser(cc[ii]);
            emailCC.push(lookupValue);
          }

          ReportList.push({
            "Title": NCReport,
            "DateCreated": dateCreated,
            "AuditDate": AuditDate,
            "AreaAudited": AreaAudited,
            "NCCreator": SP.FieldUserValue.fromUser(creator),
            "NCEndorser": SP.FieldUserValue.fromUser(endorser),
            "Source": source,
            "NCRecipient": SP.FieldUserValue.fromUser(recipient),
            "NCNumber": ncnumber,
            "Status": status,
            "Subject": subj,
            "Description": description,
            "NonConformance": nonconf,
            "FileAttachments": attachments,
            "ReportType": reporttype,
            "NCAssignee": SP.FieldUserValue.fromUser(ncassignee),
            "Flag": "No",
            "ReminderFlag": "No",
            "AgingDate": agingdate,
            "EmailCC": emailCC,
            "CommentFlag": false,
            "Category": category,
            "ExecutingPersonnel": executingpersonnel,
            "ReferenceRecord": referencerecord,
            "Auditor": auditor,
            "RiskCategory": riskcategory,
            "Actor": actor != null && actor != "" ? SP.FieldUserValue.fromUser(actor) : null
          });

        } else {
          ReportList.push({
            "Title": NCReport,
            "DateCreated": dateCreated,
            "AuditDate": AuditDate,
            "AreaAudited": AreaAudited,
            "NCCreator": SP.FieldUserValue.fromUser(creator),
            "NCEndorser": SP.FieldUserValue.fromUser(endorser),
            "Source": source,
            "NCRecipient": SP.FieldUserValue.fromUser(recipient),
            "NCNumber": ncnumber,
            "Status": status,
            "Subject": subj,
            "Description": description,
            "NonConformance": nonconf,
            "FileAttachments": attachments,
            "ReportType": reporttype,
            "NCAssignee": SP.FieldUserValue.fromUser(ncassignee),
            "Flag": "No",
            "ReminderFlag": "No",
            "AgingDate": agingdate,
            "CommentFlag": false,
            "Category": category,
            "ExecutingPersonnel": executingpersonnel,
            "ReferenceRecord": referencerecord,
            "Auditor": auditor,
            "RiskCategory": riskcategory,
            "Actor": actor != null && actor != "" ? SP.FieldUserValue.fromUser(actor) : null
          });
        }
      }

      createListItem(ReportList);
    } catch (err) {
      SP.UI.ModalDialog.commonModalDialogClose(1, 1);
    }

  } else {
    return false;
  }
}


function SaveReport(ReportStatus) {
  var Utility = SharePoint.Utility;
  var validation;

  validation = {
    Fields: {
      datePickerAuditDate: {
        required: true
      },
      peoplePickerDiv: {
        required: true
      },
      peoplePickerDiv3: {
        required: true
      },
      txtSource: {
        required: true
      },
      txtSubject: {
        required: true
      },
      txtDescription: {
        required: true
      },
      txtNonconformance: {
        required: true
      },
      txtAttachments: {
        required: true
      },
      txtAreaAudited: {
        required: true
      },
      txtCategory: {
        required: true
      }

    },
    FieldType: {
      datePickerAuditDate: {
        type: "datePicker"
      },
      peoplePickerDiv: {
        type: "peoplePicker"
      },
      peoplePickerDiv3: {
        type: "peoplePicker"
      },
      txtSource: {
        type: "input"
      },
      txtSubject: {
        type: "input"
      },
      txtDescription: {
        type: "textarea"
      },
      txtNonconformance: {
        type: "input"
      },
      txtAttachments: {
        type: "attachment"
      },
      txtAreaAudited: {
        type: "input"
      },
      txtCategory: {
        type: "input"
      }
    },
    message: {
      datePickerAuditDate: {
        required: "This field is required",
      },
      peoplePickerDiv: {
        required: "This field is required"
      },
      peoplePickerDiv3: {
        required: "This field is required"
      },
      txtSource: {
        required: "This field is required"
      },
      txtSubject: {
        required: "This field is required"
      },
      txtDescription: {
        required: "This field is required"
      },
      txtNonconformance: {
        required: "This field is required"
      },
      txtAttachments: {
        required: "This field is required"
      },
      txtAreaAudited: {
        required: "This field is required"
      },
      txtCategory: {
        required: "This field is required"
      }
    }
  }

  if (Utility.Validate(validation)) {
    try {
      dialog = SP.SOD.execute('sp.ui.dialog.js', 'SP.UI.ModalDialog.showWaitScreenWithNoClose', "Saving Report", "Please Wait");
      var NCReportCount = $('input[id^="txtNCNumber"').length;
      var NCReport = $('input[id^="txtReportId"]').val();
      var ReportList = [];
      var AreaAudited = "";
      AreaAudited = $('#txtAreaAudited').val();

      for (var i = 0; i < NCReportCount; i++) {

        var dateCreated = new Date($('input[id^="dtCreatedDate"').val());
        var AuditDate = new Date($('input[id^="dtAuditDate"]').val());
        var creator = getUserInfo("peoplePickerDiv");
        var recipient = getUserInfo("peoplePickerDiv3");
        var source = $('input[id^="txtSource"').val();
        var ncnumber = $($('input[id^="txtNCNumber"]')[i]).val();
        var status = ReportStatus;
        var subj = $($('input[id^="txtSubject"]')[i]).val();
        var description = $($('textarea[id^="txtDescription"]')[i]).val();
        var nonconf = $($('input[id^="txtNonconformance"]')[i]).val();
        var attachments = "";
        var reporttype = $('input[id^="txtReportType"]').val();
        var attachments = $($($('input[id^="txtAttachments"]')[i]).siblings('div')[0]).html().trim();
        var category = $($('input[id^="txtCategory"]')[i]).val();
        var referencerecord = $('input[id^="txtRefRecord"]').val().trim();
        var executingpersonnel = $('input[id^="txtExecutingPersonnel"]').val().trim();
        var cc = getUserInfoMulti("peopleCC");
        agingdate.setDate(agingdate.getDate());

        if (cc.length > 0) {
          var emailCC = [];
          for (var ii in cc) {
            var lookupValue = SP.FieldUserValue.fromUser(cc[ii]);
            emailCC.push(lookupValue);
          }

          ReportList.push({
            "Title": NCReport,
            "DateCreated": dateCreated,
            "AuditDate": AuditDate,
            "AreaAudited": AreaAudited,
            "NCCreator": SP.FieldUserValue.fromUser(creator),
            "NCEndorser": SP.FieldUserValue.fromUser(endorser),
            "Source": source,
            "NCRecipient": SP.FieldUserValue.fromUser(recipient),
            "NCNumber": ncnumber,
            "Status": status,
            "Subject": subj,
            "Description": description,
            "NonConformance": nonconf,
            "FileAttachments": attachments,
            "ReportType": reporttype,
            "NCAssignee": SP.FieldUserValue.fromUser(ncassignee),
            "Flag": "No",
            "ReminderFlag": "No",
            "AgingDate": agingdate,
            "EmailCC": emailCC,
            "CommentFlag": false,
            "Category": category,
            "ExecutingPersonnel": executingpersonnel,
            "ReferenceRecord": referencerecord
          });

        } else {
          ReportList.push({
            "Title": NCReport,
            "DateCreated": dateCreated,
            "AuditDate": AuditDate,
            "AreaAudited": AreaAudited,
            "NCCreator": SP.FieldUserValue.fromUser(creator),
            "NCEndorser": SP.FieldUserValue.fromUser(endorser),
            "Source": source,
            "NCRecipient": SP.FieldUserValue.fromUser(recipient),
            "NCNumber": ncnumber,
            "Status": status,
            "Subject": subj,
            "Description": description,
            "NonConformance": nonconf,
            "FileAttachments": attachments,
            "ReportType": reporttype,
            "NCAssignee": SP.FieldUserValue.fromUser(ncassignee),
            "Flag": "No",
            "ReminderFlag": "No",
            "AgingDate": agingdate,
            "CommentFlag": false,
            "Category": category,
            "ExecutingPersonnel": executingpersonnel,
            "ReferenceRecord": referencerecord
          });
        }
      }
      createListItem(ReportList);
    } catch (err) {

    }

  } else {
    return false;
  }
}

function getFileExists(fileUrl, complete, error) {
  var ctx = SP.ClientContext.get_current();
  var file = ctx.get_web().getFileByServerRelativeUrl(fileUrl);
  ctx.load(file);
  ctx.executeQueryAsync(function () {
      complete(true);
    },
    function (sender, args) {
      if (args.get_errorTypeName() === "System.IO.FileNotFoundException") {
        complete(false);
      } else {
        error(args);
      }
    });
}


function createFolder(parentFolder, subFolder) {
  var clientContext;
  var oWebsite;
  var oWebsite1;
  var oList;
  var itemCreateInfo;
  var parentFolderURL;
  var url;
  var folder;


  clientContext = new SP.ClientContext.get_current();
  oWebsite = clientContext.get_web();
  oList = oWebsite.get_lists().getByTitle("Attachments");
  url = clientContext.get_url() + "/" + parentFolder + "/"

  //check folder if doesn't exist will error if not existing
  folder = clientContext.get_web().getFolderByServerRelativeUrl(url);

  //change context to  url of clientid
  oWebsite1 = clientContext.get_web(url);

  itemCreateInfo = new SP.ListItemCreationInformation();
  itemCreateInfo.set_underlyingObjectType(SP.FileSystemObjectType.folder);
  itemCreateInfo.set_leafName(subFolder);
  this.oListItem = oList.addItem(itemCreateInfo);

  this.oListItem.update();

  clientContext.load(this.oListItem);
  clientContext.executeQueryAsync(
    Function.createDelegate(this, successHandler),
    Function.createDelegate(this, errorHandler)
  );


  function successHandler() {
    console.log('success creating a folder: ' + url);
  }

  function errorHandler(msg, args) {
    console.log('failed creating a folder: ' + url + " " + args.get_message());
  }
}


function getIcon(file) {
  switch (file.replace(/^.*\./, '')) {
    case "xlsx":
    case "xlsm":
    case "xltx":
    case "xltm":
    case "xls":
    case "xlt":
    case "xlm":
      return "/_layouts/15/images/icxls.png";
    case "doc":
    case "dot":
    case "wbk":
    case "docx":
    case "docm":
    case "dotx":
    case "dotm":
    case "docb":
      return "/_layouts/15/images/icdocx.png";
    case "ppt":
    case "pot":
    case "pps":
    case "pptx":
    case "pptm":
    case "potx":
    case "potm":
    case "ppam":
    case "ppsx":
    case "ppsm":
    case "sldx":
    case "sldm":
      return "/_layouts/15/images/icpptx.png";
    case "pdf":
      return "/_layouts/15/images/icpdf.png";
    case "png":
    case "jpeg":
    case "jpg":
    case "png":
    case "gif":
      return "/_layouts/15/images/icjpg.gif";
    default:
      fileTypeError = true;
      return "/_layouts/15/images/icgen.gif";

  }
}


var oListItem;
var index;
var fileUploaderCounter = 0;
var fileAttachmentCounter = 0;
var fileAttachmentUpload = 0;


function createListItem(itemList) { //save items to list copied from util

  var clientContext = new SP.ClientContext.get_current();
  var oList = clientContext.get_web().get_lists().getById(Application["List"]["NCReport"])
  for (var i = 0; i < itemList.length; i++) {
    var itemCreateInfo = new SP.ListItemCreationInformation();
    this.oListItem = oList.addItem(itemCreateInfo);
    for (var column in itemList[i]) {
      oListItem.set_item(column, itemList[i][column]);
    }
    oListItem.update();
    //IDContainers[i] = oListItem;
    clientContext.load(oListItem);
  }

  clientContext.executeQueryAsync(onQuerySucceeded, onQueryFailed);

  function onQuerySucceeded() {

    var folder = $('#txtReportId').val();

    var fileArray = [];
    createFolder("Attachments", $('#txtReportId').val()); //create a new folder for the new item 

    if (fileAttachmentCounter <= 0) {
      for (var i = 0; i < $("input#txtAttachments").length; i++) { // upload files to the folder
        fileArray = $("input#txtAttachments")[i].files;
        if (fileArray != undefined || file != null) {
          for (var j = 0; j < fileArray.length; j++) {
            fileAttachmentCounter++; //get the number of attachments;
          }
        }
      }
    }

    console.log("Number of attachments: " + fileAttachmentCounter);

    if (fileAttachmentCounter > 0) {
      for (var i = 0; i < $("input[id^='txtAttachments']").length; i++) {
        fileArray = $("input[id^='txtAttachments']")[i].files;
        reportid = $($('input[id^="txtNCNumber"]')[i]).val()
        if (fileArray != undefined || file != null) {
          for (var j = 0; j < fileArray.length; j++) {
            $pnp.sp.web.getFolderByServerRelativeUrl(Application["SiteName"] + "/Attachments/" + $('#txtReportId').val() + "/").files.add(fileArray[j].name, fileArray[j], true).then(function (result) {
              result.file.listItemAllFields.get().then(function (listItemAllFields) {
                // get the item id of the file and then update the columns(properties)
                console.log(listItemAllFields);
                fileAttachmentUpload++;
                if (fileAttachmentUpload == fileAttachmentCounter) {
                  console.log("Next Page . . .");
                  if ($('#txtReportType').val().toUpperCase() == "BRU") {
                    if (globalStatus.toUpperCase() == "NC-DRAFT") {
                      window.location = BRUDraftLink;
                    } else {
                      window.location = BRUAllItemLink;
                    }
                  } else {
                    if (globalStatus.toUpperCase() == "NC-DRAFT") {
                      window.location = IQADraftLink;
                    } else {
                      window.location = IQAAllItemLink;
                    }
                  }
                } else {
                  console.log("Continue . . .");
                }
              });
            });

          }
        }
      }

    } else {
      setTimeout(function () {
        if ($('#txtReportType').val().toUpperCase() == "BRU") {
          if (globalStatus.toUpperCase() == "NC-DRAFT") {
            window.location = BRUDraftLink;
          } else {
            window.location = BRUAllItemLink;
          }
        } else {
          if (globalStatus.toUpperCase() == "NC-DRAFT") {
            window.location = IQADraftLink;
          } else {
            window.location = IQAAllItemLink;
          }
        }
      }, 2000);
    }


  }


  function onQueryFailed(sender, args) {
    alert('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
  }



}

function initializePeoplePicker2(peoplePickerElementId, GroupId) {
  var schema = {};
  schema['PrincipalAccountType'] = 'User,DL,SecGroup,SPGroup';
  schema['SearchPrincipalSource'] = 15;
  schema['ResolvePrincipalSource'] = 15;
  schema['AllowMultipleValues'] = true;
  schema['MaximumEntitySuggestions'] = 50;
  if (GroupId < 0) {
    schema['SharePointGroupID'] = GroupId;;
  }

  SPClientPeoplePicker_InitStandaloneControlWrapper(peoplePickerElementId, null, schema);
}

function initializePeoplePicker(peoplePickerElementId, GroupId) {
  var schema = {};
  schema['PrincipalAccountType'] = 'User,DL,SecGroup,SPGroup';
  schema['SearchPrincipalSource'] = 15;
  schema['ResolvePrincipalSource'] = 15;
  schema['AllowMultipleValues'] = false;
  schema['MaximumEntitySuggestions'] = 50;
  if (GroupId < 0) {
    schema['SharePointGroupID'] = GroupId;
  }
  SPClientPeoplePicker_InitStandaloneControlWrapper(peoplePickerElementId, null, schema);
}

function getUserInfoMulti(peoplePickerId) {
  var peoplePicker = this.SPClientPeoplePicker.SPClientPeoplePickerDict[peoplePickerId + '_TopSpan'];
  var users = peoplePicker.GetAllUserInfo();
  var userInfo = '';
  var name = "";
  var array = [];
  for (var i = 0; i < users.length; i++) {
    var user = users[i];
    name = user["DisplayText"];
    array.push(name);

  }
  return array;
}

function getUserInfo(peoplePickerId) {
  var peoplePicker = this.SPClientPeoplePicker.SPClientPeoplePickerDict[peoplePickerId + '_TopSpan'];
  var users = peoplePicker.GetAllUserInfo();
  var userInfo = '';
  var name = '';
  for (var i = 0; i < users.length; i++) {
    var user = users[i];
    for (var userProperty in user) {
      userInfo += userProperty + ':  ' + user[userProperty] + '<br>';
      name = user["DisplayText"];
    }
  }
  console.log("name: " + name);
  return name;
}

function getUserId(loginName) {
  var context = new SP.ClientContext.get_current();
  this.user = context.get_web().ensureUser(loginName);
  context.load(this.user);
  context.executeQueryAsync(
    Function.createDelegate(null, function () {
      $('#userId').html(this.user.get_id());
    }),
    Function.createDelegate(null, function () {
      alert('Query failed. Error: ' + args.get_message());
    })
  );
}

function retrieveToPeoplePicker(peoplePickerId) {

  var ctx = new SP.ClientContext.get_current();
  var website = ctx.get_web();
  var currentUser = website.get_currentUser();
  ctx.load(currentUser);
  ctx.executeQueryAsync(Function.createDelegate(this, function (sender, args) {
      var loginName = currentUser.get_loginName();
      var form = $("div[id='" + peoplePickerId + "']");

      var userField = form.find("input[id$='" + peoplePickerId + "_TopSpan_EditorInput']").get(0);
      var peoplepicker = SPClientPeoplePicker.PickerObjectFromSubElement(userField);
      peoplepicker.AddUserKeys(loginName);
    }),
    Function.createDelegate(this, function (sender, args) {
      alert('request failed ' + args.get_message() + '\n' + args.get_stackTrace());
    }));
}

function SetCurrentUsernameToPeoplePicker() {
  var ctx = SP.ClientContext.get_current();
  var website = ctx.get_web();
  var currentUser = website.get_currentUser();
  ctx.load(currentUser);
  ctx.executeQueryAsync(function (sender, args) {
      var loginName = currentUser.get_loginName();
      var form = $("div[id='peoplePickerDiv']");

      var userField = form.find("input[id$='peoplePickerDiv_TopSpan_EditorInput']").get(0);
      var peoplepicker = SPClientPeoplePicker.PickerObjectFromSubElement(userField);
      peoplepicker.AddUserKeys(loginName);
    },
    function (sender, args) {
      console.log('request failed ' + args.get_message() + '\n' + args.get_stackTrace());
    });
}
var g_AllTerms;

function getClauseOfStandardTerms() {
  var clientContext = SP.ClientContext.get_current();
  var taxonomySession = SP.Taxonomy.TaxonomySession.getTaxonomySession(clientContext);
  var termStores = taxonomySession.get_termStores();
  var termStore = taxonomySession.getDefaultSiteCollectionTermStore();
  //GUID of Term Set from which to get the Terms.  
  var termSet = termStore.getTermSet(Application["TermStore"]["ClauseOfStandard"]);
  var terms = termSet.getAllTerms();
  clientContext.load(terms, 'Include(IsRoot, Labels, TermsCount, CustomSortOrder, Id, IsAvailableForTagging, Name, PathOfTerm, Parent, TermSet.Name)');
  clientContext.executeQueryAsync(function onSuccess() {

    var termsEnumerator = terms.getEnumerator(),
      tree = {
        term: terms,
        children: []
      };

    var data = [];
    while (termsEnumerator.moveNext()) {
      var currentTerm = termsEnumerator.get_current();
      var currentTermPath = currentTerm.get_pathOfTerm().split(';');
      var children = tree.children;

      for (var i = 0; i < currentTermPath.length; i++) {
        var foundNode = false;
        for (var j = 0; j < children.length; j++) {
          if (children[j].name === currentTermPath[i]) {
            foundNode = true;
            break;
          }
        }

        var term = foundNode ? children[j] : {
          name: currentTermPath[i],
          children: []
        };

        if (i === currentTermPath.length - 1) {
          term.term = currentTerm;
          term.title = currentTerm.get_name();
          term.guid = currentTerm.get_id().toString();
        }

        if (foundNode) {
          children = term.children;
        } else {
          children.push(term);

          if (i !== currentTermPath.length - 1) {
            children = term.children;
          }
        }
      }
    }


    var html = '';

    // Kick off the term rendering
    for (var i = 0; i < tree.children.length; i++) {
      html += renderTerm(tree.children[i]);
    }

    // Append the create HTML to the bottom of the page
    var list = document.createElement('ul');
    list.setAttribute("id", "tree");
    list.innerHTML = html;
    $('input#txtNonconformance').after(list);

    function renderTerm(term) {

      var html = '<li><div>' + term.title + '</div>';

      if (term.children && term.children.length) {
        html += '<ul>';

        for (var i = 0; i < term.children.length; i++) {
          html += renderTerm2(term.children[i]);
        }

        html += '</ul>';
      }


      return html + '</li>';
    }

    function renderTerm2(term) {

      var html = '<li><div>' + term.title + '</div>';

      if (term.children && term.children.length) {
        html += '<ul>';

        for (var i = 0; i < term.children.length; i++) {
          html += renderTerm(term.children[i]);
        }

        html += '</ul>';
      }


      return html + '</li>';
    }

    setTimeout(createTreeView($("input[id^='txtNonconformance']")[0]), 1000);


  }, function onFailure(args, msg) {
    alert('Error: ' + msg.get_message());
  });

}



function getCategoryTerms(txtInput) {
  var clientContext = SP.ClientContext.get_current();
  var taxonomySession = SP.Taxonomy.TaxonomySession.getTaxonomySession(clientContext);
  var termStores = taxonomySession.get_termStores();
  var termStore = taxonomySession.getDefaultSiteCollectionTermStore();
  //GUID of Term Set from which to get the Terms.  
  var termSet;

  termSet = termStore.getTermSet(Application["TermStore"]["Source"]["BRU"]);

  var terms = termSet.getAllTerms();
  clientContext.load(terms);
  clientContext.executeQueryAsync(function onSuccess() {
    var title = [];
    var txtTitle = [];
    var enumerator = terms.getEnumerator();
    while (enumerator.moveNext()) {
      var spTerm = enumerator.get_current();
      var items2 = spTerm.get_pathOfTerm().split(';');
      if (items2[1] != null && items2[2] != null) {
        txtTitle.push({
          "Label": items2[2],
          "category": items2[1]
        });
      }

    }

    var itemSorted = txtTitle.sort(custom_sort);

    var query = $('input[id^="txtSource"]').val().trim();

    function filterItems(query) {
      return itemSorted.filter(function (el) {
        return el["category"].toLowerCase().indexOf(query.toLowerCase()) > -1;
      })
    }

    itemSorted = filterItems(query);

    function custom_sort(a, b) {
      if (a.category < b.category) return -1;
      if (a.category > b.category) return 1;
      return 0;
    }

    InitializeInput(itemSorted, txtInput);

  }, function onFailure(args, msg) {
    console.log('Error: ' + msg.get_message());
  });
}

function getSourceTerms(Initiator) {
  var clientContext = SP.ClientContext.get_current();
  var taxonomySession = SP.Taxonomy.TaxonomySession.getTaxonomySession(clientContext);
  var termStores = taxonomySession.get_termStores();
  var termStore = taxonomySession.getDefaultSiteCollectionTermStore();
  //GUID of Term Set from which to get the Terms.  
  var termSet;
  if (Initiator.toUpperCase() == "BRU") {

    termSet = termStore.getTermSet(Application["TermStore"]["Source"]["BRU"]);
  } else {
    termSet = termStore.getTermSet(Application["TermStore"]["Source"]["IQA"]);
  }
  var terms = termSet.getAllTerms();
  clientContext.load(terms);
  clientContext.executeQueryAsync(function onSuccess() {
    var title = [];
    var txtTitle = [];
    var enumerator = terms.getEnumerator();
    while (enumerator.moveNext()) {
      var spTerm = enumerator.get_current();
      var items2 = spTerm.get_pathOfTerm().split(';');
      if (items2[0] != null && items2[1] != null && items2[2] == null) {
        txtTitle.push({
          "Label": items2[1],
          "category": items2[0]
        });
      }
    }


    // txtTitle.filter(onlyUnique);
    var itemSorted = txtTitle.sort(custom_sort)

    function custom_sort(a, b) {
      if (a.category < b.category) return -1;
      if (a.category > b.category) return 1;
      return 0;
    }


    InitializeInput(itemSorted, "txtSource");

  }, function onFailure(args, msg) {
    console.log('Error: ' + msg.get_message());
  });
}

function getSingleTerms(termName, input) {
  var clientContext = SP.ClientContext.get_current();
  var taxonomySession = SP.Taxonomy.TaxonomySession.getTaxonomySession(clientContext);
  var termStores = taxonomySession.get_termStores();
  //Name of the Term Store from which to get the Terms.  
  // var termStore = termStores.getByName("Taxonomy_NXoiZ8+XvIL1gqXB/1MIJQ==");  
  var termStore = taxonomySession.getDefaultSiteCollectionTermStore();

  //GUID of Term Set from which to get the Terms.  
  var termSet = termStore.getTermSet(termName);
  var terms = termSet.getAllTerms();
  clientContext.load(terms);
  clientContext.executeQueryAsync(function onSuccess() {
    var title = [];
    var txtTitle = [];
    var enumerator = terms.getEnumerator();

    while (enumerator.moveNext()) {
      var spTerm = enumerator.get_current();
      var items2 = spTerm.get_pathOfTerm().split(';');
      if (items2[0] != null && items2[1] != null) {
        txtTitle.push({
          "Label": items2[1],
          "category": items2[0]
        });
      }
    }
    var itemSorted = txtTitle.sort(custom_sort);

    function custom_sort(a, b) {
      if (a.category < b.category) return -1;
      if (a.category > b.category) return 1;
      return 0;
    }

    InitializeInput(itemSorted, input);

  }, function onFailure(args, msg) {
    console.log('Error: ' + msg.get_message());
  });
}

function getTerms() {
  var clientContext = SP.ClientContext.get_current();
  var taxonomySession = SP.Taxonomy.TaxonomySession.getTaxonomySession(clientContext);
  var termStores = taxonomySession.get_termStores();
  var termStore = taxonomySession.getDefaultSiteCollectionTermStore();
  var termSet = termStore.getTermSet(Application["TermStore"]["AreasAudited"]);
  var terms = termSet.getAllTerms();
  clientContext.load(terms);
  clientContext.executeQueryAsync(function onSuccess() {
    var title = [];
    var txtTitle = [];
    var enumerator = terms.getEnumerator();
    while (enumerator.moveNext()) {
      var spTerm = enumerator.get_current();
      var items2 = spTerm.get_pathOfTerm().split(';');
      if (items2[0] != null && items2[1] != null) {
        txtTitle.push({
          "Label": items2[1],
          "category": items2[0]
        });
      }
    }
    var itemSorted = txtTitle.sort(custom_sort);

    function custom_sort(a, b) {
      if (a.category < b.category) return -1;
      if (a.category > b.category) return 1;
      return 0;
    }

    InitializeInput(itemSorted, "txtAreaAudited");

  }, function onFailure(args, msg) {
    alert('Error: ' + msg.get_message());
  });
}

function InitializeInput(data, inputId) {
  $(function () {
    $.widget("custom.catcomplete", $.ui.autocomplete, {
      _create: function () {
        this._super();
        this.widget().menu("option", "items", "> :not(.ui-autocomplete-category)");
      },
      _renderMenu: function (ul, items) {
        var that = this,
          currentCategory = "";
        $.each(items, function (index, item) {
          var li;
          if (item.category != currentCategory) {
            ul.append("<li class='ui-autocomplete-category'>" + item.category + "</li>");
            ul.addClass("ms-Dropdown-items");
            ul.addClass("width", "424px");
            currentCategory = item.category;
          }
          li = that._renderItemData(ul, item).addClass(item.category);
          if (item.category) {
            li.attr("aria-label", item.category + " : " + item.label);
            li.addClass("ms-Dropdown-item");
          }
        });
      }
    });
    var items = [];
    for (var i = 0; i < data.length; i++) {
      if (data[i]["Label"] != "") {
        items.push({
          label: data[i]["Label"],
          category: data[i]["category"]
        });
      }
    }

    if (inputId.toUpperCase() != "TXTAREAAUDITED" || inputId.toUpperCase() != "TXTAUDITOR" || inputId.toUpperCase() != "TXTRISKCATEGORY") {
      $("#" + inputId).catcomplete({
        minLength: 0,
        delay: 0,
        source: items,
        messages: {
          noResults: '',
          results: function () {}
        },
        change: function (ev, ui) {
          if (!ui.item) {
            $(this).val('');
          }
        }
      }).click(function () {
        $(this).catcomplete("search");
        $('.ui-autocomplete').css('max-width', $(this).width() + "px");
      });
    } else {
      $("#" + inputId).catcomplete({
        minLength: 0,
        delay: 0,
        source: items,
        messages: {
          noResults: '',
          results: function () {}
        }
      }).click(function () {
        $(this).catcomplete("search");
        $('.ui-autocomplete').css('max-width', $(this).width() + "px");
      });
    }
  });


}

function InitializeComponents() { //Initialize HTML
  /*Retrieve Last Id from the List and create a unique ID and set to hidden HTML*/
  var caml = "<View><Query><OrderBy><FieldRef Name='ID' Ascending='False' /></OrderBy></Query><ViewFields><FieldRef Name='Title' /></ViewFields><RowLimit>1</RowLimit></View>"
  Utility.RetrieveListItemsById(Application["List"]["NCReport"], caml,
    function (listItemEnumerator) {
      var ReportId = "0000000";
      var CodeId = "0000000";
      while (listItemEnumerator.moveNext()) {
        var listItem = listItemEnumerator.get_current();
        ReportId = listItem.get_item('Title');
      };
      var id = ReportId;
      var padding = '0000000'
      var num = "" + (parseInt(id.match(/\d+/g), 10) + 1);
      var newId = 'NCCAR-' + padding.substring(0, padding.length - num.length) + num;
      console.log("Report ID: " + newId);
      $('input[id^="txtReportId"]').val(newId);

      var code = CodeId;
      padding = '000000';
      var num2 = "" + (parseInt(code.match(/\d+/g), 10) + 1);
      //var newCodeId = Application["Series"]["Name"] + "-" + padding.substring(0,padding.length-num2.length) + num2;	 
      var newCodeId = Application["Series"]["Name"] + "-XXXX"
      $('input[id^="txtNCNumber"]').val(newCodeId);
    });

  /*Initialize fabric components*/
  var ChoiceFieldGroupElements = document.querySelectorAll(".ms-ChoiceFieldGroup");
  for (var i = 0; i < ChoiceFieldGroupElements.length; i++) {
    new fabric['ChoiceFieldGroup'](ChoiceFieldGroupElements[i]);
  }

  InitiateAllPeople();
  $('input[id^="txtReportType"]').val('BRU');
  $('#NCAssigneeDiv').css("display", "block");
  getSourceTerms("BRU");

  getSingleTerms("4bb3693f-e34d-45ec-a034-414f80d1aeeb", "txtRiskCategory");
  getSingleTerms("2b3036e3-f084-431d-b435-b292485bfdf3", "txtAuditor");

  getCategoryTerms("txtCategory");
  InitiateProgressMeter("BRU");

  if ($.fn.DatePicker) {
    $('.ms-DatePicker').DatePicker({
      format: 'mm/dd/yyyy'
    });
  }

  swapDivs('peoplePickerDiv2', 'peoplePickerDiv4');
  $('#NCAssigneeDiv').css('display', 'none');
  $('div[id $= "_TopSpan"]').addClass('ms-TextField-field');
  $('#btnDateCreatedToday').click();
  getTerms();
  getClauseOfStandardTerms();

  InitializeComponentsEvents();
  setTimeout(function () {
    ExecuteOrDelayUntilScriptLoaded(SetCurrentUsernameToPeoplePicker, "sp.js", "clientpeoplepicker.js"); //set the current user to the picker
  }, 1000);
  checkIfAdmin();
}

function swapDivs(firstDivId, secondDivId) {
  var div1 = jQuery('#' + firstDivId).parent('div');
  var div2 = jQuery('#' + secondDivId).parent('div');

  tdiv1 = div1.clone();
  tdiv2 = div2.clone();

  if (!div2.is(':empty')) {
    div1.replaceWith(tdiv2);
    div2.replaceWith(tdiv1);
  }

}

function checkIfAdmin() {
  var xmlhttp = new XMLHttpRequest();

  xmlhttp.open("GET", _spPageContextInfo.webAbsoluteUrl + '/_api/web/currentuser?$expand=Groups');

  xmlhttp.setRequestHeader("Accept", "application/json;odata=verbose");

  xmlhttp.onreadystatechange = function () {
    if (xmlhttp.readyState == XMLHttpRequest.DONE) {
      console.log("disabling ribbon function");
      if (xmlhttp.status == 200) {
        var response = xmlhttp.responseText;
        var jsonitem = JSON.parse(response);
        var isSiteAdmin = jsonitem["d"]["IsSiteAdmin"]
        var groups = jsonitem["d"]["Groups"]["results"]

        var isGroupAdmin = groups.some(function (g) {
          return g["Title"] == "Administrator";
        })


        if (isSiteAdmin || isGroupAdmin) {
          $('div[id $= "_TopSpan"]').addClass('ms-TextField-field');
          $(".sp-peoplepicker-delImage").hide();
          $("input.sp-peoplepicker-editorInput[id^='peoplePickerDiv2']").prop('disabled', true);
        } else {
          $('div[id $= "_TopSpan"]').addClass('ms-TextField-field');
          $(".sp-peoplepicker-delImage").hide();
          $("input.sp-peoplepicker-editorInput[id^='peoplePickerDiv2']").prop('disabled', true);
        }

      } else {
        console.log('Error: ' + xmlhttp.statusText)
      }
    }
  }

  xmlhttp.send();
}

function InitiateAllPeople() {
  var xmlhttp = new XMLHttpRequest();

  xmlhttp.open("GET", _spPageContextInfo.webAbsoluteUrl + '/_api/web/currentuser?$expand=Groups');

  xmlhttp.setRequestHeader("Accept", "application/json;odata=verbose");

  xmlhttp.onreadystatechange = function () {
    if (xmlhttp.readyState == XMLHttpRequest.DONE) {
      console.log("disabling ribbon function");
      if (xmlhttp.status == 200) {
        var response = xmlhttp.responseText;
        var jsonitem = JSON.parse(response);
        var isSiteAdmin = jsonitem["d"]["IsSiteAdmin"]
        var groups = jsonitem["d"]["Groups"]["results"]

        var isGroupAdmin = groups.some(function (g) {
          return g["Title"] == "Administrator";
        })


        if (isSiteAdmin || isGroupAdmin) {
          initializePeoplePicker('peoplePickerDiv', -1);
          initializePeoplePicker('peoplePickerDiv2', -1);
          initializePeoplePicker('peoplePickerDiv3', -1);
          initializePeoplePicker('peoplePickerDiv4', -1);
          $('div[id $= "_TopSpan"]').addClass('ms-TextField-field');
          initializePeoplePicker2('peopleCC', -1);
        } else {
          initializePeoplePicker('peoplePickerDiv', 0);
          initializePeoplePicker('peoplePickerDiv2', 0);
          initializePeoplePicker('peoplePickerDiv3', 0);
          initializePeoplePicker('peoplePickerDiv4', 0);
          $('div[id $= "_TopSpan"]').addClass('ms-TextField-field');
          initializePeoplePicker2('peopleCC', -1);
        }

      } else {
        console.log('Error: ' + xmlhttp.statusText)
      }
    }
  }

  xmlhttp.send();
}

function InitializeComponentsEvents() { //add events to the elements

  $("a#addCC").on("click", function () {
    $('div#divCC').toggle();
    $('#peopleCC').find('div > div[id $= "_TopSpan"]').removeClass('ms-TextField-field');
    $('#peopleCC').find('div > div[id $= "_TopSpan"]').addClass('ms-TextField--textFieldUnderlined ');
  });

  $('input[id^="txtSource"]').focusout(function () {
    for (var i = 0; i < $('input[id^="txtCategory"]').length; i++) {
      if (i == 0) {
        getCategoryTerms("txtCategory");
      } else {
        var inputid = "txtCategory_" + i;
        console.log(inputid);
        getCategoryTerms(inputid);
      }
    }

  });

  $('#txtAttachments').change(function () {

    var renderItems = true;
    var inputs = $(this);
    var sFileName;
    var _validFileExtensions = ValidFiles;

    FileLoop:
      for (var i = 0; i < $(this).get(0).files.length; i++) {
        var blnValid = false;
        sFileName = $(this).get(0).files[i].name;
        Extension:
          for (var j = 0; j < _validFileExtensions.length; j++) {
            var sCurExtension = _validFileExtensions[j];
            var sFileExtension = getFileExtension3(sFileName)
            if (sFileExtension.toLowerCase() === sCurExtension.toLowerCase()) {
              blnValid = true;
            }
          }

        if (!blnValid) {
          alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
          inputs.replaceWith(inputs.val('').clone(true));
          renderItems = false;
          break FileLoop;
        }

      }

    if (renderItems) {

      var layout = "";
      $(this).next('div').find('div').find('div[id^="dummyLink"]').remove();
      for (var i = 0; i < $(this).get(0).files.length; i++) {
        var fileName = $(this).get(0).files[i].name;
        var link = Application["SiteName"] + "/Attachments/" + $('input[id^="txtReportId"]').val() + "/" + fileName;
        layout += '<div id="dummyLink">';
        layout += '<span><a  target="_blank" href="' + link + '"><img class="ms-asset-icon ms-rtePosition-4" src="' + getIcon(fileName) + '">' + fileName + '</a></span>';
        layout += '</div>';
      }
      $(this).next('div').find('div').append(layout);
    }

    function getFileExtension3(filename) {
      return filename.slice((filename.lastIndexOf(".") - 1 >>> 0) + 1);
    }

  });

  $('input#btnSaveDraft').click(function () { //button Draft click event
    globalStatus = "NC-Draft";
    var reportType = $('input[id^="txtReportType"]').val();
    if (reportType.toUpperCase() == "BRU") {
      SaveBRUReport('NC-Draft');
    } else {
      SaveReSaveReport('NC-Draft');
    }



  });

  $('input#btnSaveReport').click(function () { //button Save Report click event
    var reportType = $('input[id^="txtReportType"]').val();
    globalStatus = "NC-For Review";
    if (reportType.toUpperCase() == "BRU") {
      SaveBRUReport('NC-Assigned')
    } else {
      SaveReport('NC-For Review');
    }

  });

  $('input#btnAddReport').click(function () {

    var currentIndex = $("input[id^='txtNCNumber']").length;
    var formAttributes = {
      FormId: 'NCReport',
      FormDestination: 'NCReport',
      AddLine: true,
      isNewForm: true,
      CurrentIndex: currentIndex
    };
    Utility.CloneForm(formAttributes);

    var id = $($('input[id^="txtNCNumber"]')[currentIndex - 1]).val();
    var padding = '000000'
    var num = "" + (parseInt(id.match(/\d+/g), 10) + 1);
    //	EmpId = 'NC-SCLM-' + padding.substring(0,padding.length-num.length) + num;	 
    EmpId = Application["Series"]["Name"] + '-XXXX';
    $($('input[id^="txtNCNumber"]')[currentIndex]).val(EmpId);
    $('input[id^="txtStatus"]').val('New');

    $($('input[id^="txtNonconformance"]')[currentIndex]).val('');
    $($('input[id^="txtAttachments"]')[currentIndex]).next('div').find('div').find('div[id^="dummyLink"]').remove();
    $($('input[id^="txtAttachments"]')[currentIndex]).val('');
    $($('input[id^="txtCategory"]')[currentIndex]).attr('id', "txtCategory_" + currentIndex);
    $($('input[id^="txtNonconformance"]')[currentIndex]).attr('id', "txtNonconformance_" + currentIndex);
    var treeView = "tree_" + currentIndex;
    var currentCategory = "txtCategory_" + currentIndex;
    var currentNonConf = "txtNonconformance_" + currentIndex;

    getCategoryTerms(currentCategory);
    createTreeView($('#' + currentNonConf));

    $('input[id^="txtAttachments"]').change(function () {

      var renderItems = true;
      var inputs = $(this);
      var sFileName;
      var _validFileExtensions = ValidFiles;

      FileLoop:
        for (var i = 0; i < $(this).get(0).files.length; i++) {
          var blnValid = false;
          sFileName = $(this).get(0).files[i].name;
          Extension:
            for (var j = 0; j < _validFileExtensions.length; j++) {
              var sCurExtension = _validFileExtensions[j];
              var sFileExtension = getFileExtension3(sFileName)
              if (sFileExtension.toLowerCase() === sCurExtension.toLowerCase()) {
                blnValid = true;
              }
            }

          if (!blnValid) {
            alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
            inputs.replaceWith(inputs.val('').clone(true));
            renderItems = false;
            break FileLoop;
          }

        }

      if (renderItems) {

        var layout = "";
        $(this).next('div').find('div').find('div[id^="dummyLink"]').remove();
        for (var i = 0; i < $(this).get(0).files.length; i++) {
          var fileName = $(this).get(0).files[i].name;
          var link = Application["SiteName"] + "/Attachments/" + $('input[id^="txtReportId"]').val() + "/" + fileName;
          layout += '<div id="dummyLink">';
          layout += '<span><a  target="_blank" href="' + link + '"><img class="ms-asset-icon ms-rtePosition-4" src="' + getIcon(fileName) + '">' + fileName + '</a></span>';
          layout += '</div>';
        }
        $(this).next('div').find('div').append(layout);
      }

      function getFileExtension3(filename) {
        return filename.slice((filename.lastIndexOf(".") - 1 >>> 0) + 1);
      }

    });

    return false;

  });
}

function createTreeView(input) {
  var tree = new treefilter($(input).next("#tree"), {
    searcher: input,
    expanded: true,
    multiselect: false
  });

  $(input).focusin(function () {
    $(this).next('ul[id^="tree"]').css("display", "block");
  })


  $(input).next('ul[id="tree"]').find('li').click(function (e) {


    if ($(this).children("ul").length > 0) {
      // e.stopPropagation();
      //$(this).children('ul').toggle();
    } else {
      var sample = $(this).children().clone() //clone the element
        .children() //select all the children
        .remove() //remove all the children
        .end() //again go back to selected element
        .text();
      var index = $(this).parent('ul[id^="tree"]')[0];
      $(input).val(sample);
      $(input).next('ul[id="tree"]').css("display", "none");
    }
  })

  $(input).next('ul[id="tree"]').hover(function () {
    $(this).css("display", "block");
  }, function () {
    $(this).css("display", "none");
  })
}

$(document).ready(function () {
  InitializeComponents();
});
