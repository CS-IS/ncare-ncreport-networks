var Utility = SharePoint.Utility;
var SiteUrl = window.location.protocol + "//" + window.location.host + _spPageContextInfo.siteServerRelativeUrl;
var ValidFiles = Application.ValidFiles;
var Application = Application.AppConfig;
var dialog;
var BRUAllItemLink = Application["Link"]["BRU"]
var IQAAllItemLink = Application["Link"]["IQA"];
var BRUDraftLink = Application["Link"]["BRUDraft"];
var IQADraftLink = Application["Link"]["IQADraft"];
var itemsToDelete = [];
var CommentFlagValue = false;

function showCancelButton(displayname) {
  var internalAuditor = getUserInfo("peoplePickerDiv");
  if (internalAuditor.includes(displayname)) {
    $('#btnSaveReport').after('<a class="tooltipbutton expand" data-title="Marks the report as cancelled"><input class="ms-Button ms-Button--primary" id="btnCloseReport" type="button" value="Cancel Report" style="font-size: small; font-family: inherit; color: #854442; font-weight: 600; background-color: white; border-color: #854442; border-radius: 5px;width:99px;font-size:11px;float: right;"></a>');
    $('input#btnCloseReport').click(function () { //button Save Report click event
      var comments = $('#txtComments').val().trim();
      if (comments.length > 0) { //Add Confirmation Message
        var html = document.createElement('div');
        html.innerHTML = '<h2>Comments below will not be saved. If you wish to save the comment click Add Comment first otherwise continue. </h2>\
        <br>\
        <div class="ms-core-form-section"><div class="ms-core-form-bottomButtonBox"><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.OK); return false;">Continue</button><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.cancel); return false;">Cancel</button>\
        </div></div>'
        OpenPopUpPageWithDialogOptions({
          title: "Confirmation",
          html: html,
          dialogReturnValueCallback: function (dialogResult) {
            if (dialogResult == 1) {
              SavingItemByStatus("Cancelled");
            }
          }
        });
      } else {
        SavingItemByStatus("Cancelled");
      }
    });
  }
}

function retrieveCommentSPA() {
  $('#CommentDiv').empty();
  var ID = getQueryString("ID");
  var comments = retrieveComment('NCReport', ID, 'Comments', true);
  var layout = createCommentLayout(comments);
  if (layout == "") {
    $('#CommentDiv').parent('div').css('display', 'none');
  } else {
    $('#CommentDiv').parent('div').css('display', 'block');
  }
  $('#CommentDiv').append(layout).fadeIn(1000);

  function getQueryString(key) {
    var regex = new RegExp('[\\?&amp;]' + key + '=([^&amp;#]*)');
    var qs = regex.exec(window.location.href);
    return qs[1];
  }

  function createCommentLayout(data) {
    var layout = "";
    var groupedData = _.groupBy(data, function (d) {
      return d.Modified
    });

    function custom_sort(a, b) {
      return new Date(a.Modified).getTime() - new Date(b.Modified).getTime();
    }

    var newData = data; //data.sort(custom_sort);

    var groupedData = _.groupBy(newData, function (d) {
      return d.Modified
    });
    var header = [];
    for (var key in groupedData) {
      if (groupedData.hasOwnProperty(key)) {
        console.log(key);
        header.push(key);
      }
    }

    for (var i = 0; i < header.length; i++) {
      var dat = groupedData[header[i]];
      layout += '<div id="content"><h2 class="Intro-title ms-fontSize-ml">' + header[i] + '</h2>';
      layout += '<br/>';
      for (var j = 0; j < dat.length; j++) {
        console.log("Date: " + dat[j]["Modified"] + " Name: " + dat[j]["Name"]);
        layout += '<div class="ms-Persona ms-Persona--sm">'
        layout += '<div class="ms-Persona-imageArea">'
        layout += '<img class="ms-Persona-image" src="/_layouts/15/userphoto.aspx?size=L&username=' + dat[j]["Email"] + '">'
        layout += '</div>';
        layout += '<div class="ms-Persona-details">';
        layout += '<div class="ms-Persona-primaryText">' + dat[j]["FieldName"] + '</div>';
        layout += '<div class="ms-Persona-secondaryText">' + dat[j]["Name"] + '</div>';
        layout += '<div class="ms-Persona-optionalText">' + dat[j]["CustomDate"] + '</div>';
        layout += '</div>';
        layout += '</div>';
        layout += '<br/>';
      }
    }

    return layout;
  }
}

function updateComment() {
  //var comments = $($('textarea[id^="txtComments"]')[0]).val();
  var fileAttachmentCounterComment = 0;
  var fileAttachmentUploadComment = 0;
  var ajax = $.Deferred();
  var comments = "";
  var id = $('input[id^="txtCounterId"]').val();
  var flag = true;



  for (var i = 0; i < $("input#txtFileComment").length; i++) {
    fileArray = $("input#txtFileComment")[i].files;
    if (fileArray != undefined || file != null) {
      for (var j = 0; j < fileArray.length; j++) {
        fileAttachmentCounterComment++;
      }
    }
  }

  if (fileAttachmentCounterComment != 0) {
    $('#txtComments').parent('div').siblings('div[id="temp"]').find('div[id="Attachments"]')
    $('#txtComments').parent('div').siblings('div[id="temp"]').find('div[id="Attachments"]').find('a[id^="deleteLink"]').remove();
    $('#txtComments').parent('div').siblings('div[id="temp"]').find('div[id="Attachments"]').find('span[class^="ms-newdocument-iconouter"]').remove();
    $('#txtComments').parent('div').siblings('div[id="temp"]').find('div[id="Attachments"]').find('div[id^="NewdummyLink"]').attr("id", "dummyLink");
    comments = "<label>" + $($('textarea[id^="txtComments"]')[0]).val().trim() + "</label>" + $('#txtComments').parent('div').siblings('div[id="temp"]').find('div[id="Attachments"]').html().trim();
  } else {
    comments = "<label>" + $($('textarea[id^="txtComments"]')[0]).val();
  }

  if (fileAttachmentCounterComment != 0) {
    createFolder("CommentFiles", $('#txtNCNumber').val()); //create a new folder for the new item 
    for (var myIndex = 0; myIndex < $("input[id^='txtFileComment']").length; myIndex++) {
      fileArray = $("input[id^='txtFileComment']")[myIndex].files;
      reportid = $($('input[id^="txtNCNumber"]')[myIndex]).val()
      if (fileArray != undefined || file != null) {
        for (var j = 0; j < fileArray.length; j++) {
          $pnp.sp.web.getFolderByServerRelativeUrl(Application["SiteName"] + "/CommentFiles/" + $('#txtNCNumber').val() + "/").files.add(fileArray[j].name, fileArray[j], true).then((result) => {
            result.file.listItemAllFields.get().then((listItemAllFields) => {
              fileAttachmentUploadComment++;
              if (fileAttachmentUploadComment == fileAttachmentCounterComment) {
                console.log("Next Page . . .");
                $pnp.sp.web.lists.getByTitle("NCReport").items.getById($('input[id^="txtCounterId"]').val()).update({
                  Comments: comments,
                  LatestComment: comments,
                  CommentFlag: flag
                }).then(function (sender, args) {
                  $('#txtComments').parent('div').siblings('div[id="temp"]').find('div[id="Attachments"]').empty();
                  $($('textarea[id^="txtComments"]')[0]).val('');
                  ajax.resolve(sender, args);
                }, function (sender, args) {
                  ajax.reject(sender, args);
                });
              } else {
                console.log("Continue . . .");
              }
            });
          });

        }
      }
    }
  } else {
    $pnp.sp.web.lists.getByTitle("NCReport").items.getById($('input[id^="txtCounterId"]').val()).update({
      Comments: comments,
      LatestComment: comments,
      CommentFlag: flag
    }).then(function (sender, args) {
      ajax.resolve(sender, args);
    }, function (sender, args) {
      ajax.reject(sender, args);
    });
  }

  return ajax;

}

function swapDivs(obj1, obj2) {

  var div1 = jQuery('#' + obj1).siblings('label');
  var div2 = jQuery('#' + obj2).siblings('label');

  tdiv1 = div1.clone();
  tdiv2 = div2.clone();


  if (!div2.is(':empty')) {
    div1.replaceWith(tdiv2);
    div2.replaceWith(tdiv1);
  }
  var tobj1 = document.getElementById(obj1);
  var tobj2 = document.getElementById(obj2);
  var temp = document.createElement("div");
  tobj1.parentNode.insertBefore(temp, tobj1);

  // move obj1 to right before obj2
  tobj2.parentNode.insertBefore(tobj1, tobj2);

  // move obj2 to right before where obj1 used to be
  temp.parentNode.insertBefore(tobj2, temp);

  // remove temporary marker node
  temp.parentNode.removeChild(temp);

  $("a#addCCBRU").css('display', 'block');
  $("a#addCCBRU").on("click", function () {
    $('div#divCC').toggle();
    $('#peopleCC').find('div > div[id $= "_TopSpan"]').removeClass('ms-TextField-field');
    $('#peopleCC').find('div > div[id $= "_TopSpan"]').addClass('ms-TextField--textFieldUnderlined ');
  });

}

function addRemoveAttachmentsButton(FileUploaderId) {

  $(FileUploaderId).next('div').find('div').find('span').each(function () {
    $(this).append('<a id="deleteLink"href="#"><img title="Remove File" src="' + Application["SiteName"] + '/SiteAssets/Styles/Images/removefile.png" class="ms-rtePosition-4" style="width: 15px;height: 15px;margin-left: 5px;"></a>')
  });

  //$("a#deleteLink")
  $(FileUploaderId).next('div').find('div').find('span').find("a#deleteLink").on("click", function () {
    var Attachment = $(this).siblings('a').text();
    //console.log(Attachment);
    var path = $(this).siblings()[0].getAttribute('href');
    if (confirm('Are you sure to delete this item?')) {
      $(this).parent('span').parent('div').remove();
      itemsToDelete.push({
        "Filename": Attachment,
        "Path": path
      });
    }
  });

}

var dfdDelete = $.Deferred();

function deleteSelectedFiles(items) {
  for (var i = 0; i < items.length; i++) {
    var clientContext;
    var oWebsite;
    var file = items[i]["Filename"];
    var fileUrl = items[i]["Path"];
    clientContext = new SP.ClientContext.get_current();
    oWebsite = clientContext.get_web();
    clientContext.load(oWebsite);

    this.fileToDelete = oWebsite.getFileByServerRelativeUrl(fileUrl);
    this.fileToDelete.deleteObject();
  }
  clientContext.executeQueryAsync(function (sender, args) {
    dfdDelete.resolve(sender, args);
    console.log("File Deleted");
  }, function (sender, args) {
    dfdDelete.reject(sender, args);

  });
}

function InitiateProgressMeter(Initiator) {
  var caml = "<View><Query> <OrderBy> <FieldRef Name='Order0' Ascending='True' /> </OrderBy> </Query></View>";
  var ListId = "";
  if (Initiator.toUpperCase() != "BRU") {
    ListId = Application["List"]["IQAStatus"];
  } else {
    //ListId = "BB6EAE36-E849-49D0-9255-8A45D9F3D4BE";
    ListId = Application["List"]["BRUStatus"];
  }

  Utility.RetrieveListItemsById(ListId, caml,
    function (listItemEnumerator) {
      var items = [];
      while (listItemEnumerator.moveNext()) {
        var listItem = listItemEnumerator.get_current();
        items.push({
          "Status": listItem.get_item("Title"),
          "ProgressMark": listItem.get_item("ProgressMark")
        })
      };
      CreateProgressMeter(items);
    });

  function CreateProgressMeter(ItemRecord) {
    var layout = "";

    for (var i = 0; i < ItemRecord.length; i++) {
      layout += "<li id='" + ItemRecord[i]["Status"] + "'><span>" + ItemRecord[i]["ProgressMark"] + "<span></li>";
    }
    $('#ProgressMeter').append(layout);
    var status = $('input[id^="txtStatus"]').val();
    $('#ProgressMeter > li[id^="' + status + '"]').prevAll().addClass('inactive');
    $('#ProgressMeter > li[id^="' + status + '"]').addClass('active');
  }
}

var dfd2 = $.Deferred();

function startWorkflow(itemID, subID) {
  if ($('#txtReportType').val().toUpperCase() != "BRU") {
    if (TempReportStatus.toUpperCase() == "NC-CHALLENGED" || TempReportStatus.toUpperCase() == "CAP-FOR REVISION" || TempReportStatus.toUpperCase() == "NC-ASSIGNED") {
      var context = SP.ClientContext.get_current();
      var web = context.get_web();
      var wfServiceManager = SP.WorkflowServices.WorkflowServicesManager.newObject(context, web);
      var subscription = wfServiceManager.getWorkflowSubscriptionService().getSubscription(subID);

      context.load(subscription);
      context.executeQueryAsync(
        function (sender, args) {
          console.log("Subscription load success. Attempting to start workflow.");
          var inputParameters = {};
          wfServiceManager.getWorkflowInstanceService().startWorkflowOnListItem(subscription, itemID, inputParameters);

          context.executeQueryAsync(
            function (sender, args) {
              //dfd2.resolve(sender, args);
              console.log("Successfully starting workflow.");
            },
            function (sender, args) {
              //dfd2.reject(sender, args);
              console.log("Failed to start workflow.");
              console.log("Error: " + args.get_message() + "\n" + args.get_stackTrace());
            }
          );
        },
        function (sender, args) {
          console.log("Failed to load subscription.");
          console.log("Error: " + args.get_message() + "\n" + args.get_stackTrace());
        }
      );
    }
  }
  else {
    var context = SP.ClientContext.get_current();
    var web = context.get_web();
    var wfServiceManager = SP.WorkflowServices.WorkflowServicesManager.newObject(context, web);
    var subscription = wfServiceManager.getWorkflowSubscriptionService().getSubscription(subID);

    context.load(subscription);
    context.executeQueryAsync(
      function (sender, args) {
        console.log("Subscription load success. Attempting to start workflow.");
        var inputParameters = {};
        wfServiceManager.getWorkflowInstanceService().startWorkflowOnListItem(subscription, itemID, inputParameters);

        context.executeQueryAsync(
          function (sender, args) {
            //dfd2.resolve(sender, args);
            console.log("Successfully starting workflow.");
          },
          function (sender, args) {
            //dfd2.reject(sender, args);
            console.log("Failed to start workflow.");
            console.log("Error: " + args.get_message() + "\n" + args.get_stackTrace());
          }
        );
      },
      function (sender, args) {
        console.log("Failed to load subscription.");
        console.log("Error: " + args.get_message() + "\n" + args.get_stackTrace());
      }
    );
  }
  /* */
}

var dfd = $.Deferred();

function stopWorkflow(listID, itemID) {

  var context = SP.ClientContext.get_current();
  var workflowServicesManager = SP.WorkflowServices.WorkflowServicesManager.newObject(context, context.get_web());
  var workflowInstanceService = workflowServicesManager.getWorkflowInstanceService();
  var wfInstances = workflowInstanceService.enumerateInstancesForListItem(listID, itemID);

  context.load(wfInstances);
  context.executeQueryAsync(
    function (sender, args) {
      var instancesEnum = wfInstances.getEnumerator();
      while (instancesEnum.moveNext()) {
        var instance = instancesEnum.get_current();
        workflowInstanceService.terminateWorkflow(instance);
        context.executeQueryAsync(
          function (sender, args) {
            dfd.resolve(sender, args);
            console.log("Suspend Successful");
          },
          function (sender, args) {
            dfd.reject(sender, args);
            console.log("Failed to terminate workflow.");
            console.log("Error: " + args.get_message() + "\n" + args.get_stackTrace());
          }
        );

      }

    },
    function (sender, args) {
      dfd.reject(sender, args);
      console.log("Failed to load instances.");
      console.log("Error: " + args.get_message() + "\n" + args.get_stackTrace());
    }
  );
  return dfd;
};

function CreateCorrectiveActionPlan(itemList) {
  var clientContext = new SP.ClientContext.get_current();
  var oList = clientContext.get_web().get_lists().getById(Application["List"]["NCReport"]);
  for (var i = 0; i < itemList.length; i++) {
    index = i;
    TempReportStatus = itemList[i]["Status"];
    var oListItem = oList.getItemById(itemList[i]["ID"]);
    for (var column in itemList[i]) {
      if (column.toUpperCase() != "ID") {
        oListItem.set_item(column, itemList[i][column]);
      }
    }
    oListItem.update();
    clientContext.load(oListItem);
  }

  clientContext.executeQueryAsync(onQuerySucceeded, onQueryFailed);

  function onQueryFailed(sender, args) {
    console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
  }

  function onQuerySucceeded() {
    var fileArray = [];
    for (var i = 0; i < $('#CAPDiv').find('input[type = "file"]').length; i++) { // upload files to the folder
      fileArray = $('#CAPDiv').find('input[type = "file"]')[i].files;
      if (fileArray != undefined || file != null) {
        for (var j = 0; j < fileArray.length; j++) {
          fileAttachmentCounter++; //get the number of attachments;
        }
      }
    }

    console.log("Number of attachments: " + fileAttachmentCounter);
    var folders = ["RCA", "CAP"];

    if (fileAttachmentCounter != 0) {
      $pnp.sp.web.folders.getByName('RCA').folders.add($('#txtNCNumber').val()).then(function (data) {
        console.log("Folder is created at " + data.data.ServerRelativeUrl)
        $pnp.sp.web.folders.getByName('CAP').folders.add($('#txtNCNumber').val()).then(function (data) {
          console.log("Folder is created at " + data.data.ServerRelativeUrl)
          UploadedTest();
        }).catch(function (data) {
          console.log("CAP" + data);
        });
      }).catch(function (data) {
        console.log("RCA" + data);
      });

    } else {
      if (TempReportStatus.toUpperCase() == "NC-CHALLENGED") {
        setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["IQAEmail"]), 2000);
        setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["BRUEmail"]), 2000);
      } else if (TempReportStatus.toUpperCase() == "NC-ASSIGNED") {
        setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["IQAEmail"]), 2000);
        setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["BRUEmail"]), 2000);
      } else if (TempReportStatus.toUpperCase() == "CAP-FOR REVISION") {
        setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["IQAEmail"]), 2000);
        setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["BRUEmail"]), 2000);
      } else if (TempReportStatus.toUpperCase() == "CAP-FOR REVIEW") {
        setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["IQAEmail"]), 2000);
        setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["BRUEmail"]), 2000);
      } else if (TempReportStatus.toUpperCase() == "CAP-FOR AUDITORS ASSESSMENT") {
        setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["IQAEmail"]), 2000);
        setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["BRUEmail"]), 2000);
      } else if (TempReportStatus.toUpperCase() == "CAP-FOR APPROVAL") {
        setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["BRUEmail"]), 1000);
        setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["IQAEmail"]), 2000);
      } else if (TempReportStatus.toUpperCase() == "CAP-FOR IMPLEMENTATION") {
        setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["IQAEmail"]), 2000);
        setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["BRUEmail"]), 2000);
      } else if (TempReportStatus.toUpperCase() == "CAP-FOR FINAL EVALUATION") {
        setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["IQAEmail"]), 2000);
        setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["BRUEmail"]), 2000);
      } else if (TempReportStatus.toUpperCase() == "CAP-RESOLVED") {
        setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["IQAEmail"]), 2000);
        setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["BRUEmail"]), 2000);
      }

      setTimeout(function () {
        if ($('#txtReportType').val().toUpperCase() == "BRU") {
          window.location = BRUAllItemLink;
        } else {
          window.location = IQAAllItemLink;
        }
      }, 3000);

    }

    function UploadedTest() {
      for (var i = 0; i < $('#CAPDiv').find('input[type = "file"]').length; i++) { // upload files to the folder
        fileArray = $('#CAPDiv').find('input[type = "file"]')[i].files;


        if (fileArray != undefined || fileArray != null) {
          for (var j = 0; j < fileArray.length; j++) {
            $pnp.sp.web.getFolderByServerRelativeUrl(Application["SiteName"] + "/" + folders[i] + "/" + $('#txtNCNumber').val() + "/").files.add(fileArray[j].name, fileArray[j], true).then((r) => {
              fileAttachmentUpload++;
              if (fileAttachmentUpload == fileAttachmentCounter) {
                if (TempReportStatus.toUpperCase() == "NC-CHALLENGED") {
                  setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["IQAEmail"]), 2000);
                  setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["BRUEmail"]), 2000);
                } else if (TempReportStatus.toUpperCase() == "NC-ASSIGNED") {
                  setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["IQAEmail"]), 2000);
                  setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["BRUEmail"]), 2000);
                } else if (TempReportStatus.toUpperCase() == "CAP-FOR REVISION") {
                  setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["IQAEmail"]), 2000);
                  setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["BRUEmail"]), 2000);
                } else if (TempReportStatus.toUpperCase() == "CAP-FOR REVIEW") {
                  setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["IQAEmail"]), 2000);
                  setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["BRUEmail"]), 2000);
                } else if (TempReportStatus.toUpperCase() == "CAP-FOR AUDITORS ASSESSMENT") {
                  setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["IQAEmail"]), 2000);
                  setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["BRUEmail"]), 2000);
                } else if (TempReportStatus.toUpperCase() == "CAP-FOR APPROVAL") {
                  setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["BRUEmail"]), 1000);
                  setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["IQAEmail"]), 2000);
                } else if (TempReportStatus.toUpperCase() == "CAP-FOR IMPLEMENTATION") {
                  setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["IQAEmail"]), 2000);
                  setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["BRUEmail"]), 2000);
                } else if (TempReportStatus.toUpperCase() == "CAP-FOR FINAL EVALUATION") {
                  setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["IQAEmail"]), 2000);
                  setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["BRUEmail"]), 2000);
                } else if (TempReportStatus.toUpperCase() == "CAP-RESOLVED") {
                  setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["IQAEmail"]), 2000);
                  setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["BRUEmail"]), 2000);
                }

                setTimeout(function () {
                  if ($('#txtReportType').val().toUpperCase() == "BRU") {
                    window.location = BRUAllItemLink;
                  } else {
                    window.location = IQAAllItemLink;
                  }
                }, 1000);

              } else {
                console.log("Continue ...");
              }
            }, (f) => {
              console.log(f);
            });
          }
        }
      }
    }
  }
}


function CreateCorrectiveActionPlan2(itemList) {
  var clientContext = new SP.ClientContext.get_current();
  var oList = clientContext.get_web().get_lists().getById(Application["List"]["NCReport"]);
  for (var i = 0; i < itemList.length; i++) {
    index = i;
    TempReportStatus = itemList[i]["Status"];
    var oListItem = oList.getItemById(itemList[i]["ID"]);
    for (var column in itemList[i]) {
      if (column.toUpperCase() != "ID") {
        oListItem.set_item(column, itemList[i][column]);
      }
    }
    oListItem.update();
    clientContext.load(oListItem);
  }

  clientContext.executeQueryAsync(onQuerySucceeded, onQueryFailed);

  function onQueryFailed(sender, args) {
    console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
  }

  function onQuerySucceeded() {

    var fileArray = [];
    if (fileAttachmentCounter <= 0) {
      for (var i = 0; i < $('#CAPDiv').find('input[type = "file"]').length; i++) { // upload files to the folder
        fileArray = $('#CAPDiv').find('input[type = "file"]')[i].files;
        if (fileArray != undefined || file != null) {
          for (var j = 0; j < fileArray.length; j++) {
            fileAttachmentCounter++; //get the number of attachments;
          }
        }
      }
    }

    console.log("Number of attachments: " + fileAttachmentCounter);
    var folders = ["RCA", "CAP"];

    if (fileAttachmentCounter != 0) {
      /*  for (var i = 0; i < $('#CAPDiv').find('input[type = "file"]').length; i++) {
         createFolder(folders[i], $('#txtNCNumber').val());
       }
       setTimeout(function(){
         UploadedTest()
       }, 3000); */
      $pnp.sp.web.folders.getByName('RCA').folders.add($('#txtNCNumber').val()).then(function (data) {
        $pnp.sp.web.folders.getByName('CAP').folders.add($('#txtNCNumber').val()).then(function (data) {
          console.log("Folder is created at " + data.data.ServerRelativeUrl)
          UploadedTest();
        }).catch(function (data) {
          console.log("CAP" + data);
        });
      }).catch(function (data) {
        console.log("RCA" + data);
      });
    } else {

      setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["IQAEmail"]), 1000);
      setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["BRUEmail"]), 1000);
      setTimeout(function () {
        if ($('#txtReportType').val().toUpperCase() == "BRU") {
          window.location = BRUAllItemLink;
        } else {
          window.location = IQAAllItemLink;
        }
      }, 2000);
    }

    function UploadedTest() {
      for (var i = 0; i < $('#CAPDiv').find('input[type = "file"]').length; i++) { // upload files to the folder
        fileArray = $('#CAPDiv').find('input[type = "file"]')[i].files;
        //createFolder(folders[i], $('#txtNCNumber').val());
        if (fileArray != undefined || fileArray != null) {
          for (var j = 0; j < fileArray.length; j++) {
            $pnp.sp.web.getFolderByServerRelativeUrl(Application["SiteName"] + "/" + folders[i] + "/" + $('#txtNCNumber').val() + "/").files.add(fileArray[j].name, fileArray[j], true).then((r) => {
              fileAttachmentUpload++;
              if (fileAttachmentUpload == fileAttachmentCounter) {
                setTimeout(function () {
                  setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["IQAEmail"]), 2000);
                  setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["BRUEmail"]), 2000);
                  if ($('#txtReportType').val().toUpperCase() == "BRU") {
                    window.location = BRUAllItemLink;
                  } else {
                    window.location = IQAAllItemLink;
                  }
                }, 1000);

              } else {
                console.log("Continue ...");
              }
            }, (f) => {
              console.log(f);
            });
          }
        }
      }
    }
  }
}

function CreateAssigneeList(itemList) { //save items to list copied from util
  var clientContext = new SP.ClientContext.get_current();
  var oList = clientContext.get_web().get_lists().getById(Application["List"]["NCReport"]);
  for (var i = 0; i < itemList.length; i++) {
    index = i;
    var oListItem = oList.getItemById(itemList[i]["ID"]);
    for (var column in itemList[i]) {
      if (column.toUpperCase() != "ID") {
        oListItem.set_item(column, itemList[i][column]);
      }
    }

    oListItem.update();
    clientContext.load(oListItem);
  }
  clientContext.executeQueryAsync(onQuerySucceeded, onQueryFailed);

  function onQuerySucceeded() {

    var folder = $('#txtReportId').val();
    var fileArray = [];
    createFolder("Attachments", $('#txtReportId').val()); //create a new folder for the new item 

    for (var i = 0; i < $("input#txtAttachments").length; i++) { // upload files to the folder
      fileArray = $("input#txtAttachments")[i].files;
      if (fileArray != undefined || file != null) {
        for (var j = 0; j < fileArray.length; j++) {
          fileAttachmentCounter++; //get the number of attachments;
        }
      }
    }

    console.log("Number of attachments: " + fileAttachmentCounter);

    if (fileAttachmentCounter != 0) {
      for (var myIndex = 0; myIndex < $("input[id^='txtAttachments']").length; myIndex++) {
        fileArray = $("input[id^='txtAttachments']")[myIndex].files;
        reportid = $($('input[id^="txtNCNumber"]')[myIndex]).val()
        if (fileArray != undefined || file != null) {
          for (var j = 0; j < fileArray.length; j++) {
            $pnp.sp.web.getFolderByServerRelativeUrl(Application["SiteName"] + "/Attachments/" + $('#txtReportId').val() + "/").files.add(fileArray[j].name, fileArray[j], true).then((result) => {
              result.file.listItemAllFields.get().then((listItemAllFields) => {
                fileAttachmentUpload++;
                if (fileAttachmentUpload == fileAttachmentCounter) {
                  console.log("Next Page . . .");
                  setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["IQAEmail"]), 3000);
                  var globalStatus = $('#txtStatus').val();
                  if ($('#txtReportType').val().toUpperCase() == "BRU") {
                    if (globalStatus.toUpperCase() == "NC-DRAFT") {
                      window.location = BRUDraftLink;
                    } else {
                      window.location = BRUAllItemLink;
                    }
                  } else {
                    if (globalStatus.toUpperCase() == "NC-DRAFT") {
                      window.location = IQADraftLink;
                    } else {
                      window.location = IQAAllItemLink;
                    }
                  }
                } else {
                  console.log("Continue . . .");
                }
              });
            });

          }
        }
      }
    } else {
      setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["IQAEmail"]), 2000);
      setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["BRUEmail"]), 2000);
      setTimeout(function () {

        if ($('#txtReportType').val().toUpperCase() == "BRU") {
          window.location = BRUAllItemLink;
        } else {
          window.location = IQAAllItemLink;
        }
      }, 4000);
    }
  }

  function onQueryFailed(sender, args) {
    console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
  }

}

function UpdateCorrectiveActionPlan2(ReportStatus) {

  var Utility = SharePoint.Utility;

  var validation = {
    Fields: {
      txtCAPComments: {
        required: true
      },
      dateImplementationDate: {
        required: true
      },
      txtImplementationDays: {
        required: true
      }
    },
    FieldType: {
      txtCAPComments: {
        type: "textarea"
      },
      dateImplementationDate: {
        type: "datePicker"
      },
      txtImplementationDays: {
        type: "input"
      }
    },
    message: {
      txtCAPComments: {
        required: "This field is required"
      },
      dateImplementationDate: {
        required: "This field is required"
      },
      txtImplementationDays: {
        required: "This field is required"
      }
    }

  }

  if (Utility.Validate(validation)) {
    dialog = SP.SOD.execute('sp.ui.dialog.js', 'SP.UI.ModalDialog.showWaitScreenWithNoClose', "Saving Report", "Please Wait");
    var ReportList = [];
    var rca, cap, CAPComment;
    var Id = $('input[id^="txtCounterId"]').val();
    var noOfDays = 10;
    if ($('input[id^="txtImplementationDays"]').val() != "") {
      noOfDays = $('input[id^="txtImplementationDays"]').val();
    }

    $('#txtRAC').next('div').find('a[id^="deleteLink"]').remove();
    $('#txtRAC').next('div').find('span[class^="ms-newdocument-iconouter"]').remove();
    $('#txtRAC').next('div').find('div[id^="NewdummyLink"]').attr("id", "dummyLink");

    rca = $($($('input[id^="txtRAC"]')[0]).siblings('div')[0]).html().trim();

    $('#txtCAP').next('div').find('a[id^="deleteLink"]').remove();
    $('#txtCAP').next('div').find('span[class^="ms-newdocument-iconouter"]').remove();
    $('#txtCAP').next('div').find('div[id^="NewdummyLink"]').attr("id", "dummyLink");

    cap = $($($('input[id^="txtCAP"]')[0]).siblings('div')[0]).html().trim();
    CAPComment = $($('textarea[id^="txtCAPComments"]')[0]).val();

    var implementationDate = new Date($('input[id^="dtImplementationDate"').val());
    var penalties = $('input[id^="txtPenalties"]').val();
    var endorser = getUserInfo("peoplePickerDiv2");
    var creator = getUserInfo("peoplePickerDiv");
    var recipient = getUserInfo("peoplePickerDiv3");
    var ncassignee = getUserInfo("peoplePickerDiv4");
    var actor = null;
    if ($('input[id^="txtReportType"]').val().toUpperCase() != "BRU") {
      switch (ReportStatus.toUpperCase()) {
        case "NC-DRAFT":
        case "NC-FOR REVISION":
        case "CAP-FOR AUDITORS ASSESSMENT":
        case "CAP-FOR FINAL EVALUATION":
          actor = creator;
          break;
        case "NC-FOR REVIEW":
        case "NC-CHALLENGED":
        case "CAP-FOR APPROVAL":
        case "CAP-RESOLVED":
          actor = endorser;
          break;
        case "NC-ISSUED":
        case "CAP-FOR REVIEW":
          actor = recipient;
          break;
        case "NC-ASSIGNED":
        case "CAP-FOR REVISION":
        case "CAP-FOR IMPLEMENTATION":
          actor = ncassignee;
          break;
        default:
          actor = null;
      }
    } else {
      switch (ReportStatus.toUpperCase()) {
        case "NC-DRAFT":
        case "CAP-FOR APPROVAL":
        case "CAP-FOR FINAL EVALUATION":
          actor = creator;
          break;
        case "NC-ASSIGNED":
        case "CAP-FOR REVISION":
        case "CAP-FOR IMPLEMENTATION":
        case "CAP-RESOLVED":
          actor = ncassignee;
          break;
        default:
          actor = null;
      }
    }
    ReportList.push({
      "ID": Id,
      "Status": ReportStatus,
      "NoOfDaysImplement": noOfDays,
      "CAPAttachment": cap,
      "RCAAttachment": rca,
      "CAPComment": CAPComment,
      "CommentFlag": CommentFlagValue,
      "ImplementationDate": implementationDate,
      "Penalties": penalties,
      "Actor": actor != null && actor != "" ? SP.FieldUserValue.fromUser(actor) : null
    });

    if (itemsToDelete.length > 0) {
      $.when(deleteSelectedFiles(itemsToDelete))
        .done(function (data) {
          CreateCorrectiveActionPlan2(ReportList);
        })
        .fail(function (sender, args) {
          console.log("Error: " + args.get_message() + "\n" + args.get_stackTrace());
        });
    } else {
      CreateCorrectiveActionPlan2(ReportList);
    }
  } else {
    return false;
  }
}



function UpdateCorrectiveActionPlan(ReportStatus) {

  var Utility = SharePoint.Utility;

  var validation = {
    Fields: {
      txtCAPComments: {
        required: true
      },
      dateImplementationDate: {
        required: true
      },
      txtImplementationDays: {
        required: true
      }
    },
    FieldType: {
      txtCAPComments: {
        type: "textarea"
      },
      dateImplementationDate: {
        type: "datePicker"
      },
      txtImplementationDays: {
        type: "input"
      }
    },
    message: {
      txtCAPComments: {
        required: "This field is required"
      },
      dateImplementationDate: {
        required: "This field is required"
      },
      txtImplementationDays: {
        required: "This field is required"
      }
    }

  }

  if (Utility.Validate(validation)) {
    dialog = SP.SOD.execute('sp.ui.dialog.js', 'SP.UI.ModalDialog.showWaitScreenWithNoClose', "Saving Report", "Please Wait");
    var ReportList = [];
    var rca, cap, CAPComment;
    var Id = $('input[id^="txtCounterId"]').val();
    var noOfDays = 10;
    if ($('input[id^="txtImplementationDays"]').val() != "") {
      noOfDays = $('input[id^="txtImplementationDays"]').val();
    }

    $('#txtRAC').next('div').find('a[id^="deleteLink"]').remove();
    $('#txtRAC').next('div').find('span[class^="ms-newdocument-iconouter"]').remove();
    $('#txtRAC').next('div').find('div[id^="NewdummyLink"]').attr("id", "dummyLink");

    rca = $($($('input[id^="txtRAC"]')[0]).siblings('div')[0]).html().trim();

    $('#txtCAP').next('div').find('a[id^="deleteLink"]').remove();
    $('#txtCAP').next('div').find('span[class^="ms-newdocument-iconouter"]').remove();
    $('#txtCAP').next('div').find('div[id^="NewdummyLink"]').attr("id", "dummyLink");


    cap = $($($('input[id^="txtCAP"]')[0]).siblings('div')[0]).html().trim();
    CAPComment = $($('textarea[id^="txtCAPComments"]')[0]).val();

    var penalties = $('input[id^="txtPenalties"]').val();
    var implementationDate = new Date($('input[id^="dtImplementationDate"').val());
    var endorser = getUserInfo("peoplePickerDiv2");
    var creator = getUserInfo("peoplePickerDiv");
    var recipient = getUserInfo("peoplePickerDiv3");
    var ncassignee = getUserInfo("peoplePickerDiv4");

    var actor = null;
    if ($('input[id^="txtReportType"]').val().toUpperCase() != "BRU") {
      switch (ReportStatus.toUpperCase()) {
        case "NC-DRAFT":
        case "NC-FOR REVISION":
        case "CAP-FOR AUDITORS ASSESSMENT":
        case "CAP-FOR FINAL EVALUATION":
          actor = creator;
          break;
        case "NC-FOR REVIEW":
        case "NC-CHALLENGED":
        case "CAP-FOR APPROVAL":
        case "CAP-RESOLVED":
          actor = endorser;
          break;
        case "NC-ISSUED":
        case "CAP-FOR REVIEW":
          actor = recipient;
          break;
        case "NC-ASSIGNED":
        case "CAP-FOR REVISION":
        case "CAP-FOR IMPLEMENTATION":
          actor = ncassignee;
          break;
        default:
          actor = null;
      }
    } else {
      switch (ReportStatus.toUpperCase()) {
        case "NC-DRAFT":
        case "CAP-FOR APPROVAL":
        case "CAP-FOR FINAL EVALUATION":
          actor = creator;
          break;
        case "NC-ASSIGNED":
        case "CAP-FOR REVISION":
        case "CAP-FOR IMPLEMENTATION":
        case "CAP-RESOLVED":
          actor = ncassignee;
          break;
        default:
          actor = null;
      }
    }


    ReportList.push({
      "ID": Id,
      "Status": ReportStatus,
      "NoOfDaysImplement": noOfDays,
      "CAPAttachment": cap,
      "RCAAttachment": rca,
      "CAPComment": CAPComment,
      "CommentFlag": CommentFlagValue,
      "Penalties": penalties,
      "ImplementationDate": implementationDate,
      "Actor": actor != null && actor != "" ? SP.FieldUserValue.fromUser(actor) : null
    });

    if (itemsToDelete.length > 0) {
      $.when(deleteSelectedFiles(itemsToDelete))
        .done(function (data) {
          if (ReportStatus.toUpperCase() == "NC-CHALLENGED") {

            $.when(stopWorkflow(Application["List"]["NCReport"], $('input[id^="txtCounterId"]').val()))
              .done(function (data) {
                CreateCorrectiveActionPlan(ReportList);
              })
              .fail(function (sender, args) {
                console.log("Error: " + args.get_message() + "\n" + args.get_stackTrace());
              });
          } else if (ReportStatus.toUpperCase() == "NC-ASSIGNED") {
            $.when(stopWorkflow(Application["List"]["NCReport"], $('input[id^="txtCounterId"]').val()))
              .done(function (data) {
                CreateCorrectiveActionPlan(ReportList);
              })
              .fail(function (sender, args) {
                console.log("Error: " + args.get_message() + "\n" + args.get_stackTrace());
              });
          } else if (ReportStatus.toUpperCase() == "CAP-FOR REVIEW") {
            $.when(stopWorkflow(Application["List"]["NCReport"], $('input[id^="txtCounterId"]').val()))
              .done(function (data) {
                CreateCorrectiveActionPlan(ReportList);
              })
              .fail(function (sender, args) {
                console.log("Error: " + args.get_message() + "\n" + args.get_stackTrace());
              });
          } else if (ReportStatus.toUpperCase() == "CAP-FOR REVISION") {
            $.when(stopWorkflow(Application["List"]["NCReport"], $('input[id^="txtCounterId"]').val()))
              .done(function (data) {
                CreateCorrectiveActionPlan(ReportList);
              })
              .fail(function (sender, args) {
                console.log("Error: " + args.get_message() + "\n" + args.get_stackTrace());
              });
          } else if (ReportStatus.toUpperCase() == "CAP-FOR AUDITORS ASSESSMENT") {
            $.when(stopWorkflow(Application["List"]["NCReport"], $('input[id^="txtCounterId"]').val()))
              .done(function (data) {
                CreateCorrectiveActionPlan(ReportList);
              })
              .fail(function (sender, args) {
                console.log("Error: " + args.get_message() + "\n" + args.get_stackTrace());
              });
          } else if (ReportStatus.toUpperCase() == "CAP-FOR APPROVAL") {
            $.when(stopWorkflow(Application["List"]["NCReport"], $('input[id^="txtCounterId"]').val()))
              .done(function (data) {
                CreateCorrectiveActionPlan(ReportList);
              })
              .fail(function (sender, args) {
                console.log("Error: " + args.get_message() + "\n" + args.get_stackTrace());
              });
          } else if (ReportStatus.toUpperCase() == "CAP-FOR IMPLEMENTATION") {
            $.when(stopWorkflow(Application["List"]["NCReport"], $('input[id^="txtCounterId"]').val()))
              .done(function (data) {
                CreateCorrectiveActionPlan(ReportList);
              })
              .fail(function (sender, args) {
                console.log("Error: " + args.get_message() + "\n" + args.get_stackTrace());
              });
          } else {
            CreateCorrectiveActionPlan(ReportList);
          }
        })
        .fail(function (sender, args) {
          console.log("Error: " + args.get_message() + "\n" + args.get_stackTrace());
        });
    } else {
      if (ReportStatus.toUpperCase() == "NC-CHALLENGED") {

        $.when(stopWorkflow(Application["List"]["NCReport"], $('input[id^="txtCounterId"]').val()))
          .done(function (data) {
            CreateCorrectiveActionPlan(ReportList);
          })
          .fail(function (sender, args) {
            console.log("Error: " + args.get_message() + "\n" + args.get_stackTrace());
          });
      } else if (ReportStatus.toUpperCase() == "NC-ASSIGNED") {
        $.when(stopWorkflow(Application["List"]["NCReport"], $('input[id^="txtCounterId"]').val()))
          .done(function (data) {
            CreateCorrectiveActionPlan(ReportList);
          })
          .fail(function (sender, args) {
            console.log("Error: " + args.get_message() + "\n" + args.get_stackTrace());
          });
      } else if (ReportStatus.toUpperCase() == "CAP-FOR REVIEW") {
        $.when(stopWorkflow(Application["List"]["NCReport"], $('input[id^="txtCounterId"]').val()))
          .done(function (data) {
            CreateCorrectiveActionPlan(ReportList);
          })
          .fail(function (sender, args) {
            console.log("Error: " + args.get_message() + "\n" + args.get_stackTrace());
          });
      } else if (ReportStatus.toUpperCase() == "CAP-FOR REVISION") {
        $.when(stopWorkflow(Application["List"]["NCReport"], $('input[id^="txtCounterId"]').val()))
          .done(function (data) {
            CreateCorrectiveActionPlan(ReportList);
          })
          .fail(function (sender, args) {
            console.log("Error: " + args.get_message() + "\n" + args.get_stackTrace());
          });
      } else if (ReportStatus.toUpperCase() == "CAP-FOR AUDITORS ASSESSMENT") {
        $.when(stopWorkflow(Application["List"]["NCReport"], $('input[id^="txtCounterId"]').val()))
          .done(function (data) {
            CreateCorrectiveActionPlan(ReportList);
          })
          .fail(function (sender, args) {
            console.log("Error: " + args.get_message() + "\n" + args.get_stackTrace());
          });
      } else if (ReportStatus.toUpperCase() == "CAP-FOR APPROVAL") {
        $.when(stopWorkflow(Application["List"]["NCReport"], $('input[id^="txtCounterId"]').val()))
          .done(function (data) {
            CreateCorrectiveActionPlan(ReportList);
          })
          .fail(function (sender, args) {
            console.log("Error: " + args.get_message() + "\n" + args.get_stackTrace());
          });
      } else if (ReportStatus.toUpperCase() == "CAP-FOR IMPLEMENTATION") {
        $.when(stopWorkflow(Application["List"]["NCReport"], $('input[id^="txtCounterId"]').val()))
          .done(function (data) {
            CreateCorrectiveActionPlan(ReportList);
          })
          .fail(function (sender, args) {
            console.log("Error: " + args.get_message() + "\n" + args.get_stackTrace());
          });
      } else {
        CreateCorrectiveActionPlan(ReportList);
      }
    }



  } else {
    return false;
  }
}

function UpdateNCAssignee(ReportStatus) {

  var Utility = SharePoint.Utility;
  var validation;
  var agingdate = new Date();

  validation = {
    Fields: {
      datePickerAuditDate: {
        required: true
      },
      peoplePickerDiv: {
        required: true
      },
      peoplePickerDiv4: {
        required: true
      },
      txtSource: {
        required: true
      },
      txtSubject: {
        required: true
      },
      txtDescription: {
        required: true
      },
      txtNonconformance: {
        required: true
      },
      txtAreaAudited: {
        required: true
      },
      txtCategory: {
        required: true
      },
      txtAuditor: {
        required: true
      },
      txtRiskCategory: {
        required: true
      }

    },
    FieldType: {
      datePickerAuditDate: {
        type: "datePicker"
      },
      peoplePickerDiv: {
        type: "peoplePicker"
      },
      peoplePickerDiv4: {
        type: "peoplePicker"
      },
      txtSource: {
        type: "input"
      },
      txtSubject: {
        type: "input"
      },
      txtDescription: {
        type: "textarea"
      },
      txtNonconformance: {
        type: "input"
      },
      txtAreaAudited: {
        type: "input"
      },
      txtCategory: {
        type: "input"
      },
      txtAuditor: {
        type: "input"
      },
      txtRiskCategory: {
        type: "input"
      }
    },
    message: {
      datePickerAuditDate: {
        required: "This field is required",
      },
      peoplePickerDiv: {
        required: "This field is required"
      },
      peoplePickerDiv4: {
        required: "This field is required"
      },
      txtSource: {
        required: "This field is required"
      },
      txtSubject: {
        required: "This field is required"
      },
      txtDescription: {
        required: "This field is required"
      },
      txtNonconformance: {
        required: "This field is required"
      },
      txtAreaAudited: {
        required: "This field is required"
      },
      txtCategory: {
        required: "This field is required"
      },
      txtAuditor: {
        required: "This field is required"
      },
      txtRiskCategory: {
        required: "This field is required"
      }
    }

  }


  if (Utility.Validate(validation)) {

    dialog = SP.SOD.execute('sp.ui.dialog.js', 'SP.UI.ModalDialog.showWaitScreenWithNoClose', "Saving Report", "Please Wait");
    var ReportList = [];
    var comments
    var Id = $('input[id^="txtCounterId"]').val();
    var ncassignee = getUserInfo("peoplePickerDiv4");
    var NCReportCount = $('input[id^="txtNCNumber"').length;
    var NCReport = $('input[id^="txtReportId"]').val();
    var ReportList = [];
    var AreaAudited = "";
    var agingdate = new Date();
    AreaAudited = $('#txtAreaAudited').val();
    for (var i = 0; i < NCReportCount; i++) {

      var dateCreated = new Date($('input[id^="dtCreatedDate"').val());
      var AuditDate = new Date($('input[id^="dtAuditDate"]').val());
      var endorser = getUserInfo("peoplePickerDiv2");
      var creator = getUserInfo("peoplePickerDiv");
      var recipient = getUserInfo("peoplePickerDiv3");
      var source = $('input[id^="txtSource"').val();
      var ncnumber = $($('input[id^="txtNCNumber"]')[i]).val();
      var status = ReportStatus;
      var subj = $($('input[id^="txtSubject"]')[i]).val();
      var description = $($('textarea[id^="txtDescription"]')[i]).val();
      var nonconf = $($('input[id^="txtNonconformance"]')[i]).val();
      var attachments = "";
      var reporttype = $('input[id^="txtReportType"]').val();
      var ncassignee = getUserInfo("peoplePickerDiv4");
      var ReferenceRecord = $('input[id^="txtRefRecord"]').val();
      var ExecutingPersonnel = $('input[id^="txtExecutingPersonnel"]').val();
      var auditor = $('input[id^="txtAuditor"]').val();
      var riskcategory = $('input[id^="txtRiskCategory"]').val();
      $('#txtAttachments').next('div').find('a[id^="deleteLink"]').remove();
      $('#txtAttachments').next('div').find('span[class^="ms-newdocument-iconouter"]').remove();
      $('#txtAttachments').next('div').find('div[id^="NewdummyLink"]').attr("id", "dummyLink");

      var attachments = $($($('input[id^="txtAttachments"]')[i]).siblings('div')[0]).html().trim();
      var actor = null;
      if ($('input[id^="txtReportType"]').val().toUpperCase() != "BRU") {
        switch (ReportStatus.toUpperCase()) {
          case "NC-DRAFT":
          case "NC-FOR REVISION":
          case "CAP-FOR AUDITORS ASSESSMENT":
          case "CAP-FOR FINAL EVALUATION":
            actor = creator;
            break;
          case "NC-FOR REVIEW":
          case "NC-CHALLENGED":
          case "CAP-FOR APPROVAL":
          case "CAP-RESOLVED":
            actor = endorser;
            break;
          case "NC-ISSUED":
          case "CAP-FOR REVIEW":
            actor = recipient;
            break;
          case "NC-ASSIGNED":
          case "CAP-FOR REVISION":
          case "CAP-FOR IMPLEMENTATION":
            actor = ncassignee;
            break;
          default:
            actor = null;
        }
      } else {
        switch (ReportStatus.toUpperCase()) {
          case "NC-DRAFT":
          case "CAP-FOR APPROVAL":
          case "CAP-FOR FINAL EVALUATION":
            actor = creator;
            break;
          case "NC-ASSIGNED":
          case "CAP-FOR REVISION":
          case "CAP-FOR IMPLEMENTATION":
          case "CAP-RESOLVED":
            actor = ncassignee;
            break;
          default:
            actor = null;
        }
      }

      ReportList.push({
        "ID": Id,
        "Title": NCReport,
        "DateCreated": dateCreated,
        "AuditDate": AuditDate,
        "AreaAudited": AreaAudited,
        "NCEndorser": SP.FieldUserValue.fromUser(endorser),
        "NCCreator": SP.FieldUserValue.fromUser(creator),
        "Source": source,
        "NCRecipient": SP.FieldUserValue.fromUser(recipient),
        "NCNumber": ncnumber,
        "Status": status,
        "Subject": subj,
        "Description": description,
        "NonConformance": nonconf,
        "FileAttachments": attachments,
        "ReportType": reporttype,
        "NCAssignee": SP.FieldUserValue.fromUser(ncassignee),
        "Flag": "No",
        "ReminderFlag": "No",
        "AgingDate": agingdate,
        "CommentFlag": CommentFlagValue,
        "ReferenceRecord": ReferenceRecord,
        "ExecutingPersonnel": ExecutingPersonnel,
        "Auditor": auditor,
        "RiskCategory": riskcategory,
        "Actor": actor != null && actor != "" ? SP.FieldUserValue.fromUser(actor) : null
      });
    }


    if (itemsToDelete.length > 0) {
      $.when(deleteSelectedFiles(itemsToDelete))
        .done(function (data) {

          if (ReportStatus.toUpperCase() == "NC-CHALLENGED") {

            $.when(stopWorkflow(Application["List"]["NCReport"], $('input[id^="txtCounterId"]').val()))
              .done(function (data) {
                CreateAssigneeList(ReportList);
              })
              .fail(function (sender, args) {
                console.log("Error: " + args.get_message() + "\n" + args.get_stackTrace());
              });
          } else if (ReportStatus.toUpperCase() == "NC-ASSIGNED") {
            $.when(stopWorkflow(Application["List"]["NCReport"], $('input[id^="txtCounterId"]').val()))
              .done(function (data) {
                CreateAssigneeList(ReportList);
              })
              .fail(function (sender, args) {
                console.log("Error: " + args.get_message() + "\n" + args.get_stackTrace());
              });
          } else if (ReportStatus.toUpperCase() == "CAP-FOR REVIEW") {
            $.when(stopWorkflow(Application["List"]["NCReport"], $('input[id^="txtCounterId"]').val()))
              .done(function (data) {
                CreateCorrectiveActionPlan(ReportList);
              })
              .fail(function (sender, args) {
                console.log("Error: " + args.get_message() + "\n" + args.get_stackTrace());
              });
          } else if (ReportStatus.toUpperCase() == "CAP-FOR REVISION") {
            $.when(stopWorkflow(Application["List"]["NCReport"], $('input[id^="txtCounterId"]').val()))
              .done(function (data) {
                CreateAssigneeList(ReportList);
              })
              .fail(function (sender, args) {
                console.log("Error: " + args.get_message() + "\n" + args.get_stackTrace());
              });
          } else {
            CreateAssigneeList(ReportList);
          }

        })
        .fail(function (sender, args) {
          console.log("Error: " + args.get_message() + "\n" + args.get_stackTrace());
        });
    } else {
      if (ReportStatus.toUpperCase() == "NC-CHALLENGED") {

        $.when(stopWorkflow(Application["List"]["NCReport"], $('input[id^="txtCounterId"]').val()))
          .done(function (data) {
            CreateAssigneeList(ReportList);
          })
          .fail(function (sender, args) {
            console.log("Error: " + args.get_message() + "\n" + args.get_stackTrace());
          });
      } else if (ReportStatus.toUpperCase() == "NC-ASSIGNED") {
        $.when(stopWorkflow(Application["List"]["NCReport"], $('input[id^="txtCounterId"]').val()))
          .done(function (data) {
            CreateAssigneeList(ReportList);
          })
          .fail(function (sender, args) {
            console.log("Error: " + args.get_message() + "\n" + args.get_stackTrace());
          });
      } else if (ReportStatus.toUpperCase() == "CAP-FOR REVIEW") {
        $.when(stopWorkflow(Application["List"]["NCReport"], $('input[id^="txtCounterId"]').val()))
          .done(function (data) {
            CreateCorrectiveActionPlan(ReportList);
          })
          .fail(function (sender, args) {
            console.log("Error: " + args.get_message() + "\n" + args.get_stackTrace());
          });
      } else if (ReportStatus.toUpperCase() == "CAP-FOR REVISION") {
        $.when(stopWorkflow(Application["List"]["NCReport"], $('input[id^="txtCounterId"]').val()))
          .done(function (data) {
            CreateAssigneeList(ReportList);
          })
          .fail(function (sender, args) {
            console.log("Error: " + args.get_message() + "\n" + args.get_stackTrace());
          });
      } else {
        CreateAssigneeList(ReportList);
      }
    }


  } else {
    return false;
  }
}

function SavingItemByStatus(ReportStatus) {

  var Utility = SharePoint.Utility;
  var validation;
  // var radioClick = $($('#choicefieldgroup').find('ul .ms-RadioButton').find('label')[0]).hasClass("is-checked")
  validation = {
    Fields: {
      datePickerAuditDate: {
        required: true
      },
      peoplePickerDiv: {
        required: true
      },
      peoplePickerDiv2: {
        required: true
      },
      peoplePickerDiv3: {
        required: true
      },
      txtSource: {
        required: true
      },
      txtCategory: {
        required: true
      },
      txtAreaAudited: {
        required: true
      },
      txtRiskCategory: {
        required: true
      }
    },
    FieldType: {
      datePickerAuditDate: {
        type: "datePicker"
      },
      peoplePickerDiv: {
        type: "peoplePicker"
      },
      peoplePickerDiv2: {
        type: "peoplePicker"
      },
      peoplePickerDiv3: {
        type: "peoplePicker"
      },
      txtSource: {
        type: "input"
      },
      txtCategory: {
        type: "input"
      },
      txtAreaAudited: {
        type: "input"
      },
      txtRiskCategory: {
        type: "input"
      }
    },
    message: {
      datePickerAuditDate: {
        required: "This field is required",
      },
      peoplePickerDiv: {
        required: "This field is required"
      },
      peoplePickerDiv2: {
        required: "This field is required"
      },
      peoplePickerDiv3: {
        required: "This field is required"
      },
      txtSource: {
        required: "This field is required"
      },
      txtCategory: {
        required: "This field is required"
      },
      txtAreaAudited: {
        required: "This field is required"
      },
      txtRiskCategory: {
        required: "This field is required"
      }
    }
  }

  if (Utility.Validate(validation)) {
    dialog = SP.SOD.execute('sp.ui.dialog.js', 'SP.UI.ModalDialog.showWaitScreenWithNoClose', "Saving Report", "Please Wait");
    var NCReportCount = $('input[id^="txtNCNumber"').length;
    var NCReport = $('input[id^="txtReportId"]').val();
    var ReportList = [];
    var AreaAudited = "";
    var agingdate = new Date();
    var resolvedate = new Date();
    var comments = "";
    var implementationDays = "10";

    AreaAudited = $('#txtAreaAudited').val();

    for (var i = 0; i < NCReportCount; i++) {
      var Id = $('input[id^="txtCounterId"]').val();
      var dateCreated = new Date($('input[id^="dtCreatedDate"').val());
      var AuditDate = new Date($('input[id^="dtAuditDate"]').val());
      var endorser = getUserInfo("peoplePickerDiv2");
      var creator = getUserInfo("peoplePickerDiv");
      var recipient = getUserInfo("peoplePickerDiv3");
      var ncassignee = getUserInfo("peoplePickerDiv4");
      var actor = null;
      if ($('input[id^="txtReportType"]').val().toUpperCase() != "BRU") {
        switch (ReportStatus.toUpperCase()) {
          case "NC-DRAFT":
          case "NC-FOR REVISION":
          case "CAP-FOR AUDITORS ASSESSMENT":
          case "CAP-FOR FINAL EVALUATION":
            actor = creator;
            break;
          case "NC-FOR REVIEW":
          case "NC-CHALLENGED":
          case "CAP-FOR APPROVAL":
          case "CAP-RESOLVED":
            actor = endorser;
            break;
          case "NC-ISSUED":
          case "CAP-FOR REVIEW":
            actor = recipient;
            break;
          case "NC-ASSIGNED":
          case "CAP-FOR REVISION":
          case "CAP-FOR IMPLEMENTATION":
            actor = ncassignee;
            break;
          default:
            actor = null;
        }
      } else {
        switch (ReportStatus.toUpperCase()) {
          case "NC-DRAFT":
          case "CAP-FOR APPROVAL":
          case "CAP-FOR FINAL EVALUATION":
            actor = creator;
            break;
          case "NC-ASSIGNED":
          case "CAP-FOR REVISION":
          case "CAP-FOR IMPLEMENTATION":
          case "CAP-RESOLVED":
            actor = ncassignee;
            break;
          default:
            actor = null;
        }
      }
      var source = $('input[id^="txtSource"').val();
      var ncnumber = $($('input[id^="txtNCNumber"]')[i]).val();

      var subj = $($('input[id^="txtSubject"]')[i]).val();
      var description = $($('textarea[id^="txtDescription"]')[i]).val();
      var nonconf = $($('input[id^="txtNonconformance"]')[i]).val();

      var category = $($('input[id^="txtCategory"]')[i]).val();

      var ReferenceRecord = $('input[id^="txtRefRecord"]').val();
      var ExecutingPersonnel = $('input[id^="txtExecutingPersonnel"]').val();

      var auditor = $('input[id^="txtAuditor"]').val().trim();
      var riskcategory = $('input[id^="txtRiskCategory"]').val().trim();


      var attachments = "";
      var oldattachments = "";
      var auditfile = "";
      $('#txtAttachments').next('div').find('a[id^="deleteLink"]').remove();
      $('#txtAttachments').next('div').find('span[class^="ms-newdocument-iconouter"]').remove();
      $('#txtAttachments').next('div').find('div[id^="NewdummyLink"]').attr("id", "dummyLink");

      attachments = $($($('input[id^="txtAttachments"]')[0]).siblings('div')[0]).html().trim();

      $('#txtAuditFile').next('div').find('a[id^="deleteLink"]').remove();
      $('#txtAuditFile').next('div').find('span[class^="ms-newdocument-iconouter"]').remove();
      $('#txtAuditFile').next('div').find('div[id^="NewdummyLink"]').attr("id", "dummyLink");
      auditfile = $($($('input[id^="txtAuditFile"]')[0]).siblings('div')[0]).html().trim();

      if ($('input[id^="txtImplementationDays"]').val().trim() != "") {
        implementationDays = $('input[id^="txtImplementationDays"]').val().trim();
      }

      agingdate.setDate(agingdate.getDate());


      if (ReportStatus.toUpperCase() == "NC-ISSUED") {
        if ($('input[id^="dtIssuedDate"]').val().trim().length <= 0) {
          comments = "NC Report Issued";
        }
      } else if (ReportStatus.toUpperCase() == "CAP-FOR AUDITORS ASSESSMENT") {
        if ($('input[id^="dtCapReleasedDate"]').val().trim().length <= 0) {
          comments = "CAP with RCA released to Auditors"
        }
      } else if (ReportStatus.toUpperCase() == "CAP-FOR IMPLEMENTATION") {
        if ($('input[id^="dtCapApprovedDate"]').val().trim().length <= 0) {
          comments = "CAP Approved";
        }
      } else if (ReportStatus.toUpperCase() == "CAP-FOR FINAL EVALUATION") {
        if ($('input[id^="dtCapAuditedDate"]').val().trim().length <= 0) {
          comments = "CAP Tagged For Audit";
        }
      } else if (ReportStatus.toUpperCase() == "CAP-FOR RESOLVED") {
        comments = "CAP For Closing";
      } else if (ReportStatus.toUpperCase() == "CLOSED") {
        comments = "NC Closed";
        if ($($('textarea[id^="txtComments"]')[i]).val().trim().length > 0) {
          comments += " - " + $($('textarea[id^="txtComments"]')[i]).val().trim();
        }
      }

      var cc = getUserInfoMulti("peopleCC");
      agingdate.setDate(agingdate.getDate());

      if (ReportStatus.toUpperCase() == "CAP-RESOLVED") {
        if (cc.length > 0) {
          var emailCC = [];
          for (var ii in cc) {
            var lookupValue = SP.FieldUserValue.fromUser(cc[ii]);
            emailCC.push(lookupValue);
          }

          ReportList.push({
            "ID": Id,
            "Title": NCReport,
            "DateCreated": dateCreated,
            "AuditDate": AuditDate,
            "AreaAudited": AreaAudited,
            "NCEndorser": SP.FieldUserValue.fromUser(endorser),
            "NCCreator": SP.FieldUserValue.fromUser(creator),
            "Source": source,
            "NCRecipient": SP.FieldUserValue.fromUser(recipient),
            "NCNumber": ncnumber,
            "Status": ReportStatus,
            "Subject": subj,
            "Description": description,
            "NonConformance": nonconf,
            "FileAttachments": attachments,
            "Comments": comments,
            "AgingDate": agingdate,
            // "LatestComment": comments,
            "Flag": "No",
            "ReminderFlag": "No",
            "NoOfDaysImplement": implementationDays,
            "CommentFlag": CommentFlagValue,
            "EmailCC": emailCC,
            "Category": category,
            "ReferenceRecord": ReferenceRecord,
            "ExecutingPersonnel": ExecutingPersonnel,
            "Auditor": auditor,
            "RiskCategory": riskcategory,
            "Actor": actor != null && actor != "" ? SP.FieldUserValue.fromUser(actor) : null
          });
        } else {
          ReportList.push({
            "ID": Id,
            "Title": NCReport,
            "DateCreated": dateCreated,
            "AuditDate": AuditDate,
            "AreaAudited": AreaAudited,
            "NCEndorser": SP.FieldUserValue.fromUser(endorser),
            "NCCreator": SP.FieldUserValue.fromUser(creator),
            "Source": source,
            "NCRecipient": SP.FieldUserValue.fromUser(recipient),
            "NCNumber": ncnumber,
            "Status": ReportStatus,
            "Subject": subj,
            "Description": description,
            "NonConformance": nonconf,
            "FileAttachments": attachments,
            "Comments": comments,
            "AgingDate": agingdate,
            // "LatestComment": comments,
            "Flag": "No",
            "ReminderFlag": "No",
            "NoOfDaysImplement": implementationDays,
            "CommentFlag": CommentFlagValue,
            "Category": category,
            "ReferenceRecord": ReferenceRecord,
            "ExecutingPersonnel": ExecutingPersonnel,
            "Auditor": auditor,
            "RiskCategory": riskcategory,
            "Actor": actor != null && actor != "" ? SP.FieldUserValue.fromUser(actor) : null
          });
        }

      } else if (ReportStatus.toUpperCase() == "CAP-FOR FINAL EVALUATION") {
        if (cc.length > 0) {
          var emailCC = [];
          for (var ii in cc) {
            var lookupValue = SP.FieldUserValue.fromUser(cc[ii]);
            emailCC.push(lookupValue);
          }

          ReportList.push({
            "ID": Id,
            "Title": NCReport,
            "DateCreated": dateCreated,
            "AuditDate": AuditDate,
            "AreaAudited": AreaAudited,
            "NCEndorser": SP.FieldUserValue.fromUser(endorser),
            "NCCreator": SP.FieldUserValue.fromUser(creator),
            "Source": source,
            "NCRecipient": SP.FieldUserValue.fromUser(recipient),
            "NCNumber": ncnumber,
            "Status": ReportStatus,
            "Subject": subj,
            "Description": description,
            "NonConformance": nonconf,
            "FileAttachments": attachments,
            "Comments": comments,
            "AgingDate": agingdate,
            // "LatestComment": comments,
            "Flag": "No",
            "ReminderFlag": "No",
            "NoOfDaysImplement": implementationDays,
            "CommentFlag": CommentFlagValue,
            "EmailCC": emailCC,
            "Category": category,
            "AuditFile": auditfile,
            "ReferenceRecord": ReferenceRecord,
            "ExecutingPersonnel": ExecutingPersonnel,
            "Auditor": auditor,
            "RiskCategory": riskcategory,
            "Actor": actor != null && actor != "" ? SP.FieldUserValue.fromUser(actor) : null
          });
        } else {
          ReportList.push({
            "ID": Id,
            "Title": NCReport,
            "DateCreated": dateCreated,
            "AuditDate": AuditDate,
            "AreaAudited": AreaAudited,
            "NCEndorser": SP.FieldUserValue.fromUser(endorser),
            "NCCreator": SP.FieldUserValue.fromUser(creator),
            "Source": source,
            "NCRecipient": SP.FieldUserValue.fromUser(recipient),
            "NCNumber": ncnumber,
            "Status": ReportStatus,
            "Subject": subj,
            "Description": description,
            "NonConformance": nonconf,
            "FileAttachments": attachments,
            "Comments": comments,
            "AgingDate": agingdate,
            // "LatestComment": comments,
            "Flag": "No",
            "ReminderFlag": "No",
            "NoOfDaysImplement": implementationDays,
            "CommentFlag": CommentFlagValue,
            "Category": category,
            "AuditFile": auditfile,
            "ReferenceRecord": ReferenceRecord,
            "ExecutingPersonnel": ExecutingPersonnel,
            "Auditor": auditor,
            "RiskCategory": riskcategory,
            "Actor": actor != null && actor != "" ? SP.FieldUserValue.fromUser(actor) : null
          });
        }
      } else {
        if (cc.length > 0) {
          var emailCC = [];
          for (var ii in cc) {
            var lookupValue = SP.FieldUserValue.fromUser(cc[ii]);
            emailCC.push(lookupValue);
          }

          ReportList.push({
            "ID": Id,
            "Title": NCReport,
            "DateCreated": dateCreated,
            "AuditDate": AuditDate,
            "AreaAudited": AreaAudited,
            "NCEndorser": SP.FieldUserValue.fromUser(endorser),
            "NCCreator": SP.FieldUserValue.fromUser(creator),
            "Source": source,
            "NCRecipient": SP.FieldUserValue.fromUser(recipient),
            "NCNumber": ncnumber,
            "Status": ReportStatus,
            "Subject": subj,
            "Description": description,
            "NonConformance": nonconf,
            "FileAttachments": attachments,
            "Comments": comments,
            "AgingDate": agingdate,
            // "LatestComment": comments,
            "Flag": "No",
            "ReminderFlag": "No",
            "NoOfDaysImplement": implementationDays,
            "CommentFlag": CommentFlagValue,
            "EmailCC": emailCC,
            "Category": category,
            "ReferenceRecord": ReferenceRecord,
            "ExecutingPersonnel": ExecutingPersonnel,
            "Auditor": auditor,
            "RiskCategory": riskcategory,
            "Actor": actor != null && actor != "" ? SP.FieldUserValue.fromUser(actor) : null
          });
        } else {
          ReportList.push({
            "ID": Id,
            "Title": NCReport,
            "DateCreated": dateCreated,
            "AuditDate": AuditDate,
            "AreaAudited": AreaAudited,
            "NCEndorser": SP.FieldUserValue.fromUser(endorser),
            "NCCreator": SP.FieldUserValue.fromUser(creator),
            "Source": source,
            "NCRecipient": SP.FieldUserValue.fromUser(recipient),
            "NCNumber": ncnumber,
            "Status": ReportStatus,
            "Subject": subj,
            "Description": description,
            "NonConformance": nonconf,
            "FileAttachments": attachments,
            "Comments": comments,
            "AgingDate": agingdate,
            // "LatestComment": comments,
            "Flag": "No",
            "ReminderFlag": "No",
            "NoOfDaysImplement": implementationDays,
            "CommentFlag": CommentFlagValue,
            "Category": category,
            "ReferenceRecord": ReferenceRecord,
            "ExecutingPersonnel": ExecutingPersonnel,
            "Auditor": auditor,
            "RiskCategory": riskcategory,
            "Actor": actor != null && actor != "" ? SP.FieldUserValue.fromUser(actor) : null
          });
        }
      }


    }


    if (itemsToDelete.length > 0) {
      $.when(deleteSelectedFiles(itemsToDelete))
        .done(function (data) {
          if (ReportStatus.toUpperCase() == "NC-CHALLENGED") {
            $.when(stopWorkflow(Application["List"]["NCReport"], $('input[id^="txtCounterId"]').val()))
              .done(function (data) {
                createListItem(ReportList);
              })
              .fail(function (sender, args) {
                console.log("Error: " + args.get_message() + "\n" + args.get_stackTrace());
              });
          } else if (ReportStatus.toUpperCase() == "CANCELLED") {
            $.when(stopWorkflow(Application["List"]["NCReport"], $('input[id^="txtCounterId"]').val()))
              .done(function (data) {
                createListItem(ReportList);
              })
              .fail(function (sender, args) {
                console.log("Error: " + args.get_message() + "\n" + args.get_stackTrace());
              });
          } else if (ReportStatus.toUpperCase() == "NC-FOR REVIEW") {
            $.when(stopWorkflow(Application["List"]["NCReport"], $('input[id^="txtCounterId"]').val()))
              .done(function (data) {
                createListItem(ReportList);
              })
              .fail(function (sender, args) {
                console.log("Error: " + args.get_message() + "\n" + args.get_stackTrace());
              });
          } else if (ReportStatus.toUpperCase() == "NC-FOR REVISION") {
            $.when(stopWorkflow(Application["List"]["NCReport"], $('input[id^="txtCounterId"]').val()))
              .done(function (data) {
                createListItem(ReportList);
              })
              .fail(function (sender, args) {
                console.log("Error: " + args.get_message() + "\n" + args.get_stackTrace());
              });
          } else if (ReportStatus.toUpperCase() == "NC-ISSUED") {
            $.when(stopWorkflow(Application["List"]["NCReport"], $('input[id^="txtCounterId"]').val()))
              .done(function (data) {
                createListItem(ReportList);
              })
              .fail(function (sender, args) {
                console.log("Error: " + args.get_message() + "\n" + args.get_stackTrace());
              });
          } else if (ReportStatus.toUpperCase() == "NC-ASSIGNED") {
            $.when(stopWorkflow(Application["List"]["NCReport"], $('input[id^="txtCounterId"]').val()))
              .done(function (data) {
                createListItem(ReportList);
              })
              .fail(function (sender, args) {
                console.log("Error: " + args.get_message() + "\n" + args.get_stackTrace());
              });
          } else if (ReportStatus.toUpperCase() == "CAP-FOR REVIEW") {
            $.when(stopWorkflow(Application["List"]["NCReport"], $('input[id^="txtCounterId"]').val()))
              .done(function (data) {
                createListItem(ReportList);
              })
              .fail(function (sender, args) {
                console.log("Error: " + args.get_message() + "\n" + args.get_stackTrace());
              });
          } else if (ReportStatus.toUpperCase() == "CAP-FOR REVISION") {
            $.when(stopWorkflow(Application["List"]["NCReport"], $('input[id^="txtCounterId"]').val()))
              .done(function (data) {
                createListItem(ReportList);
              })
              .fail(function (sender, args) {
                console.log("Error: " + args.get_message() + "\n" + args.get_stackTrace());
              });
          } else if (ReportStatus.toUpperCase() == "CAP-FOR AUDITORS ASSESSMENT") {
            $.when(stopWorkflow(Application["List"]["NCReport"], $('input[id^="txtCounterId"]').val()))
              .done(function (data) {
                createListItem(ReportList);
              })
              .fail(function (sender, args) {
                console.log("Error: " + args.get_message() + "\n" + args.get_stackTrace());
              });
          } else if (ReportStatus.toUpperCase() == "CAP-FOR APPROVAL") {
            $.when(stopWorkflow(Application["List"]["NCReport"], $('input[id^="txtCounterId"]').val()))
              .done(function (data) {
                createListItem(ReportList);
              })
              .fail(function (sender, args) {
                console.log("Error: " + args.get_message() + "\n" + args.get_stackTrace());
              });
          } else if (ReportStatus.toUpperCase() == "CAP-FOR IMPLEMENTATION") {
            $.when(stopWorkflow(Application["List"]["NCReport"], $('input[id^="txtCounterId"]').val()))
              .done(function (data) {
                createListItem(ReportList);
              })
              .fail(function (sender, args) {
                console.log("Error: " + args.get_message() + "\n" + args.get_stackTrace());
              });
          } else if (ReportStatus.toUpperCase() == "CAP-FOR FINAL EVALUATION") {
            $.when(stopWorkflow(Application["List"]["NCReport"], $('input[id^="txtCounterId"]').val()))
              .done(function (data) {
                TagForAuditCreate(ReportList);
                //createListItem(ReportList);
              })
              .fail(function (sender, args) {
                console.log("Error: " + args.get_message() + "\n" + args.get_stackTrace());
              });
          } else if (ReportStatus.toUpperCase() == "CAP-RESOLVED") {
            $.when(stopWorkflow(Application["List"]["NCReport"], $('input[id^="txtCounterId"]').val()))
              .done(function (data) {
                createListItem(ReportList);
              })
              .fail(function (sender, args) {
                console.log("Error: " + args.get_message() + "\n" + args.get_stackTrace());
              });
          } else if (ReportStatus.toUpperCase() == "CLOSED") {
            $.when(stopWorkflow(Application["List"]["NCReport"], $('input[id^="txtCounterId"]').val()))
              .done(function (data) {
                createListItem(ReportList);
              })
              .fail(function (sender, args) {
                console.log("Error: " + args.get_message() + "\n" + args.get_stackTrace());
              });
          } else {
            createListItem(ReportList);
          }
        })
        .fail(function (sender, args) {
          console.log("Error: " + args.get_message() + "\n" + args.get_stackTrace());
        });
    } else {
      if (ReportStatus.toUpperCase() == "NC-CHALLENGED") {
        $.when(stopWorkflow(Application["List"]["NCReport"], $('input[id^="txtCounterId"]').val()))
          .done(function (data) {
            createListItem(ReportList);
          })
          .fail(function (sender, args) {
            console.log("Error: " + args.get_message() + "\n" + args.get_stackTrace());
          });
      } else if (ReportStatus.toUpperCase() == "CANCELLED") {
        $.when(stopWorkflow(Application["List"]["NCReport"], $('input[id^="txtCounterId"]').val()))
          .done(function (data) {
            createListItem(ReportList);
          })
          .fail(function (sender, args) {
            console.log("Error: " + args.get_message() + "\n" + args.get_stackTrace());
          });
      } else if (ReportStatus.toUpperCase() == "NC-FOR REVIEW") {
        $.when(stopWorkflow(Application["List"]["NCReport"], $('input[id^="txtCounterId"]').val()))
          .done(function (data) {
            createListItem(ReportList);
          })
          .fail(function (sender, args) {
            console.log("Error: " + args.get_message() + "\n" + args.get_stackTrace());
          });
      } else if (ReportStatus.toUpperCase() == "NC-FOR REVISION") {
        $.when(stopWorkflow(Application["List"]["NCReport"], $('input[id^="txtCounterId"]').val()))
          .done(function (data) {
            createListItem(ReportList);
          })
          .fail(function (sender, args) {
            console.log("Error: " + args.get_message() + "\n" + args.get_stackTrace());
          });
      } else if (ReportStatus.toUpperCase() == "NC-ISSUED") {
        $.when(stopWorkflow(Application["List"]["NCReport"], $('input[id^="txtCounterId"]').val()))
          .done(function (data) {
            createListItem(ReportList);
          })
          .fail(function (sender, args) {
            console.log("Error: " + args.get_message() + "\n" + args.get_stackTrace());
          });
      } else if (ReportStatus.toUpperCase() == "NC-ASSIGNED") {
        $.when(stopWorkflow(Application["List"]["NCReport"], $('input[id^="txtCounterId"]').val()))
          .done(function (data) {
            createListItem(ReportList);
          })
          .fail(function (sender, args) {
            console.log("Error: " + args.get_message() + "\n" + args.get_stackTrace());
          });
      } else if (ReportStatus.toUpperCase() == "CAP-FOR REVIEW") {
        $.when(stopWorkflow(Application["List"]["NCReport"], $('input[id^="txtCounterId"]').val()))
          .done(function (data) {
            createListItem(ReportList);
          })
          .fail(function (sender, args) {
            console.log("Error: " + args.get_message() + "\n" + args.get_stackTrace());
          });
      } else if (ReportStatus.toUpperCase() == "CAP-FOR REVISION") {
        $.when(stopWorkflow(Application["List"]["NCReport"], $('input[id^="txtCounterId"]').val()))
          .done(function (data) {
            createListItem(ReportList);
          })
          .fail(function (sender, args) {
            console.log("Error: " + args.get_message() + "\n" + args.get_stackTrace());
          });
      } else if (ReportStatus.toUpperCase() == "CAP-FOR AUDITORS ASSESSMENT") {
        $.when(stopWorkflow(Application["List"]["NCReport"], $('input[id^="txtCounterId"]').val()))
          .done(function (data) {
            createListItem(ReportList);
          })
          .fail(function (sender, args) {
            console.log("Error: " + args.get_message() + "\n" + args.get_stackTrace());
          });
      } else if (ReportStatus.toUpperCase() == "CAP-FOR APPROVAL") {
        $.when(stopWorkflow(Application["List"]["NCReport"], $('input[id^="txtCounterId"]').val()))
          .done(function (data) {
            createListItem(ReportList);
          })
          .fail(function (sender, args) {
            console.log("Error: " + args.get_message() + "\n" + args.get_stackTrace());
          });
      } else if (ReportStatus.toUpperCase() == "CAP-FOR IMPLEMENTATION") {
        $.when(stopWorkflow(Application["List"]["NCReport"], $('input[id^="txtCounterId"]').val()))
          .done(function (data) {
            createListItem(ReportList);
          })
          .fail(function (sender, args) {
            console.log("Error: " + args.get_message() + "\n" + args.get_stackTrace());
          });
      } else if (ReportStatus.toUpperCase() == "CAP-FOR FINAL EVALUATION") {
        $.when(stopWorkflow(Application["List"]["NCReport"], $('input[id^="txtCounterId"]').val()))
          .done(function (data) {
            TagForAuditCreate(ReportList);
            //createListItem(ReportList);
          })
          .fail(function (sender, args) {
            console.log("Error: " + args.get_message() + "\n" + args.get_stackTrace());
          });
      } else if (ReportStatus.toUpperCase() == "CLOSED") {
        $.when(stopWorkflow(Application["List"]["NCReport"], $('input[id^="txtCounterId"]').val()))
          .done(function (data) {
            createListItem(ReportList);
          })
          .fail(function (sender, args) {
            console.log("Error: " + args.get_message() + "\n" + args.get_stackTrace());
          });
      } else if (ReportStatus.toUpperCase() == "CAP-RESOLVED") {
        $.when(stopWorkflow(Application["List"]["NCReport"], $('input[id^="txtCounterId"]').val()))
          .done(function (data) {
            createListItem(ReportList);
          })
          .fail(function (sender, args) {
            console.log("Error: " + args.get_message() + "\n" + args.get_stackTrace());
          });
      } else {
        createListItem(ReportList);
      }
    }



  } else {
    return false;
  }
}

function IsCurrentUserMemberOfGroup(groupName, OnComplete) {

  var currentContext = new SP.ClientContext.get_current();
  var currentWeb = currentContext.get_web();

  var currentUser = currentContext.get_web().get_currentUser();
  currentContext.load(currentUser);

  var allGroups = currentWeb.get_siteGroups();
  currentContext.load(allGroups);

  var group = allGroups.getByName(groupName);
  currentContext.load(group);

  var groupUsers = group.get_users();
  currentContext.load(groupUsers);

  currentContext.executeQueryAsync(OnSuccess, OnFailure);

  function OnSuccess(sender, args) {
    var userInGroup = false;
    var groupUserEnumerator = groupUsers.getEnumerator();

    while (groupUserEnumerator.moveNext()) {
      var groupUser = groupUserEnumerator.get_current();
      if (groupUser.get_id() == currentUser.get_id()) {
        userInGroup = true;
        break;
      }
    }

    OnComplete(userInGroup);
  }

  function OnFailure(sender, args) {
    OnComplete(false);
  }
}


function retrieveComment(ListName, ItemId, FieldName, getAllComments) {
  var items = [];
  $().SPServices({
    operation: "GetVersionCollection",
    async: false,
    strlistID: ListName,
    strlistItemID: ItemId,
    strFieldName: FieldName,
    completefunc: function (xData, Status) {
      $(xData.responseText).find("Version").each(function (i) {

        var editor = $(this).attr("Editor");
        var edit2 = editor.substring(0, editor.indexOf(',#'));
        var getEmail = editor.split(',')[3];
        var email = getEmail.substr(1, getEmail.length);
        var name = edit2.split('#')[1];
        if (getAllComments) {

          items.push({
            "Name": name,
            FieldName: $(this).attr(FieldName),
            "Modified": formatDate($(this).attr("Modified")),
            "Email": email,
            "Time": formatAMPM($(this).attr("Modified")),
            "CustomDate": timeSince(new Date($(this).attr("Modified")))
          })

        } else { //get specific
          if (name == _spPageContextInfo.userDisplayName) {
            items.push({
              "Name": name,
              FieldName: $(this).attr(FieldName),
              "Modified": formatDate($(this).attr("Modified")),
              "Email": email,
              "Time": formatAMPM($(this).attr("Modified")),
              "CustomDate": timeSince(formatDate($(this).attr("Modified")) + " " + formatAMPM($(this).attr("Modified")))
            })
          }
        }

      });
    }

  });

  function parseDate(input) {
    var parts = input.match(/(\d+)/g);
    // new Date(year, month [, date [, hours[, minutes[, seconds[, ms]]]]])
    return new Date(parts[0], parts[1] - 1, parts[2]); // months are 0-based
  }

  function formatDate(date) {
    var date = new Date(date);
    var mm = (date.getMonth() + 1);
    mm = (mm < 10) ? '0' + mm : mm;
    var dd = date.getDate();
    dd = (dd < 10) ? '0' + dd : dd;
    return mm + '/' + dd + '/' + date.getFullYear();
  }

  function formatAMPM(getDate) {
    var date = new Date(getDate);
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
  }

  function timeSince(timeStamp) {
    var now = new Date(),
      secondsPast = (now.getTime() - timeStamp.getTime()) / 1000;

    var formatDate = function (date, format, utc) {
      var MMMM = ["\x00", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
      var MMM = ["\x01", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
      var dddd = ["\x02", "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
      var ddd = ["\x03", "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];

      function ii(i, len) {
        var s = i + "";
        len = len || 2;
        while (s.length < len) s = "0" + s;
        return s;
      }

      var y = utc ? date.getUTCFullYear() : date.getFullYear();
      format = format.replace(/(^|[^\\])yyyy+/g, "$1" + y);
      format = format.replace(/(^|[^\\])yy/g, "$1" + y.toString().substr(2, 2));
      format = format.replace(/(^|[^\\])y/g, "$1" + y);

      var M = (utc ? date.getUTCMonth() : date.getMonth()) + 1;
      format = format.replace(/(^|[^\\])MMMM+/g, "$1" + MMMM[0]);
      format = format.replace(/(^|[^\\])MMM/g, "$1" + MMM[0]);
      format = format.replace(/(^|[^\\])MM/g, "$1" + ii(M));
      format = format.replace(/(^|[^\\])M/g, "$1" + M);

      var d = utc ? date.getUTCDate() : date.getDate();
      format = format.replace(/(^|[^\\])dddd+/g, "$1" + dddd[0]);
      format = format.replace(/(^|[^\\])ddd/g, "$1" + ddd[0]);
      format = format.replace(/(^|[^\\])dd/g, "$1" + ii(d));
      format = format.replace(/(^|[^\\])d/g, "$1" + d);

      var H = utc ? date.getUTCHours() : date.getHours();
      format = format.replace(/(^|[^\\])HH+/g, "$1" + ii(H));
      format = format.replace(/(^|[^\\])H/g, "$1" + H);

      var h = H > 12 ? H - 12 : H == 0 ? 12 : H;
      format = format.replace(/(^|[^\\])hh+/g, "$1" + ii(h));
      format = format.replace(/(^|[^\\])h/g, "$1" + h);

      var m = utc ? date.getUTCMinutes() : date.getMinutes();
      format = format.replace(/(^|[^\\])mm+/g, "$1" + ii(m));
      format = format.replace(/(^|[^\\])m/g, "$1" + m);

      var s = utc ? date.getUTCSeconds() : date.getSeconds();
      format = format.replace(/(^|[^\\])ss+/g, "$1" + ii(s));
      format = format.replace(/(^|[^\\])s/g, "$1" + s);

      var f = utc ? date.getUTCMilliseconds() : date.getMilliseconds();
      format = format.replace(/(^|[^\\])fff+/g, "$1" + ii(f, 3));
      f = Math.round(f / 10);
      format = format.replace(/(^|[^\\])ff/g, "$1" + ii(f));
      f = Math.round(f / 10);
      format = format.replace(/(^|[^\\])f/g, "$1" + f);

      var T = H < 12 ? "AM" : "PM";
      format = format.replace(/(^|[^\\])TT+/g, "$1" + T);
      format = format.replace(/(^|[^\\])T/g, "$1" + T.charAt(0));

      var t = T.toLowerCase();
      format = format.replace(/(^|[^\\])tt+/g, "$1" + t);
      format = format.replace(/(^|[^\\])t/g, "$1" + t.charAt(0));

      var tz = -date.getTimezoneOffset();
      var K = utc || !tz ? "Z" : tz > 0 ? "+" : "-";
      if (!utc) {
        tz = Math.abs(tz);
        var tzHrs = Math.floor(tz / 60);
        var tzMin = tz % 60;
        K += ii(tzHrs) + ":" + ii(tzMin);
      }
      format = format.replace(/(^|[^\\])K/g, "$1" + K);

      var day = (utc ? date.getUTCDay() : date.getDay()) + 1;
      format = format.replace(new RegExp(dddd[0], "g"), dddd[day]);
      format = format.replace(new RegExp(ddd[0], "g"), ddd[day]);

      format = format.replace(new RegExp(MMMM[0], "g"), MMMM[M]);
      format = format.replace(new RegExp(MMM[0], "g"), MMM[M]);

      format = format.replace(/\\(.)/g, "$1");

      return format;
    };

    if (secondsPast < 60) { // Less than a minute
      return parseInt(secondsPast) + 'secs ago';
    }
    if (secondsPast < 3600) { // Less than an hour
      return parseInt(secondsPast / 60) + 'mins ago';
    }
    if (secondsPast <= 86400) { // Less than a day
      return parseInt(secondsPast / 3600) + 'hrs ago';
    }
    if (secondsPast <= 172800) { // Less than 2 days
      return 'Yesderday at ' + formatDate(timeStamp, "h:mmtt");
    }
    if (secondsPast > 172800) { // After two days
      var timeString;

      if (secondsPast <= 604800)
        timeString = formatDate(timeStamp, "dddd") + " at " + formatDate(timeStamp, "h:mmtt") // with in a week
      else if (now.getFullYear() > timeStamp.getFullYear())
        timeString = formatDate(timeStamp, "MMMM d, yyyy") // a year ago
      else if (now.getMonth() > timeStamp.getMonth())
        timeString = formatDate(timeStamp, "MMMM d") // months ago
      else
        timeString = formatDate(timeStamp, "MMMM d") + " at " + formatDate(timeStamp, "h:mmtt") // with in a month

      return timeString;
    }
  }


  return items;
}


function DeleteCurrentItem(ItemId) {

  var clientContext = new SP.ClientContext.get_current();
  var oList = clientContext.get_web().get_lists().getById(Application["List"]["NCReport"]);
  this.oListItem = oList.getItemById(ItemId);
  oListItem.deleteObject();
  clientContext.executeQueryAsync(Success, Failed);

  function Success() {
    //window.location = "/sites/nccardev/Lists/NCReport";

    if ($('#txtReportType').val().toUpperCase() == "BRU") {
      window.location = BRUAllItemLink;
    } else {
      window.location = IQAAllItemLink;
    }
  }

  function Failed(sender, args) {
    alert('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
  }

}

function createTreeView(input, treeID) {
  var tree = new treefilter($(input).next("#tree"), {
    searcher: input,
    expanded: true,
    multiselect: false
  });

  $(input).focusin(function () {
    $(this).next('ul[id^="tree"]').css("display", "block");
  })


  $(input).next('ul[id="tree"]').find('li').click(function (e) {


    if ($(this).children("ul").length > 0) {
      // e.stopPropagation();
      //$(this).children('ul').toggle();
    } else {
      var sample = $(this).children().clone() //clone the element
        .children() //select all the children
        .remove() //remove all the children
        .end() //again go back to selected element
        .text();
      var index = $(this).parent('ul[id^="tree"]')[0];
      $(input).val(sample);
      $(input).next('ul[id="tree"]').css("display", "none");
    }
  })

  $(input).next('ul[id="tree"]').hover(function () {
    $(this).css("display", "block");
  }, function () {
    $(this).css("display", "none");
  })
}

function retrieveItem(callback) {

  var ID = getQueryString("ID");
  var comments = retrieveComment('NCReport', ID, 'Comments', true);
  var layout = createCommentLayout(comments);
  if (layout == "") {
    $('#CommentDiv').parent('div').css('display', 'none');
  } else {
    $('#CommentDiv').parent('div').css('display', 'block');
  }
  $('#CommentDiv').append(layout);


  function createCommentLayout(data) {
    var layout = "";
    var groupedData = _.groupBy(data, function (d) {
      return d.Modified
    });

    function custom_sort(a, b) {
      return new Date(a.Modified).getTime() - new Date(b.Modified).getTime();
    }

    var newData = data //data.sort(custom_sort);

    var groupedData = _.groupBy(newData, function (d) {
      return d.Modified
    });
    var header = [];
    for (var key in groupedData) {
      if (groupedData.hasOwnProperty(key)) {
        console.log(key);
        header.push(key);
      }
    }

    for (var i = 0; i < header.length; i++) {
      var dat = groupedData[header[i]];
      layout += '<div id="content"><h2 class="Intro-title ms-fontSize-ml">' + header[i] + '</h2>';
      layout += '<br/>';
      for (var j = 0; j < dat.length; j++) {
        console.log("Date: " + dat[j]["Modified"] + " Name: " + dat[j]["Name"]);
        layout += '<div class="ms-Persona ms-Persona--sm">'
        layout += '<div class="ms-Persona-imageArea">'
        layout += '<img class="ms-Persona-image" src="/_layouts/15/userphoto.aspx?size=L&username=' + dat[j]["Email"] + '">'
        layout += '</div>';
        layout += '<div class="ms-Persona-details">';
        layout += '<div class="ms-Persona-primaryText">' + dat[j]["FieldName"] + '</div>';
        layout += '<div class="ms-Persona-secondaryText">' + dat[j]["Name"] + '</div>';
        layout += '<div class="ms-Persona-optionalText">' + dat[j]["CustomDate"] + '</div>';
        layout += '</div>';
        layout += '</div>';
        layout += '<br/>';
      }
    }

    return layout;
  }

  var caml = "<View><Query> <Where> <Eq> <FieldRef Name='ID' /> <Value Type='Counter'>" + ID + "</Value> </Eq> </Where> </Query></View>"
  Utility.RetrieveListItemsById(Application["List"]["NCReport"], caml,
    function (listItemEnumerator) {
      var status = "";
      var actor;
      var reportType = "";
      var numberOfDaysImplemented
      while (listItemEnumerator.moveNext()) {
        var listItem = listItemEnumerator.get_current();
        $('input[id^="txtCounterId"]').val(listItem.get_item('ID'));
        $('input[id^="txtReportId"]').val(listItem.get_item('Title'));
        $('input[id^="dtCreatedDate"]').val(formatDate(listItem.get_item('DateCreated')));
        $('input[id^="dtAuditDate"]').val(formatDate(listItem.get_item('AuditDate')));
        $('input[id^="txtAreaAudited"]').val(listItem.get_item('AreaAudited'));
        $('input[id^="txtSource"]').val(listItem.get_item('Source'));
        $('input[id^="txtNCNumber"]').val(listItem.get_item('NCNumber'));
        $('input[id^="txtStatus"]').val(listItem.get_item('Status'));
        $('input[id^="txtSubject"]').val(listItem.get_item('Subject'));
        $('input[id^="txtDescription"]').val(listItem.get_item('Description'));
        $('input[id^="txtNonconformance"]').val(listItem.get_item('NonConformance'));
        $('textarea[id^="txtDescription"]').val(listItem.get_item('Description'));

        retrieveToPeoplePicker("peoplePickerDiv", listItem.get_item('NCCreator').get_lookupValue());
        retrieveToPeoplePicker("peoplePickerDiv3", listItem.get_item('NCRecipient').get_lookupValue());
        $('input[id^="txtReportType"]').val(listItem.get_item('ReportType'));
        reportType = listItem.get_item('ReportType');

        if (listItem.get_item('Penalties') != null) {
          $('input[id^="txtPenalties"]').val(listItem.get_item('Penalties'));
        }
        if (listItem.get_item('ImplementationDate') != null) {
          $('input[id^="dtImplementationDate"]').val(formatDate(listItem.get_item('ImplementationDate')));
        }
        if (listItem.get_item('ExecutingPersonnel') != null) {
          $('input[id^="txtExecutingPersonnel"]').val(listItem.get_item('ExecutingPersonnel'));
        }
        if (listItem.get_item('ReferenceRecord') != null) {
          $('input[id^="txtRefRecord"]').val(listItem.get_item('ReferenceRecord'));
        }
        if (listItem.get_item('Auditor') != null) {
          $('input[id^="txtAuditor"]').val(listItem.get_item('Auditor'));
        }
        if (listItem.get_item('RiskCategory') != null) {
          $('input[id^="txtRiskCategory"]').val(listItem.get_item('RiskCategory'));
        }
        if (listItem.get_item('ReportIssueDate') != null) {
          $('input[id^="dtIssuedDate"]').val(listItem.get_item('ReportIssueDate'));
        }
        if (listItem.get_item('CapReleasedDate') != null) {
          $('input[id^="dtCapReleasedDate"]').val(listItem.get_item('CapReleasedDate'));
        }
        if (listItem.get_item('CapApprovedDate') != null) {
          $('input[id^="dtCapApprovedDate"]').val(listItem.get_item('CapApprovedDate'));
        }
        if (listItem.get_item('CapAuditedDate') != null) {
          $('input[id^="dtCapAuditedDate"]').val(listItem.get_item('CapAuditedDate'));
        }
        if (listItem.get_item('Category') != null) {
          $('input[id^="txtCategory"]').val(listItem.get_item('Category'));
        }
        if (listItem.get_item('AuditFile') != null) {
          var auditfile = listItem.get_item("AuditFile")
          $('#txtAuditFile').after(rca);
        }
        if (listItem.get_item('EmailCC') != null) {
          var members = listItem.get_item('EmailCC');
          for (i = 0; i < members.length; i++) {
            var member = members[i];
            retrieveToPeoplePicker("peopleCC", member.get_lookupValue());
          }
        }
        if (listItem.get_item('AuditFile') != null) {
          var auditfile = listItem.get_item("AuditFile");
          $('#txtAuditFile').after(auditfile);

        }
        if (listItem.get_item('CAPComment') != null) {
          $('#txtCAPComments').val(listItem.get_item('CAPComment'));

        }

        if (listItem.get_item('NCEndorser') != null) {
          retrieveToPeoplePicker("peoplePickerDiv2", listItem.get_item('NCEndorser').get_lookupValue());
        }

        if (listItem.get_item('NCAssignee') != null) {
          retrieveToPeoplePicker("peoplePickerDiv4", listItem.get_item('NCAssignee').get_lookupValue());
          actor = listItem.get_item('NCAssignee').get_lookupValue();
        }

        if (listItem.get_item('NoOfDaysImplement') != null) {
          numberOfDaysImplemented = listItem.get_item('NoOfDaysImplement');
          $('#txtImplementationDays').val(listItem.get_item('NoOfDaysImplement'));
        }
        if (listItem.get_item("CAPAttachment") != null) {
          var cap = listItem.get_item("CAPAttachment");
          $('#txtCAP').after(cap);
        }
        if (listItem.get_item("RCAAttachment") != null) {
          var rca = listItem.get_item("RCAAttachment")
          $('#txtRAC').after(rca);
        }
        if (listItem.get_item("AgingDate") != null) {
          $('input[id^="dtAgingDate"]').val(formatDate(listItem.get_item('AgingDate')));
        }
        $('input[id^="txtReminder"]').val(listItem.get_item('ReminderFlag'));
        var attachments = listItem.get_item('FileAttachments');
        status = listItem.get_item('Status');
        $('input[id^="txtReportType"]').val(listItem.get_item('ReportType'));
        $('#txtAttachments').after(attachments);

      };

      CustomFormByStatus(actor, status, reportType);
      InitiateProgressMeter($('input[id^="txtStatus"]').val());
      getTerms();
      getSourceTerms();
      getSourceTerms2($('input[id^="txtReportType"]').val().toUpperCase());
      getCategoryTerms("txtCategory");
      getSingleTerms("4bb3693f-e34d-45ec-a034-414f80d1aeeb", "txtRiskCategory");
      getSingleTerms("2b3036e3-f084-431d-b435-b292485bfdf3", "txtAuditor");
      $('#RelatedFindingsDiv a').each(function(){ 
        var redirectURL = Application["SiteName"] + "/SitePages/" + "RelatedFindingsPage.aspx" + "/" + listItem.get_item('NCNumber')
            $(this).attr("href", redirectURL); 
      });


    });

  function CustomFormByStatus(Actor, ReportStatus, ReportType) {
    Utility.GetCurrentUserGroups(function (groups) {
      console.log(groups)
      var reportStaus = $('input[id^="txtStatus"]').val().toUpperCase();
      var ReportType = $('input[id^="txtReportType"]').val().toUpperCase();
      switch (reportStaus) {
        case "NC-DRAFT":
          fnNCDraft(groups, ReportType);
          break;
        case "NC-FOR REVIEW":
          fnNCForReview(groups);
          break;
        case "NC-FOR REVISION":
          fnNCForRevision(groups);
          break;
        case "NC-ISSUED":
          fnNCIssued(groups);
          break;
        case "NC-CHALLENGED":
          fnNCChallenged(groups);
          break;
        case "NC-ASSIGNED":
          fnNCAssigned(Actor, groups, ReportType);
          break;
        case "CAP-FOR REVIEW":
          fnCAPReview(Actor, groups, ReportType);
          break;
        case "CAP-FOR REVISION":
          fnCAPRevision(Actor, groups, ReportType);
          break;
        case "CAP-FOR AUDITORS ASSESSMENT":
          fnCAPAuditorAssessment(Actor, groups);
          break;
        case "CAP-FOR APPROVAL":
          fnCAPApproval(Actor, groups, ReportType);
          break;
        case "CAP-FOR IMPLEMENTATION":
          fnCAPImplementation(Actor, groups, ReportType);
          break;
        case "CAP-FOR FINAL EVALUATION":
          fnForFinalImplementation(Actor, groups, ReportType);
          break;
        case "CAP-RESOLVED":
          fnCAPResolve(Actor, groups, ReportType);
          break;
        case "CLOSED":
          fnClosed(Actor, groups, ReportType);
          break;
        case "CANCELLED":
          fnClosed(Actor, groups, ReportType);
          break;
      }

    });
  }

  function fnClosed(Actor, groups, ReportType) {
    if (ReportType.toUpperCase() != "BRU") {
      $('#NCAssigneeDiv').css('display', 'block');
      $('#peoplePickerDiv3').css('display', 'block');
      $('#CommentInput').css('display', 'none');
      $('#CommentPart').css('display', 'block');
      $('#btnAddReport').css('display', 'none');
      $('#btnDeleteReport').css('display', 'none');
      $('#btnSaveDraft').css('display', 'none');
      $('#btnSaveReport').css('display', 'none');
      $('#RecepientDiv').css('display', 'block');
      $('#CAPDiv').css("display", 'block');
      $('#AuditDiv').css("display", 'block');

      $(".sp-peoplepicker-delImage").hide();
      DisableForms("NCInformationDivMain", true);
    } else {
      swapDivs('peoplePickerDiv2', 'peoplePickerDiv4');
      $('#peoplePickerDiv2').parent('div').css('display', 'none');
      $('#CAPDiv').css("display", 'block');
      $('#AuditDiv').css("display", 'block');
      $('#NCAssigneeDiv').css('display', 'block');
      $('#CommentInput').css('display', 'none');
      $('#CommentPart').css('display', 'block');
      $('#btnAddReport').css('display', 'none');
      $('#btnDeleteReport').css('display', 'none');
      $('#btnSaveDraft').css('display', 'none');
      $('#btnSaveReport').css('display', 'none');
      $('#RecepientDiv').css('display', 'none');
      $(".sp-peoplepicker-delImage").hide();
      DisableForms("NCInformationDivMain", true);
    }

  }

  function fnCAPResolve(Assignee, groups, ReportType) {
    if (ReportType.toUpperCase() != "BRU") {
      var displayname = _spPageContextInfo.userDisplayName;
      var actor = getUserInfoMulti("peoplePickerDiv2");
      if (actor.includes(displayname) || groups.includes("Networks Administrator")) {
        $(".sp-peoplepicker-delImage").hide();
        $('#CAPDiv').css("display", 'block');
        DisableForms("NCInformationDiv", true);
        DisableForms("NCReport", true);
        DisableForms("CAPDiv", true);
        DisableForms("peoplePickerDiv4", false);
        $('#NCAssigneeDiv').css('display', 'block');
        $('#RecepientDiv').css('display', 'block');
        $('#CommentInput').css('display', 'block');
        $('#CommentPart').css('display', 'block');
        $('#btnAddReport').css('display', 'none');
        $('#btnDeleteReport').css('display', 'none');
        $('#btnSaveDraft').css('display', 'none');
        $('#btnSaveReport').css('display', 'none');

        $('#btnSaveReport').after('<a class="tooltipbutton expand" data-title="Returns report to Auditor for revision"><input class="ms-Button ms-Button--primary" id="btnReturn" type="button" value="Return NC Report" style="font-size: small; font-family: inherit; color: white; font-weight: 600; background-color: #4b3832; border-color: #4b3832; border-radius: 5px;width:99px;font-size:9px;float: right;"></a>');
        $('#btnSaveReport').after('<a class="tooltipbutton expand" data-title="Marks the report as closed or resolved"><input class="ms-Button ms-Button--primary" id="btnCloseReport" type="button" value="Close Report" style="font-size: small; font-family: inherit; color: white; font-weight: 600; background-color: #4b3832; border-color: #4b3832; border-radius: 5px;width:99px;font-size:11px;float: right;"></a>');
        $('input#btnAddComment').click(function () { //button Save Report click event
          var validation = {
            Fields: {
              txtComments: {
                required: true
              }
            },
            FieldType: {
              txtComments: {
                type: "textarea"
              }
            },
            message: {
              txtComments: {
                required: "This field is required",
              }
            }

          }
          if (Utility.Validate(validation)) {
            $('#s4-bodyContainer').addClass('blur-filter');
            $('#s4-workspace').after('<div id="loading-bar-spinner" class="spinner"><div class="spinner-icon"></div></div>');
            setTimeout(function () {
              var ajax = updateComment();
              ajax.done(function (data, xhr) {
                retrieveCommentSPA();
                $($('textarea[id^="txtComments"]')[0]).val('');
                $(".blur-filter").removeClass("blur-filter");
                $("#loading-bar-spinner").remove();
              }).fail(function (sender, args) {
                console.log(args);
              });
            }, 1000);
          } else {
            return false;
          }

        });

        $('input#btnReturn').click(function () { //button Save Report click event
          var comments = $('#txtComments').val().trim();
          if (comments.length > 0) { //Add Confirmation Message
            var html = document.createElement('div');
            html.innerHTML = '<h2>Comments below will not be saved. If you wish to save the comment click Add Comment first otherwise continue. </h2>\
            <br>\
            <div class="ms-core-form-section"><div class="ms-core-form-bottomButtonBox"><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.OK); return false;">Continue</button><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.cancel); return false;">Cancel</button>\
            </div></div>'
            OpenPopUpPageWithDialogOptions({
              title: "Confirmation",
              html: html,
              dialogReturnValueCallback: function (dialogResult) {
                if (dialogResult == 1) {
                  SavingItemByStatus("CAP-For Final Evaluation");
                }
              }
            });
          } else {
            SavingItemByStatus("CAP-For Final Evaluation");
          }
        });

        $('input#btnCloseReport').click(function () { //button Save Report click event
          var comments = $('#txtComments').val().trim();
          if (comments.length > 0) { //Add Confirmation Message
            var html = document.createElement('div');
            html.innerHTML = '<h2>Comments below will not be saved. If you wish to save the comment click Add Comment first otherwise continue. </h2>\
            <br>\
            <div class="ms-core-form-section"><div class="ms-core-form-bottomButtonBox"><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.OK); return false;">Continue</button><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.cancel); return false;">Cancel</button>\
            </div></div>'
            OpenPopUpPageWithDialogOptions({
              title: "Confirmation",
              html: html,
              dialogReturnValueCallback: function (dialogResult) {
                if (dialogResult == 1) {
                  SavingItemByStatus("Closed");
                }
              }
            });
          } else {
            SavingItemByStatus("Closed");
          }
        });
      } else {
        $('#CAPDiv').css("display", 'block');
        $(".sp-peoplepicker-delImage").hide();
        $('#NCAssigneeDiv').css('display', 'block');
        $('#RecepientDiv').css('display', 'block');
        $('input[type^="button"]').css("display", 'none');
        DisableForms("NCInformationDivMain", true);
        // UnableToView("You don't have Permission to View the Item", link);
      }
    } else {
      if (Assignee == _spPageContextInfo.userDisplayName || groups.includes("Networks Administrator")) {
        swapDivs('peoplePickerDiv2', 'peoplePickerDiv4');
        $('#peoplePickerDiv2').parent('div').css('display', 'none');
        $(".sp-peoplepicker-delImage").hide();
        $('#RecepientDiv').css('display', 'none');
        $('#CAPDiv').css("display", 'block');
        DisableForms("NCInformationDiv", true);
        DisableForms("NCReport", true);
        DisableForms("CAPDiv", true);
        DisableForms("peoplePickerDiv4", false);
        $('#NCAssigneeDiv').css('display', 'block');
        $('#RecepientDiv').css('display', 'none');
        $('#CommentInput').css('display', 'block');
        $('#CommentPart').css('display', 'block');
        $('#btnAddReport').css('display', 'none');
        $('#btnDeleteReport').css('display', 'none');
        $('#btnSaveDraft').css('display', 'none');
        $('#btnSaveReport').css('display', 'none');

        $('#btnSaveReport').after('<input class="ms-Button ms-Button--primary" id="btnCloseReport" type="button" value="Close Report" style="font-size: small; font-family: inherit; color: white; font-weight: 600; background-color: #4b3832; border-color: #4b3832; border-radius: 5px;width:99px;font-size:11px;float: right;">');
        $('input#btnAddComment').click(function () { //button Save Report click event
          var validation = {
            Fields: {
              txtComments: {
                required: true
              }
            },
            FieldType: {
              txtComments: {
                type: "textarea"
              }
            },
            message: {
              txtComments: {
                required: "This field is required",
              }
            }

          }
          if (Utility.Validate(validation)) {
            $('#s4-bodyContainer').addClass('blur-filter');
            $('#s4-workspace').after('<div id="loading-bar-spinner" class="spinner"><div class="spinner-icon"></div></div>');
            setTimeout(function () {
              var ajax = updateComment();
              ajax.done(function (data, xhr) {
                retrieveCommentSPA();
                $($('textarea[id^="txtComments"]')[0]).val('');
                $(".blur-filter").removeClass("blur-filter");
                $("#loading-bar-spinner").remove();

              }).fail(function (sender, args) {
                console.log(args);
              });
            }, 1000);
          } else {
            return false;
          }

        });
        $('input#btnCloseReport').click(function () { //button Save Report click event
          var comments = $('#txtComments').val().trim();
          if (comments.length > 0) { //Add Confirmation Message
            var html = document.createElement('div');
            html.innerHTML = '<h2>Comments below will not be saved. If you wish to save the comment click Add Comment first otherwise continue. </h2>\
            <br>\
            <div class="ms-core-form-section"><div class="ms-core-form-bottomButtonBox"><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.OK); return false;">Continue</button><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.cancel); return false;">Cancel</button>\
            </div></div>'
            OpenPopUpPageWithDialogOptions({
              title: "Confirmation",
              html: html,
              dialogReturnValueCallback: function (dialogResult) {
                if (dialogResult == 1) {
                  SavingItemByStatus("Closed");
                }
              }
            });
          } else {
            SavingItemByStatus("Closed");
          }
        });
      } else {
        swapDivs('peoplePickerDiv2', 'peoplePickerDiv4');
        $('#peoplePickerDiv2').parent('div').css('display', 'none');
        $('#CAPDiv').css("display", 'block');
        $(".sp-peoplepicker-delImage").hide();
        $('#NCAssigneeDiv').css('display', 'block');
        $('#RecepientDiv').css('display', 'none');
        $('input[type^="button"]').css("display", 'none');
        DisableForms("NCInformationDivMain", true);
        // UnableToView("You don't have Permission to View the Item", link);
      }
    }
    showCancelButton(displayname);
  }

  function fnForFinalImplementation(Assignee, groups, ReportType) {
    if (ReportType.toUpperCase() != "BRU") {
      var displayname = _spPageContextInfo.userDisplayName;
      var actor = getUserInfoMulti("peoplePickerDiv");
      if (actor.includes(displayname) || groups.includes("Networks Administrator")) {
        $(".sp-peoplepicker-delImage").hide();
        $('#CAPDiv').css("display", 'block');
        DisableForms("NCInformationDiv", true);
        DisableForms("NCReport", true);
        DisableForms("CAPDiv", true);
        DisableForms("peoplePickerDiv4", false);
        DisableForms("AuditDiv", true);
        $('#AuditDiv').css('display', 'block');
        $('#NCAssigneeDiv').css('display', 'block');
        $('#RecepientDiv').css('display', 'block');
        $('#CommentInput').css('display', 'block');
        $('#CommentPart').css('display', 'block');
        $('#btnAddReport').css('display', 'none');
        $('#btnDeleteReport').css('display', 'none');
        $('#btnSaveDraft').css('display', 'none');
        $('#btnSaveReport').css('display', 'none');
        $('#AuditDiv').css('display', 'block');

        $('#btnSaveReport').after('<div class="tooltiparea expand" data-title="Returns report to concerned/audited office for proper implementation Note: Ensure to add comment for reason of non-acceptance of provided solution"><input class="ms-Button ms-Button--primary" id="btnDeny" type="button" value="Deny Report" style="font-size: small; font-family: inherit; color: white; font-weight: 600; background-color: #4b3832; border-color: #4b3832; border-radius: 5px;width:99px;font-size:11px;float: right;"></a>');
        $('#btnSaveReport').after('<div class="tooltiparealeft expand" data-title="Notifies Lead Auditor that report is ready for closing"><input class="ms-Button ms-Button--primary" id="btnClose" type="button" value="Accept Report" style="font-size: small; font-family: inherit; color: white; font-weight: 600; background-color: #4b3832; border-color: #4b3832; border-radius: 5px;width:99px;font-size:11px;float: right;"></a>');
        $('#txtAuditFile').change(function () {
          var layout = "";
          var renderItems = true;
          var inputs = $(this);
          var sFileName;
          var _validFileExtensions = ValidFiles;

          FileLoop:
          for (var i = 0; i < $(this).get(0).files.length; i++) {
            var blnValid = false;
            sFileName = $(this).get(0).files[i].name;
            Extension:
            for (var j = 0; j < _validFileExtensions.length; j++) {
              var sCurExtension = _validFileExtensions[j];
              var sFileExtension = getFileExtension3(sFileName)
              if (sFileExtension.toLowerCase() === sCurExtension.toLowerCase()) {
                blnValid = true;
              }
            }

            if (!blnValid) {
              alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
              inputs.replaceWith(inputs.val('').clone(true));
              renderItems = false;
              break FileLoop;
            }

          }

          if (renderItems) {
            var layout = "";
            $(this).next('div').find('div').find('div[id^="NewdummyLink"]').remove();
            for (var i = 0; i < $(this).get(0).files.length; i++) {
              var fileName = $(this).get(0).files[i].name;
              var link = Application["SiteName"] + "/AuditFile/" + $('input[id^="txtNCNumber"]').val() + "/" + fileName;
              layout += '<div id="NewdummyLink">';
              layout += '<span class="ms-newdocument-iconouter"><img class="ms-newdocument-icon" src="/_layouts/15/images/spcommon.png?rev=44#ThemeKey=" alt="new" title="new"></span><span><a target="_blank" a href="' + link + '"><img class="ms-asset-icon ms-rtePosition-4" src="' + getIcon(fileName) + '">' + fileName + '</a></span>';
              layout += '</div>';
            }
            $(this).next('div').find('div[id="Attachments"]').prepend(layout);
          }

          function getFileExtension3(filename) {
            return filename.slice((filename.lastIndexOf(".") - 1 >>> 0) + 1);
          }

        });

        $('input#btnAddComment').click(function () {
          var validation = {
            Fields: {
              txtComments: {
                required: true
              }
            },
            FieldType: {
              txtComments: {
                type: "textarea"
              }
            },
            message: {
              txtComments: {
                required: "This field is required",
              }
            }

          }
          if (Utility.Validate(validation)) {
            $('#s4-bodyContainer').addClass('blur-filter');
            $('#s4-workspace').after('<div id="loading-bar-spinner" class="spinner"><div class="spinner-icon"></div></div>');
            setTimeout(function () {
              var ajax = updateComment();
              ajax.done(function (data, xhr) {
                retrieveCommentSPA();
                $($('textarea[id^="txtComments"]')[0]).val('');
                $(".blur-filter").removeClass("blur-filter");
                $("#loading-bar-spinner").remove();

              }).fail(function (sender, args) {
                console.log(args);
              });
            }, 1000);
          } else {
            return false;
          }

        });

        $('input#btnDeny').click(function () { //button Save Report click event
          var comments = $('#txtComments').val().trim();
          if (comments.length > 0) { //Add Confirmation Message
            var html = document.createElement('div');
            html.innerHTML = '<h2>Comments below will not be saved. If you wish to save the comment click Add Comment first otherwise continue. </h2>\
            <br>\
            <div class="ms-core-form-section"><div class="ms-core-form-bottomButtonBox"><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.OK); return false;">Continue</button><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.cancel); return false;">Cancel</button>\
            </div></div>'
            OpenPopUpPageWithDialogOptions({
              title: "Confirmation",
              html: html,
              dialogReturnValueCallback: function (dialogResult) {
                if (dialogResult == 1) {
                  SavingItemByStatus("CAP-For Implementation");
                }
              }
            });
          } else {
            SavingItemByStatus("CAP-For Implementation");
          }

        });

        $('input#btnClose').click(function () { //button Save Report click event
          var comments = $('#txtComments').val().trim();
          if (comments.length > 0) { //Add Confirmation Message
            var html = document.createElement('div');
            html.innerHTML = '<h2>Comments below will not be saved. If you wish to save the comment click Add Comment first otherwise continue. </h2>\
            <br>\
            <div class="ms-core-form-section"><div class="ms-core-form-bottomButtonBox"><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.OK); return false;">Continue</button><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.cancel); return false;">Cancel</button>\
            </div></div>'
            OpenPopUpPageWithDialogOptions({
              title: "Confirmation",
              html: html,
              dialogReturnValueCallback: function (dialogResult) {
                if (dialogResult == 1) {
                  SavingItemByStatus("CAP-Resolved");
                }
              }
            });
          } else {
            SavingItemByStatus("CAP-Resolved");
          }
        });

      } else {
        DisableForms("AuditDiv", true);
        $('#AuditDiv').css('display', 'block');
        $(".sp-peoplepicker-delImage").hide();
        $('#CAPDiv').css("display", 'block');
        $('#CommentPart').css('display', 'block');
        $('#NCAssigneeDiv').css('display', 'block');
        $('#RecepientDiv').css('display', 'block');
        $('input[type^="button"]').css("display", 'none');
        DisableForms("NCInformationDivMain", true);
        //UnableToView("You don't have Permission to View the Item", link);
      }
    } else {
      var displayname = _spPageContextInfo.userDisplayName;
      var actor = getUserInfoMulti("peoplePickerDiv");
      if (actor.includes(displayname) || groups.includes("Networks Administrator")) {
        swapDivs('peoplePickerDiv2', 'peoplePickerDiv4');
        $('#peoplePickerDiv2').parent('div').css('display', 'none');
        $(".sp-peoplepicker-delImage").hide();
        $('#RecepientDiv').css('display', 'none');
        $('#CAPDiv').css("display", 'block');
        DisableForms("NCInformationDiv", true);
        DisableForms("NCReport", true);
        DisableForms("CAPDiv", true);
        DisableForms("peoplePickerDiv4", false);
        DisableForms("AuditDiv", true);
        $('#AuditDiv').css('display', 'block');
        $('#NCAssigneeDiv').css('display', 'block');
        $('#RecepientDiv').css('display', 'none');
        $('#NCAssigneeDiv').css('display', 'block');
        $('#CommentInput').css('display', 'block');
        $('#CommentPart').css('display', 'block');
        $('#btnAddReport').css('display', 'none');
        $('#btnDeleteReport').css('display', 'none');
        $('#btnSaveDraft').css('display', 'none');
        $('#btnSaveReport').css('display', 'none');

        $('#btnSaveReport').after('<div class="tooltiparea expand" data-title="Returns report to concerned/audited office for proper implementation Note: Ensure to add comment for reason of non-acceptance of provided solution"><input class="ms-Button ms-Button--primary" id="btnDeny" type="button" value="Deny Report" style="font-size: small; font-family: inherit; color: white; font-weight: 600; background-color: #4b3832; border-color: #4b3832; border-radius: 5px;width:99px;font-size:11px;float: right;"></a>');
        $('#btnSaveReport').after('<div class="tooltiparealeft expand" data-title="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Marks the report as closed or resolved&nbsp;&nbsp;&nbsp;&nbsp;"><input class="ms-Button ms-Button--primary" id="btnClose" type="button" value="Accept Report" style="font-size: small; font-family: inherit; color: white; font-weight: 600; background-color: #4b3832; border-color: #4b3832; border-radius: 5px;width:99px;font-size:11px;float: right;"></a>');

        $('input#btnAddComment').click(function () { //button Save Report click event
          var validation = {
            Fields: {
              txtComments: {
                required: true
              }
            },
            FieldType: {
              txtComments: {
                type: "textarea"
              }
            },
            message: {
              txtComments: {
                required: "This field is required",
              }
            }

          }
          if (Utility.Validate(validation)) {
            $('#s4-bodyContainer').addClass('blur-filter');
            $('#s4-workspace').after('<div id="loading-bar-spinner" class="spinner"><div class="spinner-icon"></div></div>');
            setTimeout(function () {
              var ajax = updateComment();
              ajax.done(function (data, xhr) {
                retrieveCommentSPA();
                $($('textarea[id^="txtComments"]')[0]).val('');
                $(".blur-filter").removeClass("blur-filter");
                $("#loading-bar-spinner").remove();

              }).fail(function (sender, args) {
                console.log(args);
              });
            }, 1000);
          } else {
            return false;
          }

        });

        $('input#btnDeny').click(function () { //button Save Report click event

          var comments = $('#txtComments').val().trim();
          if (comments.length > 0) { //Add Confirmation Message
            var html = document.createElement('div');
            html.innerHTML = '<h2>Comments below will not be saved. If you wish to save the comment click Add Comment first otherwise continue. </h2>\
            <br>\
            <div class="ms-core-form-section"><div class="ms-core-form-bottomButtonBox"><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.OK); return false;">Continue</button><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.cancel); return false;">Cancel</button>\
            </div></div>'
            OpenPopUpPageWithDialogOptions({
              title: "Confirmation",
              html: html,
              dialogReturnValueCallback: function (dialogResult) {
                if (dialogResult == 1) {
                  SavingItemByStatus("CAP-For Implementation");
                }
              }
            });
          } else {
            SavingItemByStatus("CAP-For Implementation");
          }
        });

        $('input#btnClose').click(function () { //button Save Report click event
          var comments = $('#txtComments').val().trim();
          if (comments.length > 0) { //Add Confirmation Message
            var html = document.createElement('div');
            html.innerHTML = '<h2>Comments below will not be saved. If you wish to save the comment click Add Comment first otherwise continue. </h2>\
              <br>\
              <div class="ms-core-form-section"><div class="ms-core-form-bottomButtonBox"><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.OK); return false;">Continue</button><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.cancel); return false;">Cancel</button>\
              </div></div>'
            OpenPopUpPageWithDialogOptions({
              title: "Confirmation",
              html: html,
              dialogReturnValueCallback: function (dialogResult) {
                if (dialogResult == 1) {
                  SavingItemByStatus("Closed");
                }
              }
            });
          } else {
            SavingItemByStatus("Closed");
          }

        });

      } else {
        DisableForms("AuditDiv", true);
        $('#AuditDiv').css('display', 'block');
        swapDivs('peoplePickerDiv2', 'peoplePickerDiv4');
        $('#peoplePickerDiv2').parent('div').css('display', 'none');
        $(".sp-peoplepicker-delImage").hide();
        $('#CAPDiv').css("display", 'block');
        $('#CommentPart').css('display', 'block');
        $('#NCAssigneeDiv').css('display', 'block');
        $('#RecepientDiv').css('display', 'none');
        $('input[type^="button"]').css("display", 'none');
        DisableForms("NCInformationDivMain", true);
        // UnableToView("You don't have Permission to View the Item", link);
      }

    }
    showCancelButton(displayname);
  }

  function fnCAPImplementation(Assignee, groups, ReportType) {
    if (ReportType.toUpperCase() != "BRU") {
      if (Assignee == _spPageContextInfo.userDisplayName || groups.includes("Networks Administrator")) {
        $(".sp-peoplepicker-delImage").hide();
        $('#CAPDiv').css("display", 'block');
        DisableForms("NCInformationDiv", true);
        DisableForms("NCReport", true);
        DisableForms("CAPDiv", true);
        DisableForms("peoplePickerDiv4", true);
        addRemoveAttachmentsButton($('#txtAuditFile'));
        $('#NCAssigneeDiv').css('display', 'block');
        $('#RecepientDiv').css('display', 'block');
        $('#CommentInput').css('display', 'block');
        $('#CommentPart').css('display', 'block');
        $('#AuditDiv').css('display', 'block');
        $('#btnAddReport').css('display', 'none');
        $('#btnDeleteReport').css('display', 'none');
        $('#AuditDiv').css('display', 'block');
        $('#btnSaveDraft').css('display', 'none');
        $('#btnSaveReport').css('display', 'none');

        $('#btnSaveReport').after('<div class="tooltiparealeft expand" data-title="Notifies Auditor on the completion of action plan and request for a re-audit to validate its effectvitity"><input class="ms-Button ms-Button--primary" id="btnTagForAudit" type="button" value="Tag for Audit" style="font-size: small; font-family: inherit; color: white; font-weight: 600; background-color: #4b3832; border-color: #4b3832; border-radius: 5px;width:99px;font-size:11px;float: right;"></a>');
        $('input#btnTagForAudit').click(function () { //button Save Report click event

          var comments = $('#txtComments').val().trim();
          if (comments.length > 0) { //Add Confirmation Message
            var html = document.createElement('div');
            html.innerHTML = '<h2>Comments below will not be saved. If you wish to save the comment click Add Comment first otherwise continue. </h2>\
            <br>\
            <div class="ms-core-form-section"><div class="ms-core-form-bottomButtonBox"><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.OK); return false;">Continue</button><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.cancel); return false;">Cancel</button>\
            </div></div>'
            OpenPopUpPageWithDialogOptions({
              title: "Warning",
              html: html,
              dialogReturnValueCallback: function (dialogResult) {
                if (dialogResult == 1) {
                  SavingItemByStatus("CAP-For Final Evaluation");
                }
              }
            });
          } else {
            SavingItemByStatus("CAP-For Final Evaluation");
          }

        });


        $('input#btnAddComment').click(function () { //button Save Report click event
          var validation = {
            Fields: {
              txtComments: {
                required: true
              }
            },
            FieldType: {
              txtComments: {
                type: "textarea"
              }
            },
            message: {
              txtComments: {
                required: "This field is required",
              }
            }

          }
          if (Utility.Validate(validation)) {
            $('#s4-bodyContainer').addClass('blur-filter');
            $('#s4-workspace').after('<div id="loading-bar-spinner" class="spinner"><div class="spinner-icon"></div></div>');
            setTimeout(function () {
              var ajax = updateComment();
              ajax.done(function (data, xhr) {
                retrieveCommentSPA();
                $($('textarea[id^="txtComments"]')[0]).val('');
                $(".blur-filter").removeClass("blur-filter");
                $("#loading-bar-spinner").remove();

              }).fail(function (sender, args) {
                console.log(args);
              });
            }, 1000);
          } else {
            return false;
          }

        });

        $('#txtAuditFile').change(function () {
          var layout = "";
          var renderItems = true;
          var inputs = $(this);
          var sFileName;
          var _validFileExtensions = ValidFiles;

          FileLoop:
          for (var i = 0; i < $(this).get(0).files.length; i++) {
            var blnValid = false;
            sFileName = $(this).get(0).files[i].name;
            Extension:
            for (var j = 0; j < _validFileExtensions.length; j++) {
              var sCurExtension = _validFileExtensions[j];
              var sFileExtension = getFileExtension3(sFileName)
              if (sFileExtension.toLowerCase() === sCurExtension.toLowerCase()) {
                blnValid = true;
              }
            }

            if (!blnValid) {
              alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
              inputs.replaceWith(inputs.val('').clone(true));
              renderItems = false;
              break FileLoop;
            }

          }

          if (renderItems) {
            var layout = "";
            $(this).next('div').find('div').find('div[id^="NewdummyLink"]').remove();
            for (var i = 0; i < $(this).get(0).files.length; i++) {
              var fileName = $(this).get(0).files[i].name;
              var link = Application["SiteName"] + "/AuditFile/" + $('input[id^="txtNCNumber"]').val() + "/" + fileName;
              layout += '<div id="NewdummyLink">';
              layout += '<span class="ms-newdocument-iconouter"><img class="ms-newdocument-icon" src="/_layouts/15/images/spcommon.png?rev=44#ThemeKey=" alt="new" title="new"></span><span><a target="_blank" a href="' + link + '"><img class="ms-asset-icon ms-rtePosition-4" src="' + getIcon(fileName) + '">' + fileName + '</a></span>';
              layout += '</div>';
            }
            $(this).next('div').find('div[id="Attachments"]').prepend(layout);
          }

          function getFileExtension3(filename) {
            return filename.slice((filename.lastIndexOf(".") - 1 >>> 0) + 1);
          }

        });

      } else {
        DisableForms("AuditDiv", true);
        $('#AuditDiv').css('display', 'block');
        $('#CAPDiv').css("display", 'block');
        $(".sp-peoplepicker-delImage").hide();
        $('#NCAssigneeDiv').css('display', 'block');
        $('#RecepientDiv').css('display', 'block');
        $('input[type^="button"]').css("display", 'none');
        DisableForms("NCInformationDivMain", true);
        //UnableToView("You don't have Permission to View the Item", link);
      }
    } else {
      if (Assignee == _spPageContextInfo.userDisplayName || groups.includes("Networks Administrator")) {
        swapDivs('peoplePickerDiv2', 'peoplePickerDiv4');
        $('#peoplePickerDiv2').parent('div').css('display', 'none');
        $('#AuditDiv').css('display', 'block');
        $(".sp-peoplepicker-delImage").hide();
        $('#RecepientDiv').css('display', 'none');
        $('#CAPDiv').css("display", 'block');
        DisableForms("NCInformationDiv", true);
        DisableForms("NCReport", true);
        DisableForms("CAPDiv", true);
        DisableForms("peoplePickerDiv4", false);
        $('#NCAssigneeDiv').css('display', 'block');
        $('#RecepientDiv').css('display', 'none');
        $('#CommentInput').css('display', 'block');
        $('#CommentPart').css('display', 'block');
        $('#btnAddReport').css('display', 'none');
        $('#btnDeleteReport').css('display', 'none');
        $('#btnSaveDraft').css('display', 'none');
        $('#btnSaveReport').css('display', 'none');
        $('#AuditDiv').css('display', 'block');
        addRemoveAttachmentsButton($('#txtAuditFile'));


        $('#btnSaveReport').after('<div class="tooltiparealeft expand" data-title="Notifies auditor on completion of implementation"><input class="ms-Button ms-Button--primary" id="btnTagForAudit" type="button" value="Tag for Audit" style="font-size: small; font-family: inherit; color: white; font-weight: 600; background-color: #4b3832; border-color: #4b3832; border-radius: 5px;width:99px;font-size:11px;float: right;"></div>');
        $('input#btnTagForAudit').click(function () { //button Save Report click event

          var comments = $('#txtComments').val().trim();
          if (comments.length > 0) { //Add Confirmation Message
            var html = document.createElement('div');
            html.innerHTML = '<h2>Comments below will not be saved. If you wish to save the comment click Add Comment first otherwise continue. </h2>\
            <br>\
            <div class="ms-core-form-section"><div class="ms-core-form-bottomButtonBox"><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.OK); return false;">Continue</button><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.cancel); return false;">Cancel</button>\
            </div></div>'
            OpenPopUpPageWithDialogOptions({
              title: "Confirmation",
              html: html,
              dialogReturnValueCallback: function (dialogResult) {
                if (dialogResult == 1) {
                  SavingItemByStatus("CAP-For Final Evaluation");
                }
              }
            });
          } else {
            SavingItemByStatus("CAP-For Final Evaluation");
          }
        });

        $('input#btnAddComment').click(function () { //button Save Report click event
          var validation = {
            Fields: {
              txtComments: {
                required: true
              }
            },
            FieldType: {
              txtComments: {
                type: "textarea"
              }
            },
            message: {
              txtComments: {
                required: "This field is required",
              }
            }

          }
          if (Utility.Validate(validation)) {
            $('#s4-bodyContainer').addClass('blur-filter');
            $('#s4-workspace').after('<div id="loading-bar-spinner" class="spinner"><div class="spinner-icon"></div></div>');
            setTimeout(function () {
              var ajax = updateComment();
              ajax.done(function (data, xhr) {
                retrieveCommentSPA();
                $($('textarea[id^="txtComments"]')[0]).val('');
                $(".blur-filter").removeClass("blur-filter");
                $("#loading-bar-spinner").remove();

              }).fail(function (sender, args) {
                console.log(args);
              });
            }, 1000);
          } else {
            return false;
          }

        });

        $('#txtAuditFile').change(function () {
          var layout = "";
          var renderItems = true;
          var inputs = $(this);
          var sFileName;
          var _validFileExtensions = ValidFiles;

          FileLoop:
          for (var i = 0; i < $(this).get(0).files.length; i++) {
            var blnValid = false;
            sFileName = $(this).get(0).files[i].name;
            Extension:
            for (var j = 0; j < _validFileExtensions.length; j++) {
              var sCurExtension = _validFileExtensions[j];
              var sFileExtension = getFileExtension3(sFileName)
              if (sFileExtension.toLowerCase() === sCurExtension.toLowerCase()) {
                blnValid = true;
              }
            }

            if (!blnValid) {
              alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
              inputs.replaceWith(inputs.val('').clone(true));
              renderItems = false;
              break FileLoop;
            }

          }

          if (renderItems) {
            var layout = "";
            $(this).next('div').find('div').find('div[id^="NewdummyLink"]').remove();
            for (var i = 0; i < $(this).get(0).files.length; i++) {
              var fileName = $(this).get(0).files[i].name;
              var link = Application["SiteName"] + "/AuditFile/" + $('input[id^="txtNCNumber"]').val() + "/" + fileName;
              layout += '<div id="NewdummyLink">';
              layout += '<span class="ms-newdocument-iconouter"><img class="ms-newdocument-icon" src="/_layouts/15/images/spcommon.png?rev=44#ThemeKey=" alt="new" title="new"></span><span><a target="_blank" a href="' + link + '"><img class="ms-asset-icon ms-rtePosition-4" src="' + getIcon(fileName) + '">' + fileName + '</a></span>';
              layout += '</div>';
            }
            $(this).next('div').find('div[id="Attachments"]').prepend(layout);
          }

          function getFileExtension3(filename) {
            return filename.slice((filename.lastIndexOf(".") - 1 >>> 0) + 1);
          }

        });

      } else {
        DisableForms("AuditDiv", true);
        swapDivs('peoplePickerDiv2', 'peoplePickerDiv4');
        $('#peoplePickerDiv2').parent('div').css('display', 'none');
        $('#AuditDiv').css('display', 'block');
        $('#CAPDiv').css("display", 'block');
        $(".sp-peoplepicker-delImage").hide();
        $('#NCAssigneeDiv').css('display', 'block');
        $('#RecepientDiv').css('display', 'none');
        $('input[type^="button"]').css("display", 'none');
        DisableForms("NCInformationDivMain", true);
        //UnableToView("You don't have Permission to View the Item", link);
      }
    }
    showCancelButton(displayname);
  }

  function fnCAPApproval(Assignee, groups, ReportType) {

    if (ReportType.toUpperCase() != "BRU") {
      var displayname = _spPageContextInfo.userDisplayName;
      var actor = getUserInfoMulti("peoplePickerDiv2");
      if (actor.includes(displayname) || groups.includes("Networks Administrator")) {
        $(".sp-peoplepicker-delImage").hide();
        $('#CAPDiv').css("display", 'block');
        DisableForms("NCInformationDiv", true);
        DisableForms("NCReport", true);
        DisableForms("CAPDiv", true);
        DisableForms("peoplePickerDiv4", false);
        $('#NCAssigneeDiv').css('display', 'block');
        $('#RecepientDiv').css('display', 'block');
        $('#CommentInput').css('display', 'block');
        $('#CommentPart').css('display', 'block');
        $('#btnAddReport').css('display', 'none');
        $('#btnDeleteReport').css('display', 'none');
        $('#btnSaveDraft').css('display', 'none');
        $('#btnSaveReport').css('display', 'none');
        $('#btnSaveReport').after('<div class="tooltiparea expand" data-title="Returns Corrective/Preventive Action Plan to Auditor for revision. Note: Ensure to add a comment for needed revision"><input class="ms-Button ms-Button--primary" id="btnReturn" type="button" value="Return NC Report" style="font-size: small; font-family: inherit; color: white; font-weight: 600; background-color: #4b3832; border-color: #4b3832; border-radius: 5px;width:99px;font-size:9px;white-space: normal;float: right;"></div>');
        $('#btnSaveReport').after('<div class="tooltiparealeft expand" data-title="Approves Corrective/Preventive Action Plan & notifies audited office to begin implementation"><input class="ms-Button ms-Button--primary" id="btnCAPApprove" type="button" value="Approve CAP" style="font-size: small; font-family: inherit; color: white; font-weight: 600; background-color: #4b3832; border-color: #4b3832; border-radius: 5px;width:99px;font-size:9px;float: right;"></div>');
        $('input#btnAddComment').click(function () { //button Save Report click event

          var validation = {
            Fields: {
              txtComments: {
                required: true
              }
            },
            FieldType: {
              txtComments: {
                type: "textarea"
              }
            },
            message: {
              txtComments: {
                required: "This field is required",
              }
            }

          }
          if (Utility.Validate(validation)) {
            $('#s4-bodyContainer').addClass('blur-filter');
            $('#s4-workspace').after('<div id="loading-bar-spinner" class="spinner"><div class="spinner-icon"></div></div>');
            setTimeout(function () {
              var ajax = updateComment();
              ajax.done(function (data, xhr) {
                retrieveCommentSPA();
                $($('textarea[id^="txtComments"]')[0]).val('');
                $(".blur-filter").removeClass("blur-filter");
                $("#loading-bar-spinner").remove();

              }).fail(function (sender, args) {
                console.log(args);
              });
            }, 1000);
          } else {
            return false;
          }
        });

        $('input#btnReturn').click(function () { //button Save Report click event
          var ID = getQueryString("ID");
          var comments = $('#txtComments').val().trim();
          if (comments.length > 0) { //Add Confirmation Message
            var html = document.createElement('div');
            html.innerHTML = '<h2>Comments below will not be saved. If you wish to save the comment click Add Comment first otherwise continue. </h2>\
              <br>\
              <div class="ms-core-form-section"><div class="ms-core-form-bottomButtonBox"><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.OK); return false;">Continue</button><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.cancel); return false;">Cancel</button>\
              </div></div>'
            OpenPopUpPageWithDialogOptions({
              title: "Confirmation",
              html: html,
              dialogReturnValueCallback: function (dialogResult) {
                if (dialogResult == 1) {
                  SavingItemByStatus("CAP-For Auditors Assessment");
                }
              }
            });
          } else {
            SavingItemByStatus("CAP-For Auditors Assessment");
          }

        });

        $('input#btnCAPApprove').click(function () { //button Save Report click event
          var ID = getQueryString("ID");
          var comments = $('#txtComments').val().trim();
          if (comments.length > 0) { //Add Confirmation Message
            var html = document.createElement('div');
            html.innerHTML = '<h2>Comments below will not be saved. If you wish to save the comment click Add Comment first otherwise continue. </h2>\
              <br>\
              <div class="ms-core-form-section"><div class="ms-core-form-bottomButtonBox"><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.OK); return false;">Continue</button><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.cancel); return false;">Cancel</button>\
              </div></div>'
            OpenPopUpPageWithDialogOptions({
              title: "Confirmation",
              html: html,
              dialogReturnValueCallback: function (dialogResult) {
                if (dialogResult == 1) {
                  SavingItemByStatus("CAP-For Implementation");
                }
              }
            });
          } else {
            SavingItemByStatus("CAP-For Implementation");
          }

        });
      } else {
        $(".sp-peoplepicker-delImage").hide();
        $('#CAPDiv').css("display", 'block');
        $('#NCAssigneeDiv').css('display', 'block');
        $('#RecepientDiv').css('display', 'block');
        $('input[type^="button"]').css("display", 'none');
        DisableForms("NCInformationDivMain", true);
      }
    } else {
      var displayname = _spPageContextInfo.userDisplayName;
      var actor = getUserInfoMulti("peoplePickerDiv");
      if (actor.includes(displayname) || groups.includes("Networks Administrator")) {
        swapDivs('peoplePickerDiv2', 'peoplePickerDiv4');
        $('#peoplePickerDiv2').parent('div').css('display', 'none');
        $(".sp-peoplepicker-delImage").hide();
        $('#RecepientDiv').css('display', 'none');
        $('#CAPDiv').css("display", 'block');
        DisableForms("NCInformationDiv", true);
        DisableForms("NCReport", true);
        DisableForms("CAPDiv", true);
        DisableForms("peoplePickerDiv4", false);
        $('#NCAssigneeDiv').css('display', 'block');
        $('#RecepientDiv').css('display', 'none');
        $('#CommentInput').css('display', 'block');
        $('#CommentPart').css('display', 'block');
        $('#btnAddReport').css('display', 'none');
        $('#btnDeleteReport').css('display', 'none');
        $('#btnSaveDraft').css('display', 'none');
        $('#btnSaveReport').css('display', 'none');

        $('#btnSaveReport').after('<div class="tooltiparea expand" data-title="Returns CAP to assignee for revision"><input class="ms-Button ms-Button--primary" id="btnReturn" type="button" value="Return NC Report" style="font-family: inherit; color: white; font-weight: 600; background-color: #4b3832; border-color: #4b3832; border-radius: 5px;width:99px;font-size:9px;float: right;"></div>');
        //$('#btnSaveReport').after('<div class="tooltiparealeft expand" data-title="Releases Corrective/Preventive Action Plan to Auditor for review"><input class="ms-Button ms-Button--primary" id="btnCAPRelease" type="button" value="Release CAP" style="font-size: small; font-family: inherit; color: white; font-weight: 600; background-color: #4b3832; border-color: #4b3832; border-radius: 5px;width:99px;font-size:10px;float: right;"></div>');
        $('#btnSaveReport').after('<div class="tooltiparealeft expand" data-title="Approves Corrective/Preventive Action Plan & notifies audited office to begin implementation"><input class="ms-Button ms-Button--primary" id="btnCAPRelease" type="button" value="Approve CAP" style="font-size: small; font-family: inherit; color: white; font-weight: 600; background-color: #4b3832; border-color: #4b3832; border-radius: 5px;width:99px;font-size:9px;float: right;"></div>');
        $('input#btnAddComment').click(function () { //button Save Report click event
          var validation = {
            Fields: {
              txtComments: {
                required: true
              }
            },
            FieldType: {
              txtComments: {
                type: "textarea"
              }
            },
            message: {
              txtComments: {
                required: "This field is required",
              }
            }

          }
          if (Utility.Validate(validation)) {
            $('#s4-bodyContainer').addClass('blur-filter');
            $('#s4-workspace').after('<div id="loading-bar-spinner" class="spinner"><div class="spinner-icon"></div></div>');
            setTimeout(function () {
              var ajax = updateComment();
              ajax.done(function (data, xhr) {
                retrieveCommentSPA();
                $($('textarea[id^="txtComments"]')[0]).val('');
                $(".blur-filter").removeClass("blur-filter");
                $("#loading-bar-spinner").remove();

              }).fail(function (sender, args) {
                console.log(args);
              });
            }, 1000);
          } else {
            return false;
          }
        });

        $('input#btnReturn').click(function () { //button Save Report click event
          CommentFlagValue = false;
          var comments = $('#txtComments').val().trim();
          if (comments.length > 0) { //Add Confirmation Message
            var html = document.createElement('div');
            html.innerHTML = '<h2>Comments below will not be saved. If you wish to save the comment click Add Comment first otherwise continue. </h2>\
              <br>\
              <div class="ms-core-form-section"><div class="ms-core-form-bottomButtonBox"><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.OK); return false;">Continue</button><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.cancel); return false;">Cancel</button>\
              </div></div>'
            OpenPopUpPageWithDialogOptions({
              title: "Confirmation",
              html: html,
              dialogReturnValueCallback: function (dialogResult) {
                if (dialogResult == 1) {
                  SavingItemByStatus("CAP-For Revision");
                }
              }
            });
          } else {

            SavingItemByStatus("CAP-For Revision");
          }

        });

        $('input#btnCAPRelease').click(function () { //button Save Report click event
          CommentFlagValue = false;
          var ID = getQueryString("ID");
          var comments = $('#txtComments').val().trim();
          if (comments.length > 0) { //Add Confirmation Message
            var html = document.createElement('div');
            html.innerHTML = '<h2>Comments below will not be saved. If you wish to save the comment click Add Comment first otherwise continue. </h2>\
              <br>\
              <div class="ms-core-form-section"><div class="ms-core-form-bottomButtonBox"><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.OK); return false;">Continue</button><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.cancel); return false;">Cancel</button>\
              </div></div>'
            OpenPopUpPageWithDialogOptions({
              title: "Confirmation",
              html: html,
              dialogReturnValueCallback: function (dialogResult) {
                if (dialogResult == 1) {
                  SavingItemByStatus("CAP-For Implementation");
                }
              }
            });
          } else {
            SavingItemByStatus("CAP-For Implementation");
          }

        });
      } else {
        swapDivs('peoplePickerDiv2', 'peoplePickerDiv4');
        $('#peoplePickerDiv2').parent('div').css('display', 'none');
        $(".sp-peoplepicker-delImage").hide();
        $('#CAPDiv').css("display", 'block');
        $('#CommentPart').css('display', 'block');
        $('#NCAssigneeDiv').css('display', 'block');
        $('#RecepientDiv').css('display', 'none');
        $('input[type^="button"]').css("display", 'none');
        $('#CAPDiv').css("display", 'block');
        DisableForms("NCInformationDivMain", true);
      }
    }
    showCancelButton(displayname);
  }

  function fnCAPAuditorAssessment(Assignee, groups) {
    var displayname = _spPageContextInfo.userDisplayName;
    var actor = getUserInfoMulti("peoplePickerDiv");
    if (actor.includes(displayname) || groups.includes("Networks Administrator")) {
      $(".sp-peoplepicker-delImage").hide();
      $('#CAPDiv').css("display", 'block');
      DisableForms("NCInformationDiv", true);
      DisableForms("NCReport", true);
      DisableForms("CAPDiv", true);
      DisableForms("peoplePickerDiv4", false);
      $('#NCAssigneeDiv').css('display', 'block');
      $('#RecepientDiv').css('display', 'block');
      $('#CommentInput').css('display', 'block');
      $('#CommentPart').css('display', 'block');
      $('#btnAddReport').css('display', 'none');
      $('#btnDeleteReport').css('display', 'none');
      $('#btnSaveDraft').css('display', 'none');
      $('#btnSaveReport').css('display', 'none');
      $('#btnSaveReport').after('<div class="tooltiparea expand" data-title="Returns Corrective/Preventive Action Plan to Assignee for revision. Note: Ensure to add a comment for needed revision"><input class="ms-Button ms-Button--primary" id="btnReturn" type="button" value="Return NC Report" style="font-size: small; font-family: inherit; color: white; font-weight: 600; background-color: #4b3832; border-color: #4b3832; border-radius: 5px;width:99px;font-size:9px;float: right;"></div>');
      $('#btnSaveReport').after('<div class="tooltiparealeft expand" data-title="Submits Corrective/Preventive Action Plan to Lead Auditor for review"><input class="ms-Button ms-Button--primary" id="btnEndorseCAP" type="button" value="Endorse CAP" style="font-size: small; font-family: inherit; color: white; font-weight: 600; background-color: #4b3832; border-color: #4b3832; border-radius: 5px;width:99px;font-size:10px;float: right;"></div>');
      $('input#btnAddComment').click(function () { //button Save Report click event
        var validation = {
          Fields: {
            txtComments: {
              required: true
            }
          },
          FieldType: {
            txtComments: {
              type: "textarea"
            }
          },
          message: {
            txtComments: {
              required: "This field is required",
            }
          }

        }
        if (Utility.Validate(validation)) {
          $('#s4-bodyContainer').addClass('blur-filter');
          $('#s4-workspace').after('<div id="loading-bar-spinner" class="spinner"><div class="spinner-icon"></div></div>');
          setTimeout(function () {
            var ajax = updateComment();
            ajax.done(function (data, xhr) {
              retrieveCommentSPA();
              $($('textarea[id^="txtComments"]')[0]).val('');
              $(".blur-filter").removeClass("blur-filter");
              $("#loading-bar-spinner").remove();

            }).fail(function (sender, args) {
              console.log(args);
            });
          }, 1000);
        } else {
          return false;
        }
      });

      $('input#btnReturn').click(function () { //button Save Report click event
        var ID = getQueryString("ID");
        //var comments = retrieveComment('NCReport', ID, 'Comments', true);
        var comments = $('#txtComments').val().trim();
        if (comments.length > 0) { //Add Confirmation Message
          var html = document.createElement('div');
          html.innerHTML = '<h2>Comments below will not be saved. If you wish to save the comment click Add Comment first otherwise continue. </h2>\
            <br>\
            <div class="ms-core-form-section"><div class="ms-core-form-bottomButtonBox"><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.OK); return false;">Continue</button><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.cancel); return false;">Cancel</button>\
            </div></div>'
          OpenPopUpPageWithDialogOptions({
            title: "Confirmation",
            html: html,
            dialogReturnValueCallback: function (dialogResult) {
              if (dialogResult == 1) {
                SavingItemByStatus("CAP-For Revision");
              }
            }
          });
        } else {
          SavingItemByStatus("CAP-For Revision");
        }

      });

      $('input#btnEndorseCAP').click(function () { //button Save Report click event
        var ID = getQueryString("ID");
        var comments = $('#txtComments').val().trim();
        if (comments.length > 0) { //Add Confirmation Message
          var html = document.createElement('div');
          html.innerHTML = '<h2>Comments below will not be saved. If you wish to save the comment click Add Comment first otherwise continue. </h2>\
            <br>\
            <div class="ms-core-form-section"><div class="ms-core-form-bottomButtonBox"><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.OK); return false;">Continue</button><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.cancel); return false;">Cancel</button>\
            </div></div>'
          OpenPopUpPageWithDialogOptions({
            title: "Confirmation",
            html: html,
            dialogReturnValueCallback: function (dialogResult) {
              if (dialogResult == 1) {
                SavingItemByStatus("CAP-For Approval"); //the function used has no validation
              }
            }
          });
        } else {
          SavingItemByStatus("CAP-For Approval"); //the function used has no validation
        }

      });
    } else {
      $(".sp-peoplepicker-delImage").hide();
      $('#CAPDiv').css("display", 'block');
      $('#CommentPart').css('display', 'block');
      $('#NCAssigneeDiv').css('display', 'block');
      $('#RecepientDiv').css('display', 'block');
      $('input[type^="button"]').css("display", 'none');
      DisableForms("NCInformationDivMain", true);
    }

    showCancelButton(displayname);
  }

  function fnCAPRevision(Assignee, groups, ReportType) {
    if (ReportType.toUpperCase() != "BRU") {
      if (Assignee == _spPageContextInfo.userDisplayName || groups.includes("Networks Administrator")) {
        $(".sp-peoplepicker-delImage").hide();
        $('#CAPDiv').css("display", 'block');
        DisableForms("NCInformationDiv", true);
        DisableForms("NCReport", true);
        DisableForms("CAPDiv", false);
        DisableForms("peoplePickerDiv4", false);
        $('#NCAssigneeDiv').css('display', 'block');
        $('#RecepientDiv').css('display', 'block');
        $('#CommentInput').css('display', 'block');
        $('#CommentPart').css('display', 'block');
        $('#btnAddReport').css('display', 'none');
        $('#btnDeleteReport').css('display', 'none');
        $('#btnSaveDraft').css('display', 'none');
        $('#btnSaveReport').css('display', 'none');
        addRemoveAttachmentsButton($('#txtRAC'));
        addRemoveAttachmentsButton($('#txtCAP'));

        $('#txtCAP').change(function () {
          var renderItems = true;
          var inputs = $(this);
          var sFileName;
          var _validFileExtensions = ValidFiles;

          FileLoop:
          for (var i = 0; i < $(this).get(0).files.length; i++) {
            var blnValid = false;
            sFileName = $(this).get(0).files[i].name;
            Extension:
            for (var j = 0; j < _validFileExtensions.length; j++) {
              var sCurExtension = _validFileExtensions[j];
              var sFileExtension = getFileExtension3(sFileName)
              if (sFileExtension.toLowerCase() === sCurExtension.toLowerCase()) {
                blnValid = true;
              }
            }

            if (!blnValid) {
              alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
              inputs.replaceWith(inputs.val('').clone(true));
              renderItems = false;
              break FileLoop;
            }

          }

          if (renderItems) {

            var layout = "";
            $(this).next('div').find('div').find('div[id^="NewdummyLink"]').remove();
            for (var i = 0; i < $(this).get(0).files.length; i++) {
              var fileName = $(this).get(0).files[i].name;
              var link = Application["SiteName"] + "/CAP/" + $('input[id^="txtNCNumber"]').val() + "/" + fileName;
              layout += '<div id="NewdummyLink">';
              layout += '<span class="ms-newdocument-iconouter"><img class="ms-newdocument-icon" src="/_layouts/15/images/spcommon.png?rev=44#ThemeKey=" alt="new" title="new"></span><span><a target="_blank" href="' + link + '"><img class="ms-asset-icon ms-rtePosition-4" src="' + getIcon(fileName) + '">' + fileName + '</a></span>';
              layout += '</div>';
            }
            $(this).next('div').find('div[id="Attachments"]').prepend(layout);
          }

          function getFileExtension3(filename) {
            return filename.slice((filename.lastIndexOf(".") - 1 >>> 0) + 1);
          }

        });

        $('#txtRAC').change(function () {
          var layout = "";
          var renderItems = true;
          var inputs = $(this);
          var sFileName;
          var _validFileExtensions = ValidFiles;

          FileLoop:
          for (var i = 0; i < $(this).get(0).files.length; i++) {
            var blnValid = false;
            sFileName = $(this).get(0).files[i].name;
            Extension:
            for (var j = 0; j < _validFileExtensions.length; j++) {
              var sCurExtension = _validFileExtensions[j];
              var sFileExtension = getFileExtension3(sFileName)
              if (sFileExtension.toLowerCase() === sCurExtension.toLowerCase()) {
                blnValid = true;
              }
            }

            if (!blnValid) {
              alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
              inputs.replaceWith(inputs.val('').clone(true));
              renderItems = false;
              break FileLoop;
            }

          }

          if (renderItems) {
            var layout = "";
            $(this).next('div').find('div').find('div[id^="NewdummyLink"]').remove();
            for (var i = 0; i < $(this).get(0).files.length; i++) {
              var fileName = $(this).get(0).files[i].name;
              var link = Application["SiteName"] + "/RCA/" + $('input[id^="txtNCNumber"]').val() + "/" + fileName;
              layout += '<div id="NewdummyLink">';
              layout += '<span class="ms-newdocument-iconouter"><img class="ms-newdocument-icon" src="/_layouts/15/images/spcommon.png?rev=44#ThemeKey=" alt="new" title="new"></span><span><a target="_blank" a href="' + link + '"><img class="ms-asset-icon ms-rtePosition-4" src="' + getIcon(fileName) + '">' + fileName + '</a></span>';
              layout += '</div>';
            }
            $(this).next('div').find('div[id="Attachments"]').prepend(layout);
          }

          function getFileExtension3(filename) {
            return filename.slice((filename.lastIndexOf(".") - 1 >>> 0) + 1);
          }

        });


        $('#btnSaveReport').after('<a class="tooltipbutton expand" data-title="Save report and send later"><input class="ms-Button ms-Button--primary" id="btnUpdateReport" type="button" value="Save as Draft" style="font-size: small; font-family: inherit; color: white; font-weight: 600; background-color: #4b3832; border-color: #4b3832; border-radius: 5px;width:99px;font-size:11px;float: right;"></a>');
        $('#btnSaveReport').after('<div class="tooltiparealeft expand" data-title="Submits Corrective/Preventive Action Plan to Manager/BRU Head for review"><input class="ms-Button ms-Button--primary" id="btnSubmitCAP" type="button" value="Submit CAP" style="font-size: small; font-family: inherit; color: white; font-weight: 600; background-color: #4b3832; border-color: #4b3832; border-radius: 5px;width:99px;font-size:10px;float: right;"></div>');

        $('input#btnAddComment').click(function () { //button Save Report click event
          var validation = {
            Fields: {
              txtComments: {
                required: true
              }
            },
            FieldType: {
              txtComments: {
                type: "textarea"
              }
            },
            message: {
              txtComments: {
                required: "This field is required",
              }
            }

          }
          if (Utility.Validate(validation)) {
            $('#s4-bodyContainer').addClass('blur-filter');
            $('#s4-workspace').after('<div id="loading-bar-spinner" class="spinner"><div class="spinner-icon"></div></div>');
            setTimeout(function () {
              var ajax = updateComment();
              ajax.done(function (data, xhr) {
                retrieveCommentSPA();
                $($('textarea[id^="txtComments"]')[0]).val('');
                $(".blur-filter").removeClass("blur-filter");
                $("#loading-bar-spinner").remove();

              }).fail(function (sender, args) {
                console.log(args);
              });
            }, 1000);
          } else {
            return false;
          }

        });

        $('input#btnUpdateReport').click(function () { //button Save Report click event
          CommentFlagValue = true;
          var comments = $('#txtComments').val().trim();
          if (comments.length > 0) { //Add Confirmation Message
            var html = document.createElement('div');
            html.innerHTML = '<h2>Comments below will not be saved. If you wish to save the comment click Add Comment first otherwise continue. </h2>\
              <br>\
              <div class="ms-core-form-section"><div class="ms-core-form-bottomButtonBox"><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.OK); return false;">Continue</button><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.cancel); return false;">Cancel</button>\
              </div></div>'
            OpenPopUpPageWithDialogOptions({
              title: "Confirmation",
              html: html,
              dialogReturnValueCallback: function (dialogResult) {
                if (dialogResult == 1) {
                  UpdateCorrectiveActionPlan("CAP-For Revision");
                }
              }
            });
          } else {
            UpdateCorrectiveActionPlan("CAP-For Revision");
          }
        });

        $('input#btnSubmitCAP').click(function () { //button Save Report click event
          CommentFlagValue = false;
          var comments = $('#txtComments').val().trim();
          if (comments.length > 0) { //Add Confirmation Message
            var html = document.createElement('div');
            html.innerHTML = '<h2>Comments below will not be saved. If you wish to save the comment click Add Comment first otherwise continue. </h2>\
              <br>\
              <div class="ms-core-form-section"><div class="ms-core-form-bottomButtonBox"><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.OK); return false;">Continue</button><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.cancel); return false;">Cancel</button>\
              </div></div>'
            OpenPopUpPageWithDialogOptions({
              title: "Confirmation",
              html: html,
              dialogReturnValueCallback: function (dialogResult) {
                if (dialogResult == 1) {
                  UpdateCorrectiveActionPlan("CAP-For Review");
                }
              }
            });
          } else {
            UpdateCorrectiveActionPlan("CAP-For Review");
          }

        });
      } else {
        $(".sp-peoplepicker-delImage").hide();
        $('#CAPDiv').css("display", 'block');
        $('#CommentPart').css('display', 'block');
        $('#NCAssigneeDiv').css('display', 'block');
        $('#RecepientDiv').css('display', 'block');
        $('input[type^="button"]').css("display", 'none');
        DisableForms("NCInformationDivMain", true);
      }
    } else {
      if (Assignee == _spPageContextInfo.userDisplayName || groups.includes("Networks Administrator")) {
        swapDivs('peoplePickerDiv2', 'peoplePickerDiv4');
        $('#peoplePickerDiv2').parent('div').css('display', 'none');
        $(".sp-peoplepicker-delImage").hide();
        $('#CAPDiv').css("display", 'block');
        DisableForms("NCInformationDiv", true);
        DisableForms("NCReport", true);
        DisableForms("CAPDiv", false);
        DisableForms("peoplePickerDiv4", false);
        $('#NCAssigneeDiv').css('display', 'block');
        $('#RecepientDiv').css('display', 'none');
        $('#CommentInput').css('display', 'block');
        $('#CommentPart').css('display', 'block');
        $('#btnAddReport').css('display', 'none');
        $('#btnDeleteReport').css('display', 'none');
        $('#btnSaveDraft').css('display', 'none');
        $('#btnSaveReport').css('display', 'none');
        addRemoveAttachmentsButton($('#txtRAC'));
        addRemoveAttachmentsButton($('#txtCAP'));
        $('#txtCAP').change(function () {
          var renderItems = true;
          var inputs = $(this);
          var sFileName;
          var _validFileExtensions = ValidFiles;

          FileLoop:
          for (var i = 0; i < $(this).get(0).files.length; i++) {
            var blnValid = false;
            sFileName = $(this).get(0).files[i].name;
            Extension:
            for (var j = 0; j < _validFileExtensions.length; j++) {
              var sCurExtension = _validFileExtensions[j];
              var sFileExtension = getFileExtension3(sFileName)
              if (sFileExtension.toLowerCase() === sCurExtension.toLowerCase()) {
                blnValid = true;
              }
            }

            if (!blnValid) {
              alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
              inputs.replaceWith(inputs.val('').clone(true));
              renderItems = false;
              break FileLoop;
            }

          }

          if (renderItems) {

            var layout = "";
            $(this).next('div').find('div').find('div[id^="NewdummyLink"]').remove();
            for (var i = 0; i < $(this).get(0).files.length; i++) {
              var fileName = $(this).get(0).files[i].name;
              var link = Application["SiteName"] + "/CAP/" + $('input[id^="txtNCNumber"]').val() + "/" + fileName;
              layout += '<div id="NewdummyLink">';
              layout += '<span class="ms-newdocument-iconouter"><img class="ms-newdocument-icon" src="/_layouts/15/images/spcommon.png?rev=44#ThemeKey=" alt="new" title="new"></span><span><a target="_blank" href="' + link + '"><img class="ms-asset-icon ms-rtePosition-4" src="' + getIcon(fileName) + '">' + fileName + '</a></span>';
              layout += '</div>';
            }
            //$(this).next('div').find('div').append(layout);
            $(this).next('div').find('div[id="Attachments"]').prepend(layout);
          }

          function getFileExtension3(filename) {
            return filename.slice((filename.lastIndexOf(".") - 1 >>> 0) + 1);
          }

        });

        $('#txtRAC').change(function () {
          var layout = "";
          var renderItems = true;
          var inputs = $(this);
          var sFileName;
          var _validFileExtensions = ValidFiles;

          FileLoop:
          for (var i = 0; i < $(this).get(0).files.length; i++) {
            var blnValid = false;
            sFileName = $(this).get(0).files[i].name;
            Extension:
            for (var j = 0; j < _validFileExtensions.length; j++) {
              var sCurExtension = _validFileExtensions[j];
              var sFileExtension = getFileExtension3(sFileName)
              if (sFileExtension.toLowerCase() === sCurExtension.toLowerCase()) {
                blnValid = true;
              }
            }

            if (!blnValid) {
              alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
              inputs.replaceWith(inputs.val('').clone(true));
              renderItems = false;
              break FileLoop;
            }

          }

          if (renderItems) {
            var layout = "";
            $(this).next('div').find('div').find('div[id^="NewdummyLink"]').remove();
            for (var i = 0; i < $(this).get(0).files.length; i++) {
              var fileName = $(this).get(0).files[i].name;
              var link = Application["SiteName"] + "/RCA/" + $('input[id^="txtNCNumber"]').val() + "/" + fileName;
              layout += '<div id="NewdummyLink">';
              layout += '<span class="ms-newdocument-iconouter"><img class="ms-newdocument-icon" src="/_layouts/15/images/spcommon.png?rev=44#ThemeKey=" alt="new" title="new"></span><span><a target="_blank" a href="' + link + '"><img class="ms-asset-icon ms-rtePosition-4" src="' + getIcon(fileName) + '">' + fileName + '</a></span>';
              layout += '</div>';
            }
            $(this).next('div').find('div[id="Attachments"]').prepend(layout);
          }

          function getFileExtension3(filename) {
            return filename.slice((filename.lastIndexOf(".") - 1 >>> 0) + 1);
          }

        });


        $('#btnSaveReport').after('<a class="tooltipbutton expand" data-title="Save report and send later"><input class="ms-Button ms-Button--primary" id="btnUpdateReport" type="button" value="Save as Draft" style="font-size: small; font-family: inherit; color: white; font-weight: 600; background-color: #4b3832; border-color: #4b3832; border-radius: 5px;width:99px;font-size:11px;float: right;"></a>');
        // $('#btnSaveReport').after('<div class="tooltiparealeft expand" data-title="Submits CAP for internal review"><input class="ms-Button ms-Button--primary" id="btnSubmitCAP" type="button" value="Submit CAP" style="font-size: small; font-family: inherit; color: white; font-weight: 600; background-color: #4b3832; border-color: #4b3832; border-radius: 5px;width:99px;font-size:10px;float: right;"></div>');
        $('#btnSaveReport').after('<div class="tooltiparealeft expand" data-title="Submits Corrective/Preventive Action Plan to Auditor for review"><input class="ms-Button ms-Button--primary" id="btnSubmitCAP" type="button" value="Submit CAP" style="font-size: small; font-family: inherit; color: white; font-weight: 600; background-color: #4b3832; border-color: #4b3832; border-radius: 5px;width:99px;font-size:10px;float: right;"></div>');
        $('input#btnAddComment').click(function () { //button Save Report click event
          var validation = {
            Fields: {
              txtComments: {
                required: true
              }
            },
            FieldType: {
              txtComments: {
                type: "textarea"
              }
            },
            message: {
              txtComments: {
                required: "This field is required",
              }
            }

          }
          if (Utility.Validate(validation)) {
            $('#s4-bodyContainer').addClass('blur-filter');
            $('#s4-workspace').after('<div id="loading-bar-spinner" class="spinner"><div class="spinner-icon"></div></div>');
            setTimeout(function () {
              var ajax = updateComment();
              ajax.done(function (data, xhr) {
                retrieveCommentSPA();
                $($('textarea[id^="txtComments"]')[0]).val('');
                $(".blur-filter").removeClass("blur-filter");
                $("#loading-bar-spinner").remove();

              }).fail(function (sender, args) {
                console.log(args);
              });
            }, 1000);
          } else {
            return false;
          }

        });

        $('input#btnUpdateReport').click(function () { //button Save Report click event
          CommentFlagValue = true;
          var comments = $('#txtComments').val().trim();
          if (comments.length > 0) { //Add Confirmation Message
            var html = document.createElement('div');
            html.innerHTML = '<h2>Comments below will not be saved. If you wish to save the comment click Add Comment first otherwise continue. </h2>\
              <br>\
              <div class="ms-core-form-section"><div class="ms-core-form-bottomButtonBox"><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.OK); return false;">Continue</button><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.cancel); return false;">Cancel</button>\
              </div></div>'
            OpenPopUpPageWithDialogOptions({
              title: "Confirmation",
              html: html,
              dialogReturnValueCallback: function (dialogResult) {
                if (dialogResult == 1) {
                  UpdateCorrectiveActionPlan("CAP-For Revision");
                }
              }
            });
          } else {
            UpdateCorrectiveActionPlan("CAP-For Revision");
          }


        });


        $('input#btnSubmitCAP').click(function () { //button Save Report click event
          CommentFlagValue = false;
          var comments = $('#txtComments').val().trim();
          if (comments.length > 0) { //Add Confirmation Message
            var html = document.createElement('div');
            html.innerHTML = '<h2>Comments below will not be saved. If you wish to save the comment click Add Comment first otherwise continue. </h2>\
              <br>\
              <div class="ms-core-form-section"><div class="ms-core-form-bottomButtonBox"><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.OK); return false;">Continue</button><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.cancel); return false;">Cancel</button>\
              </div></div>'
            OpenPopUpPageWithDialogOptions({
              title: "Confirmation",
              html: html,
              dialogReturnValueCallback: function (dialogResult) {
                if (dialogResult == 1) {
                  UpdateCorrectiveActionPlan("CAP-For Approval");
                }
              }
            });
          } else {
            UpdateCorrectiveActionPlan("CAP-For Approval");
          }

        });

      } else {
        swapDivs('peoplePickerDiv2', 'peoplePickerDiv4');
        $('#peoplePickerDiv2').parent('div').css('display', 'none');
        $(".sp-peoplepicker-delImage").hide();
        $('#CAPDiv').css("display", 'block');
        $('#CommentPart').css('display', 'block');
        $('#NCAssigneeDiv').css('display', 'block');
        $('#RecepientDiv').css('display', 'none');
        $('input[type^="button"]').css("display", 'none');
        DisableForms("NCInformationDivMain", true);
      }
    }
    showCancelButton(_spPageContextInfo.userDisplayName);
  }

  function fnCAPReview(Assignee, groups, ReportType) {
    if (ReportType.toUpperCase() != "BRU") {
      var displayname = _spPageContextInfo.userDisplayName;
      var actor = getUserInfoMulti("peoplePickerDiv3");
      if (actor.includes(displayname) || groups.includes("Networks Administrator")) {
        $(".sp-peoplepicker-delImage").hide();
        $('#CAPDiv').css("display", 'block');
        DisableForms("NCInformationDiv", true);
        DisableForms("NCReport", true);
        DisableForms("CAPDiv", true);
        DisableForms("peoplePickerDiv4", false);
        $('#NCAssigneeDiv').css('display', 'block');
        $('#RecepientDiv').css('display', 'block');
        $('#CommentInput').css('display', 'block');
        $('#CommentPart').css('display', 'block');
        $('#btnAddReport').css('display', 'none');
        $('#btnDeleteReport').css('display', 'none');
        $('#btnSaveDraft').css('display', 'none');
        $('#btnSaveReport').css('display', 'none');

        $('#btnSaveReport').after('<div class="tooltiparea expand" data-title="Returns Corrective/Preventive Action Plan to Assignee for revision"><input class="ms-Button ms-Button--primary" id="btnReturn" type="button" value="Return NC Report" style="font-family: inherit; color: white; font-weight: 600; background-color: #4b3832; border-color: #4b3832; border-radius: 5px;width:99px;font-size:9px;float: right;"></div>');
        $('#btnSaveReport').after('<div class="tooltiparealeft expand" data-title="Releases Corrective/Preventive Action Plan to Auditor for review"><input class="ms-Button ms-Button--primary" id="btnCAPRelease" type="button" value="Release CAP" style="font-size: small; font-family: inherit; color: white; font-weight: 600; background-color: #4b3832; border-color: #4b3832; border-radius: 5px;width:99px;font-size:10px;float: right;"></div>');

        $('input#btnReturn').click(function () { //button Save Report click event

          var comments = $('#txtComments').val().trim();
          if (comments.length > 0) { //Add Confirmation Message
            var html = document.createElement('div');
            html.innerHTML = '<h2>Comments below will not be saved. If you wish to save the comment click Add Comment first otherwise continue. </h2>\
              <br>\
              <div class="ms-core-form-section"><div class="ms-core-form-bottomButtonBox"><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.OK); return false;">Continue</button><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.cancel); return false;">Cancel</button>\
              </div></div>'
            OpenPopUpPageWithDialogOptions({
              title: "Confirmation",
              html: html,
              dialogReturnValueCallback: function (dialogResult) {
                if (dialogResult == 1) {
                  SavingItemByStatus("CAP-For Revision");
                }
              }
            });
          } else {
            SavingItemByStatus("CAP-For Revision");
          }

        });

        $('input#btnAddComment').click(function () { //button Save Report click event
          var validation = {
            Fields: {
              txtComments: {
                required: true
              }
            },
            FieldType: {
              txtComments: {
                type: "textarea"
              }
            },
            message: {
              txtComments: {
                required: "This field is required",
              }
            }

          }
          if (Utility.Validate(validation)) {
            $('#s4-bodyContainer').addClass('blur-filter');
            $('#s4-workspace').after('<div id="loading-bar-spinner" class="spinner"><div class="spinner-icon"></div></div>');
            setTimeout(function () {
              var ajax = updateComment();
              ajax.done(function (data, xhr) {
                retrieveCommentSPA();
                $($('textarea[id^="txtComments"]')[0]).val('');
                $(".blur-filter").removeClass("blur-filter");
                $("#loading-bar-spinner").remove();

              }).fail(function (sender, args) {
                console.log(args);
              });
            }, 1000);
          } else {
            return false;
          }
        });
        $('input#btnCAPRelease').click(function () { //button Save Report click event

          var ID = getQueryString("ID");
          //var comments = retrieveComment('NCReport', ID, 'Comments', true);
          var comments = $('#txtComments').val().trim();
          if (comments.length > 0) { //Add Confirmation Message
            var html = document.createElement('div');
            html.innerHTML = '<h2>Comments below will not be saved. If you wish to save the comment click Add Comment first otherwise continue. </h2>\
              <br>\
              <div class="ms-core-form-section"><div class="ms-core-form-bottomButtonBox"><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.OK); return false;">Continue</button><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.cancel); return false;">Cancel</button>\
              </div></div>'
            OpenPopUpPageWithDialogOptions({
              title: "Confirmation",
              html: html,
              dialogReturnValueCallback: function (dialogResult) {
                if (dialogResult == 1) {
                  SavingItemByStatus("CAP-For Auditors Assessment");
                }
              }
            });
          } else {

            SavingItemByStatus("CAP-For Auditors Assessment");
          }

        });
      } else {
        $(".sp-peoplepicker-delImage").hide();
        $('#CAPDiv').css("display", 'block');
        $('#CommentPart').css('display', 'block');
        $('#CommentPart').css('display', 'block');
        $('#NCAssigneeDiv').css('display', 'block');
        $('#RecepientDiv').css('display', 'block');
        $('input[type^="button"]').css("display", 'none');
        $('#CAPDiv').css("display", 'block');
        DisableForms("NCInformationDivMain", true);
        //UnableToView("You don't have Permission to View the Item", link);
      }
    } else {
      var displayname = _spPageContextInfo.userDisplayName;
      var actor = getUserInfoMulti("peoplePickerDiv");
      if (actor.includes(displayname) || groups.includes("Networks Administrator")) {
        swapDivs('peoplePickerDiv2', 'peoplePickerDiv4');
        $('#peoplePickerDiv2').parent('div').css('display', 'none');
        $(".sp-peoplepicker-delImage").hide();
        $('#RecepientDiv').css('display', 'none');
        $('#CAPDiv').css("display", 'block');
        DisableForms("NCInformationDiv", true);
        DisableForms("NCReport", true);
        DisableForms("CAPDiv", true);
        DisableForms("peoplePickerDiv4", false);
        $('#NCAssigneeDiv').css('display', 'block');
        $('#RecepientDiv').css('display', 'none');
        $('#CommentInput').css('display', 'block');
        $('#CommentPart').css('display', 'block');
        $('#btnAddReport').css('display', 'none');
        $('#btnDeleteReport').css('display', 'none');
        $('#btnSaveDraft').css('display', 'none');
        $('#btnSaveReport').css('display', 'none');

        $('#btnSaveReport').after('<div class="tooltiparea expand" data-title="Returns CAP to assignee for revision"><input class="ms-Button ms-Button--primary" id="btnReturn" type="button" value="Return NC Report" style="font-family: inherit; color: white; font-weight: 600; background-color: #4b3832; border-color: #4b3832; border-radius: 5px;width:99px;font-size:9px;float: right;"></div>');
        $('#btnSaveReport').after('<div class="tooltiparealeft expand" data-title="Releases Corrective/Preventive Action Plan to Auditor for review"><input class="ms-Button ms-Button--primary" id="btnCAPRelease" type="button" value="Release CAP" style="font-size: small; font-family: inherit; color: white; font-weight: 600; background-color: #4b3832; border-color: #4b3832; border-radius: 5px;width:99px;font-size:10px;float: right;"></div>');

        $('input#btnAddComment').click(function () { //button Save Report click event
          var validation = {
            Fields: {
              txtComments: {
                required: true
              }
            },
            FieldType: {
              txtComments: {
                type: "textarea"
              }
            },
            message: {
              txtComments: {
                required: "This field is required",
              }
            }

          }
          if (Utility.Validate(validation)) {
            $('#s4-bodyContainer').addClass('blur-filter');
            $('#s4-workspace').after('<div id="loading-bar-spinner" class="spinner"><div class="spinner-icon"></div></div>');
            setTimeout(function () {
              var ajax = updateComment();
              ajax.done(function (data, xhr) {
                retrieveCommentSPA();
                $($('textarea[id^="txtComments"]')[0]).val('');
                $(".blur-filter").removeClass("blur-filter");
                $("#loading-bar-spinner").remove();

              }).fail(function (sender, args) {
                console.log(args);
              });
            }, 1000);
          } else {
            return false;
          }
        });

        $('input#btnReturn').click(function () { //button Save Report click event
          CommentFlagValue = false;
          var comments = $('#txtComments').val().trim();
          if (comments.length > 0) { //Add Confirmation Message
            var html = document.createElement('div');
            html.innerHTML = '<h2>Comments below will not be saved. If you wish to save the comment click Add Comment first otherwise continue. </h2>\
              <br>\
              <div class="ms-core-form-section"><div class="ms-core-form-bottomButtonBox"><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.OK); return false;">Continue</button><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.cancel); return false;">Cancel</button>\
              </div></div>'
            OpenPopUpPageWithDialogOptions({
              title: "Confirmation",
              html: html,
              dialogReturnValueCallback: function (dialogResult) {
                if (dialogResult == 1) {
                  SavingItemByStatus("CAP-For Revision");
                }
              }
            });
          } else {

            SavingItemByStatus("CAP-For Revision");
          }

        });

        $('input#btnCAPRelease').click(function () { //button Save Report click event
          CommentFlagValue = false;
          var ID = getQueryString("ID");
          var comments = $('#txtComments').val().trim();
          if (comments.length > 0) { //Add Confirmation Message
            var html = document.createElement('div');
            html.innerHTML = '<h2>Comments below will not be saved. If you wish to save the comment click Add Comment first otherwise continue. </h2>\
              <br>\
              <div class="ms-core-form-section"><div class="ms-core-form-bottomButtonBox"><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.OK); return false;">Continue</button><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.cancel); return false;">Cancel</button>\
              </div></div>'
            OpenPopUpPageWithDialogOptions({
              title: "Confirmation",
              html: html,
              dialogReturnValueCallback: function (dialogResult) {
                if (dialogResult == 1) {
                  SavingItemByStatus("CAP-For Implementation");
                }
              }
            });
          } else {
            SavingItemByStatus("CAP-For Implementation");
          }

        });
      } else {
        swapDivs('peoplePickerDiv2', 'peoplePickerDiv4');
        $('#peoplePickerDiv2').parent('div').css('display', 'none');
        $(".sp-peoplepicker-delImage").hide();
        $('#CAPDiv').css("display", 'block');
        $('#CommentPart').css('display', 'block');
        $('#NCAssigneeDiv').css('display', 'block');
        $('#RecepientDiv').css('display', 'none');
        $('input[type^="button"]').css("display", 'none');
        $('#CAPDiv').css("display", 'block');
        DisableForms("NCInformationDivMain", true);
      }
    }
    showCancelButton(_spPageContextInfo.userDisplayName);
  }

  function fnNCAssigned(Assignee, groups, ReportType) {
    var displayname = _spPageContextInfo.userDisplayName;
    var actor = getUserInfoMulti("peoplePickerDiv4");

    if (ReportType.toUpperCase() != "BRU") {
      if (Assignee == _spPageContextInfo.userDisplayName || groups.includes("Networks Administrator")) {
        $(".sp-peoplepicker-delImage").hide();
        $('#CAPDiv').css("display", 'block');

        $('#txtCAP').change(function () {
          var renderItems = true;
          var inputs = $(this);
          var sFileName;
          var _validFileExtensions = ValidFiles;

          FileLoop:
          for (var i = 0; i < $(this).get(0).files.length; i++) {
            var blnValid = false;
            sFileName = $(this).get(0).files[i].name;
            Extension:
            for (var j = 0; j < _validFileExtensions.length; j++) {
              var sCurExtension = _validFileExtensions[j];
              var sFileExtension = getFileExtension3(sFileName)
              if (sFileExtension.toLowerCase() === sCurExtension.toLowerCase()) {
                blnValid = true;
              }
            }

            if (!blnValid) {
              alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
              inputs.replaceWith(inputs.val('').clone(true));
              renderItems = false;
              break FileLoop;
            }

          }

          if (renderItems) {

            var layout = "";
            $(this).next('div').find('div').find('div[id^="NewdummyLink"]').remove();
            for (var i = 0; i < $(this).get(0).files.length; i++) {
              var fileName = $(this).get(0).files[i].name;
              var link = Application["SiteName"] + "/CAP/" + $('input[id^="txtNCNumber"]').val() + "/" + fileName;
              layout += '<div id="NewdummyLink">';
              layout += '<span class="ms-newdocument-iconouter"><img class="ms-newdocument-icon" src="/_layouts/15/images/spcommon.png?rev=44#ThemeKey=" alt="new" title="new"></span><span><a target="_blank" href="' + link + '"><img class="ms-asset-icon ms-rtePosition-4" src="' + getIcon(fileName) + '">' + fileName + '</a></span>';
              layout += '</div>';
            }
            //$(this).next('div').find('div').append(layout);
            $(this).next('div').find('div[id="Attachments"]').prepend(layout);
          }

          function getFileExtension3(filename) {
            return filename.slice((filename.lastIndexOf(".") - 1 >>> 0) + 1);
          }

        });

        $('#txtRAC').change(function () {
          var layout = "";
          var renderItems = true;
          var inputs = $(this);
          var sFileName;
          var _validFileExtensions = ValidFiles;

          FileLoop:
          for (var i = 0; i < $(this).get(0).files.length; i++) {
            var blnValid = false;
            sFileName = $(this).get(0).files[i].name;
            Extension:
            for (var j = 0; j < _validFileExtensions.length; j++) {
              var sCurExtension = _validFileExtensions[j];
              var sFileExtension = getFileExtension3(sFileName)
              if (sFileExtension.toLowerCase() === sCurExtension.toLowerCase()) {
                blnValid = true;
              }
            }

            if (!blnValid) {
              alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
              inputs.replaceWith(inputs.val('').clone(true));
              renderItems = false;
              break FileLoop;
            }

          }

          if (renderItems) {
            var layout = "";
            $(this).next('div').find('div').find('div[id^="NewdummyLink"]').remove();
            for (var i = 0; i < $(this).get(0).files.length; i++) {
              var fileName = $(this).get(0).files[i].name;
              var link = Application["SiteName"] + "/RCA/" + $('input[id^="txtNCNumber"]').val() + "/" + fileName;
              layout += '<div id="NewdummyLink">';
              layout += '<span class="ms-newdocument-iconouter"><img class="ms-newdocument-icon" src="/_layouts/15/images/spcommon.png?rev=44#ThemeKey=" alt="new" title="new"></span><span><a target="_blank" a href="' + link + '"><img class="ms-asset-icon ms-rtePosition-4" src="' + getIcon(fileName) + '">' + fileName + '</a></span>';
              layout += '</div>';
            }
            $(this).next('div').find('div[id="Attachments"]').prepend(layout);
          }

          function getFileExtension3(filename) {
            return filename.slice((filename.lastIndexOf(".") - 1 >>> 0) + 1);
          }

        });

        addRemoveAttachmentsButton($('#txtRAC'));
        addRemoveAttachmentsButton($('#txtCAP'));
        DisableForms("NCInformationDiv", true);
        DisableForms("NCReport", true);
        DisableForms("peoplePickerDiv4", false);
        $('#NCAssigneeDiv').css('display', 'block');
        $('#RecepientDiv').css('display', 'block');
        $('#CommentInput').css('display', 'block');
        $('#btnAddReport').css('display', 'none');
        $('#CommentPart').css('display', 'block');
        $('#btnDeleteReport').css('display', 'none');
        $('#btnSaveDraft').css('display', 'none');
        $('#btnSaveReport').css('display', 'none');

        $('#btnSaveReport').after('<a class="tooltipbutton expand" data-title="Save Report and send later"><input class="ms-Button ms-Button--primary" id="btnSaveReport" type="button" value="Save as Draft" style="font-size: small; font-family: inherit; color: white; font-weight: 600; background-color: #4b3832; border-color: #4b3832; border-radius: 5px;width:99px;font-size:11px;float: right;"></a>');
        $('#btnSaveReport').after('<div class="tooltiparealeft expand" data-title="Submits Corrective/Preventive Action Plan to Manager/BRU Head for review"><input class="ms-Button ms-Button--primary" id="btnSubmitCap" type="button" value="Submit CAP" style="font-size: small; font-family: inherit; color: white; font-weight: 600; background-color: #4b3832; border-color: #4b3832; border-radius: 5px;width:99px;font-size:10px;float: right;"></div>');
        $('input#btnAddComment').click(function () { //button Save Report click event
          var validation = {
            Fields: {
              txtComments: {
                required: true
              }
            },
            FieldType: {
              txtComments: {
                type: "textarea"
              }
            },
            message: {
              txtComments: {
                required: "This field is required",
              }
            }

          }
          if (Utility.Validate(validation)) {
            $('#s4-bodyContainer').addClass('blur-filter');
            $('#s4-workspace').after('<div id="loading-bar-spinner" class="spinner"><div class="spinner-icon"></div></div>');
            setTimeout(function () {
              var ajax = updateComment();
              ajax.done(function (data, xhr) {
                retrieveCommentSPA();
                $($('textarea[id^="txtComments"]')[0]).val('');
                $(".blur-filter").removeClass("blur-filter");
                $("#loading-bar-spinner").remove();

              }).fail(function (sender, args) {
                console.log(args);
              });
            }, 1000);
          } else {
            return false;
          }

        });
        $('input#btnSaveReport').click(function () { //button Save Report click event
          CommentFlagValue = true;
          var ID = getQueryString("ID");
          var comments = $('#txtComments').val().trim();
          if (comments.length > 0) { //Add Confirmation Message
            var html = document.createElement('div');
            html.innerHTML = '<h2>Comments below will not be saved. If you wish to save the comment click Add Comment first otherwise continue. </h2>\
              <br>\
              <div class="ms-core-form-section"><div class="ms-core-form-bottomButtonBox"><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.OK); return false;">Continue</button><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.cancel); return false;">Cancel</button>\
              </div></div>'
            OpenPopUpPageWithDialogOptions({
              title: "Confirmation",
              html: html,
              dialogReturnValueCallback: function (dialogResult) {
                if (dialogResult == 1) {
                  UpdateCorrectiveActionPlan2("NC-Assigned");
                }
              }
            });
          } else {
            UpdateCorrectiveActionPlan2("NC-Assigned");
          }

        });

        $('input#btnSubmitCap').click(function () { //button Save Report click event
          CommentFlagValue = false;
          var ID = getQueryString("ID");
          var comments = $('#txtComments').val().trim();
          if (comments.length > 0) { //Add Confirmation Message
            var html = document.createElement('div');
            html.innerHTML = '<h2>Comments below will not be saved. If you wish to save the comment click Add Comment first otherwise continue. </h2>\
              <br>\
              <div class="ms-core-form-section"><div class="ms-core-form-bottomButtonBox"><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.OK); return false;">Continue</button><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.cancel); return false;">Cancel</button>\
              </div></div>'
            OpenPopUpPageWithDialogOptions({
              title: "Confirmation",
              html: html,
              dialogReturnValueCallback: function (dialogResult) {
                if (dialogResult == 1) {
                  UpdateCorrectiveActionPlan("CAP-For Review");
                }
              }
            });
          } else {
            UpdateCorrectiveActionPlan("CAP-For Review");
          }

        });
      } else {
        $(".sp-peoplepicker-delImage").hide();
        $('#NCAssigneeDiv').css('display', 'block');
        $('#RecepientDiv').css('display', 'block');
        $('#CommentPart').css('display', 'block');
        $('input[type^="button"]').css("display", 'none');
        DisableForms("NCInformationDivMain", true);
      }
    } else {
      if (Assignee == _spPageContextInfo.userDisplayName || groups.includes("Networks Administrator")) {
        swapDivs('peoplePickerDiv2', 'peoplePickerDiv4');
        $('#peoplePickerDiv2').parent('div').css('display', 'none');
        $(".sp-peoplepicker-delImage").hide();
        $('#RecepientDiv').css('display', 'none');
        $('#CAPDiv').css("display", 'block');
        $('#txtCAP').change(function () {
          var renderItems = true;
          var inputs = $(this);
          var sFileName;
          var _validFileExtensions = ValidFiles;

          FileLoop:
          for (var i = 0; i < $(this).get(0).files.length; i++) {
            var blnValid = false;
            sFileName = $(this).get(0).files[i].name;
            Extension:
            for (var j = 0; j < _validFileExtensions.length; j++) {
              var sCurExtension = _validFileExtensions[j];
              var sFileExtension = getFileExtension3(sFileName)
              if (sFileExtension.toLowerCase() === sCurExtension.toLowerCase()) {
                blnValid = true;
              }
            }

            if (!blnValid) {
              alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
              inputs.replaceWith(inputs.val('').clone(true));
              renderItems = false;
              break FileLoop;
            }

          }

          if (renderItems) {

            var layout = "";
            $(this).next('div').find('div').find('div[id^="NewdummyLink"]').remove();
            for (var i = 0; i < $(this).get(0).files.length; i++) {
              var fileName = $(this).get(0).files[i].name;
              var link = Application["SiteName"] + "/CAP/" + $('input[id^="txtNCNumber"]').val() + "/" + fileName;
              layout += '<div id="NewdummyLink">';
              layout += '<span class="ms-newdocument-iconouter"><img class="ms-newdocument-icon" src="/_layouts/15/images/spcommon.png?rev=44#ThemeKey=" alt="new" title="new"></span><span><a target="_blank" href="' + link + '"><img class="ms-asset-icon ms-rtePosition-4" src="' + getIcon(fileName) + '">' + fileName + '</a></span>';
              layout += '</div>';
            }
            //$(this).next('div').find('div').append(layout);
            $(this).next('div').find('div[id="Attachments"]').prepend(layout);
          }

          function getFileExtension3(filename) {
            return filename.slice((filename.lastIndexOf(".") - 1 >>> 0) + 1);
          }

        });

        $('#txtRAC').change(function () {
          var layout = "";
          var renderItems = true;
          var inputs = $(this);
          var sFileName;
          var _validFileExtensions = ValidFiles;

          FileLoop:
          for (var i = 0; i < $(this).get(0).files.length; i++) {
            var blnValid = false;
            sFileName = $(this).get(0).files[i].name;
            Extension:
            for (var j = 0; j < _validFileExtensions.length; j++) {
              var sCurExtension = _validFileExtensions[j];
              var sFileExtension = getFileExtension3(sFileName)
              if (sFileExtension.toLowerCase() === sCurExtension.toLowerCase()) {
                blnValid = true;
              }
            }

            if (!blnValid) {
              alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
              inputs.replaceWith(inputs.val('').clone(true));
              renderItems = false;
              break FileLoop;
            }

          }

          if (renderItems) {
            var layout = "";
            $(this).next('div').find('div').find('div[id^="NewdummyLink"]').remove();
            for (var i = 0; i < $(this).get(0).files.length; i++) {
              var fileName = $(this).get(0).files[i].name;
              var link = Application["SiteName"] + "/RCA/" + $('input[id^="txtNCNumber"]').val() + "/" + fileName;
              layout += '<div id="NewdummyLink">';
              layout += '<span class="ms-newdocument-iconouter"><img class="ms-newdocument-icon" src="/_layouts/15/images/spcommon.png?rev=44#ThemeKey=" alt="new" title="new"></span><span><a target="_blank" a href="' + link + '"><img class="ms-asset-icon ms-rtePosition-4" src="' + getIcon(fileName) + '">' + fileName + '</a></span>';
              layout += '</div>';
            }
            $(this).next('div').find('div[id="Attachments"]').prepend(layout);
          }

          function getFileExtension3(filename) {
            return filename.slice((filename.lastIndexOf(".") - 1 >>> 0) + 1);
          }

        });

        addRemoveAttachmentsButton($('#txtRAC'));
        addRemoveAttachmentsButton($('#txtCAP'));

        DisableForms("NCInformationDiv", true);
        DisableForms("NCReport", true);
        DisableForms("peoplePickerDiv4", false);
        $('#NCAssigneeDiv').css('display', 'block');
        $('#RecepientDiv').css('display', 'none');
        $('#CommentInput').css('display', 'block');
        $('#btnAddReport').css('display', 'none');
        $('#CommentPart').css('display', 'block');
        $('#btnDeleteReport').css('display', 'none');
        $('#btnSaveDraft').css('display', 'none');
        $('#btnSaveReport').css('display', 'none');

        $('#btnSaveReport').after('<a class="tooltipbutton expand" data-title="Save report and send later"><input class="ms-Button ms-Button--primary" id="btnSaveReport" type="button" value="Save as Draft" style="font-size: small; font-family: inherit; color: white; font-weight: 600; background-color: #4b3832; border-color: #4b3832; border-radius: 5px;width:99px;font-size:11px;float: right;"></a>');
        $('#btnSaveReport').after('<div class="tooltiparealeft expand" data-title="Submits Corrective/Preventive Action Plan to Auditor for review"><input class="ms-Button ms-Button--primary" id="btnSubmitCap" type="button" value="Submit CAP" style="font-size: small; font-family: inherit; color: white; font-weight: 600; background-color: #4b3832; border-color: #4b3832; border-radius: 5px;width:99px;font-size:10px;float: right;"></div>');
        $('input#btnAddComment').click(function () { //button Save Report click event
          var validation = {
            Fields: {
              txtComments: {
                required: true
              }
            },
            FieldType: {
              txtComments: {
                type: "textarea"
              }
            },
            message: {
              txtComments: {
                required: "This field is required",
              }
            }

          }
          if (Utility.Validate(validation)) {
            $('#s4-bodyContainer').addClass('blur-filter');
            $('#s4-workspace').after('<div id="loading-bar-spinner" class="spinner"><div class="spinner-icon"></div></div>');
            setTimeout(function () {
              var ajax = updateComment();
              ajax.done(function (data, xhr) {
                retrieveCommentSPA();
                $($('textarea[id^="txtComments"]')[0]).val('');
                $(".blur-filter").removeClass("blur-filter");
                $("#loading-bar-spinner").remove();

              }).fail(function (sender, args) {
                console.log(args);
              });
            }, 1000);
          } else {
            return false;
          }
        });
        $('input#btnSaveReport').click(function () { //button Save Report click event
          CommentFlagValue = true;
          var ID = getQueryString("ID");
          var comments = $('#txtComments').val().trim();
          if (comments.length > 0) { //Add Confirmation Message
            var html = document.createElement('div');
            html.innerHTML = '<h2>Comments below will not be saved. If you wish to save the comment click Add Comment first otherwise continue. </h2>\
              <br>\
              <div class="ms-core-form-section"><div class="ms-core-form-bottomButtonBox"><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.OK); return false;">Continue</button><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.cancel); return false;">Cancel</button>\
              </div></div>'
            OpenPopUpPageWithDialogOptions({
              title: "Confirmation",
              html: html,
              dialogReturnValueCallback: function (dialogResult) {
                if (dialogResult == 1) {
                  UpdateCorrectiveActionPlan2("NC-Assigned");
                }
              }
            });
          } else {
            UpdateCorrectiveActionPlan2("NC-Assigned");
          }

        });
        $('input#btnSubmitCap').click(function () { //button Save Report click event
          CommentFlagValue = false;
          var ID = getQueryString("ID");
          var comments = $('#txtComments').val().trim();
          if (comments.length > 0) { //Add Confirmation Message
            var html = document.createElement('div');
            html.innerHTML = '<h2>Comments below will not be saved. If you wish to save the comment click Add Comment first otherwise continue. </h2>\
              <br>\
              <div class="ms-core-form-section"><div class="ms-core-form-bottomButtonBox"><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.OK); return false;">Continue</button><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.cancel); return false;">Cancel</button>\
              </div></div>'
            OpenPopUpPageWithDialogOptions({
              title: "Confirmation",
              html: html,
              dialogReturnValueCallback: function (dialogResult) {
                if (dialogResult == 1) {
                  UpdateCorrectiveActionPlan("CAP-For Approval");
                }
              }
            });
          } else {
            UpdateCorrectiveActionPlan("CAP-For Approval");
          }

        });
      } else {
        swapDivs('peoplePickerDiv2', 'peoplePickerDiv4');
        $('#peoplePickerDiv2').parent('div').css('display', 'none');
        $(".sp-peoplepicker-delImage").hide();
        $('#CommentPart').css('display', 'block');
        $('#NCAssigneeDiv').css('display', 'block');
        $('#RecepientDiv').css('display', 'none');
        $('input[type^="button"]').css("display", 'none');
        DisableForms("NCInformationDivMain", true);
        // UnableToView("You don't have Permission to View the Item", link);
      }
    }

    showCancelButton(_spPageContextInfo.userDisplayName);

  }

  function fnNCChallenged(groups) {
    var displayname = _spPageContextInfo.userDisplayName;
    var actor = getUserInfoMulti("peoplePickerDiv2");

    if (actor.includes(displayname) || groups.includes("Networks Administrator")) {
      $(".sp-peoplepicker-delImage").hide();
      DisableForms("NCInformationDiv", true);
      DisableForms("peoplePickerDiv4", false);
      DisableForms("NCReport", true);
      $('#NCAssigneeDiv').css('display', 'none');
      $('#RecepientDiv').css('display', 'block');
      $('#CommentInput').css('display', 'block');
      $('#btnAddReport').css('display', 'none');
      $('#CommentPart').css('display', 'block');
      $('#btnDeleteReport').css('display', 'none');
      $('#btnSaveDraft').css('display', 'none');
      $('#btnSaveReport').css('display', 'none');

      $('#btnSaveReport').after('<a class="tooltipbutton expand" data-title="Notifies recipient that challenge was not accepted"><input class="ms-Button ms-Button--primary" id="btnNotValid" type="button" value="Not Valid" style="font-size: small; font-family: inherit; color: white; font-weight: 600; background-color: #4b3832; border-color: #4b3832; border-radius: 5px;width:99px;font-size:11px;float: right;"></a>');
      $('#btnSaveReport').after('<a class="tooltipbutton expand" data-title="Marks the report as cancelled"><input class="ms-Button ms-Button--primary" id="btnCloseReport" type="button" value="Cancel Report" style="font-size: small; font-family: inherit; color: white; font-weight: 600; background-color: #4b3832; border-color: #4b3832; border-radius: 5px;width:99px;font-size:11px;float: right;"></a>');
      $('input#btnAddComment').click(function () { //button Save Report click event
        var validation = {
          Fields: {
            txtComments: {
              required: true
            }
          },
          FieldType: {
            txtComments: {
              type: "textarea"
            }
          },
          message: {
            txtComments: {
              required: "This field is required",
            }
          }

        }
        if (Utility.Validate(validation)) {
          $('#s4-bodyContainer').addClass('blur-filter');
          $('#s4-workspace').after('<div id="loading-bar-spinner" class="spinner"><div class="spinner-icon"></div></div>');
          setTimeout(function () {
            var ajax = updateComment();
            ajax.done(function (data, xhr) {
              retrieveCommentSPA();
              $($('textarea[id^="txtComments"]')[0]).val('');
              $(".blur-filter").removeClass("blur-filter");
              $("#loading-bar-spinner").remove();

            }).fail(function (sender, args) {
              console.log(args);
            });
          }, 1000);
        } else {
          return false;
        }

      });
      $('input#btnNotValid').click(function () { //button Save Report click event
        CommentFlagValue = false;
        var ID = getQueryString("ID");
        var comments = $('#txtComments').val().trim();
        if (comments.length > 0) { //Add Confirmation Message
          var html = document.createElement('div');
          html.innerHTML = '<h2>Comments below will not be saved. If you wish to save the comment click Add Comment first otherwise continue. </h2>\
            <br>\
            <div class="ms-core-form-section"><div class="ms-core-form-bottomButtonBox"><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.OK); return false;">Continue</button><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.cancel); return false;">Cancel</button>\
            </div></div>'
          OpenPopUpPageWithDialogOptions({
            title: "Confirmation",
            html: html,
            dialogReturnValueCallback: function (dialogResult) {
              if (dialogResult == 1) {
                SavingItemByStatus('NC-Issued');
              }
            }
          });
        } else {
          SavingItemByStatus('NC-Issued');
        }


      });
      $('input#btnCloseReport').click(function () { //button Save Report click event
        CommentFlagValue = false;
        var ID = getQueryString("ID");
        var comments = $('#txtComments').val().trim();
        if (comments.length > 0) { //Add Confirmation Message
          var html = document.createElement('div');
          html.innerHTML = '<h2>Comments below will not be saved. If you wish to save the comment click Add Comment first otherwise continue. </h2>\
            <br>\
            <div class="ms-core-form-section"><div class="ms-core-form-bottomButtonBox"><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.OK); return false;">Continue</button><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.cancel); return false;">Cancel</button>\
            </div></div>'
          OpenPopUpPageWithDialogOptions({
            title: "Confirmation",
            html: html,
            dialogReturnValueCallback: function (dialogResult) {
              if (dialogResult == 1) {
                SavingItemByStatus('Cancelled');
              }
            }
          });
        } else {
          SavingItemByStatus('Cancelled');
        }
      });
    }
    else {
      $(".sp-peoplepicker-delImage").hide();
      $('#NCAssigneeDiv').css('display', 'none');
      $('#RecepientDiv').css('display', 'block');
      $('input[type^="button"]').css("display", 'none');
      DisableForms("NCInformationDivMain", true);
      //UnableToView("You don't have Permission to View the Item", link);
    }

    showCancelButton(displayname);

  }

  function fnNCIssued(groups) {
    var aging = $('input[id^="txtReminder"]').val(); //if reminder is set to yes. hide challenge button+
    var emailCC = getUserInfoMulti("peopleCC");
    var displayname = _spPageContextInfo.userDisplayName;
    var gLevel = getUserInfoMulti("peoplePickerDiv3");
    if (gLevel.includes(displayname) || groups.includes("Networks Administrator") || emailCC.includes(displayname)) {
      $(".sp-peoplepicker-delImage").hide();
      DisableForms("NCInformationDiv", true);
      DisableForms("peoplePickerDiv4", false);
      DisableForms("NCReport", true);
      $('#RecepientDiv').css('display', 'block');
      $('#NCAssigneeDiv').css('display', 'block');
      $('#CommentInput').css('display', 'block');
      $('#btnAddReport').css('display', 'none');
      $('#CommentPart').css('display', 'block');
      $('#btnDeleteReport').css('display', 'none');
      $('#btnSaveDraft').css('display', 'none');
      $('#btnSaveReport').css('display', 'none');

      if (aging.toUpperCase() == "TRUE") {

        $('#btnSaveReport').after('<a class="tooltipbutton expand" data-title="Contest to Report has already expired"><input class="ms-Button ms-Button--primary" id="btnChallenge" type="button" value="Contest" style="font-size: small; font-family: inherit; color: white; font-weight: 600; background-color: #4b383282; border-color: #4b383282; border-radius: 5px;width:99px;font-size:11px;float: right;" disabled></a>');
        $('#btnSaveReport').after('<a class="tooltipbutton expand" data-title="Sends the report to the assignee"><input class="ms-Button ms-Button--primary" id="btnAssignReport" type="button" value="Acknowledge" style="font-size: small; font-family: inherit; color: white; font-weight: 600; background-color: #4b3832; border-color: #4b3832; border-radius: 5px;width:99px;font-size:11px;float: right;"></a>');
        $('#btnSaveReport').after('<a class="tooltipbutton expand" data-title="Marks the report as cancelled"><input class="ms-Button ms-Button--primary" id="btnCloseReport" type="button" value="Cancel Report" style="font-size: small; font-family: inherit; color: #854442; font-weight: 600; background-color: white; border-color: #854442; border-radius: 5px;width:99px;font-size:11px;float: right;"></a>');

        $('input#btnAddComment').click(function () { //button Save Report click event
          var validation = {
            Fields: {
              txtComments: {
                required: true
              }
            },
            FieldType: {
              txtComments: {
                type: "textarea"
              }
            },
            message: {
              txtComments: {
                required: "This field is required",
              }
            }

          }
          if (Utility.Validate(validation)) {
            $('#s4-bodyContainer').addClass('blur-filter');
            $('#s4-workspace').after('<div id="loading-bar-spinner" class="spinner"><div class="spinner-icon"></div></div>');
            setTimeout(function () {
              var ajax = updateComment();
              ajax.done(function (data, xhr) {
                retrieveCommentSPA();
                $($('textarea[id^="txtComments"]')[0]).val('');
                $(".blur-filter").removeClass("blur-filter");
                $("#loading-bar-spinner").remove();

              }).fail(function (sender, args) {
                console.log(args);
              });
            }, 1000);
          } else {
            return false;
          }

        });
        $('input#btnAssignReport').click(function () { //button Save Report click event.
          CommentFlagValue = false;
          var comments = $('#txtComments').val().trim();
          if (comments.length > 0) { //Add Confirmation Message
            var html = document.createElement('div');
            html.innerHTML = '<h2>Comments below will not be saved. If you wish to save the comment click Add Comment first otherwise continue. </h2>\
            <br>\
            <div class="ms-core-form-section"><div class="ms-core-form-bottomButtonBox"><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.OK); return false;">Continue</button><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.cancel); return false;">Cancel</button>\
            </div></div>'
            OpenPopUpPageWithDialogOptions({
              title: "Confirmation",
              html: html,
              dialogReturnValueCallback: function (dialogResult) {
                if (dialogResult == 1) {
                  UpdateNCAssignee('NC-Assigned');
                }
              }
            });
          } else {
            UpdateNCAssignee('NC-Assigned');
          }

        });
        $('input#btnCloseReport').click(function () { //button Save Report click event
          var comments = $('#txtComments').val().trim();
          if (comments.length > 0) { //Add Confirmation Message
            var html = document.createElement('div');
            html.innerHTML = '<h2>Comments below will not be saved. If you wish to save the comment click Add Comment first otherwise continue. </h2>\
            <br>\
            <div class="ms-core-form-section"><div class="ms-core-form-bottomButtonBox"><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.OK); return false;">Continue</button><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.cancel); return false;">Cancel</button>\
            </div></div>'
            OpenPopUpPageWithDialogOptions({
              title: "Confirmation",
              html: html,
              dialogReturnValueCallback: function (dialogResult) {
                if (dialogResult == 1) {
                  SavingItemByStatus("Closed");
                }
              }
            });
          } else {
            SavingItemByStatus("Closed");
          }
        });
      } else {

        $('#btnSaveReport').after('<div class="tooltiparea expand" data-title="Notifies the Lead Auditor that report is not&#10;accepted/valid and request its cancellation.&#10;Note: Ensure to add a comment as justification of this request"><input class="ms-Button ms-Button--primary" id="btnChallenge" type="button" value="Contest" style="font-size: small; font-family: inherit; color: white; font-weight: 600; background-color: #4b3832; border-color: #4b3832; border-radius: 5px;width:99px;font-size:11px;float: right;"></div>');
        $('#btnSaveReport').after('<a class="tooltipbutton expand" data-title="Sends the report to the assignee"><input class="ms-Button ms-Button--primary" id="btnAssignReport" type="button" value="Acknowledge" style="font-size: small; font-family: inherit; color: white; font-weight: 600; background-color: #4b3832; border-color: #4b3832; border-radius: 5px;width:99px;font-size:11px;float: right;"></a>');

        $('input#btnChallenge').click(function () { //button Save Report click event
          var comments = $('#txtComments').val().trim();
          if (comments.length > 0) { //Add Confirmation Message
            var html = document.createElement('div');
            html.innerHTML = '<h2>Comments below will not be saved. If you wish to save the comment click Add Comment first otherwise continue. </h2>\
            <br>\
            <div class="ms-core-form-section"><div class="ms-core-form-bottomButtonBox"><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.OK); return false;">Continue</button><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.cancel); return false;">Cancel</button>\
            </div></div>'
            OpenPopUpPageWithDialogOptions({
              title: "Confirmation",
              html: html,
              dialogReturnValueCallback: function (dialogResult) {
                if (dialogResult == 1) {
                  SavingItemByStatus('NC-Challenged');
                }
              }
            });
          } else {
            SavingItemByStatus('NC-Challenged');
          }

        });
        $('input#btnAddComment').click(function () { //button Save Report click event
          var validation = {
            Fields: {
              txtComments: {
                required: true
              }
            },
            FieldType: {
              txtComments: {
                type: "textarea"
              }
            },
            message: {
              txtComments: {
                required: "This field is required",
              }
            }

          }
          if (Utility.Validate(validation)) {
            $('#s4-bodyContainer').addClass('blur-filter');
            $('#s4-workspace').after('<div id="loading-bar-spinner" class="spinner"><div class="spinner-icon"></div></div>');
            setTimeout(function () {
              var ajax = updateComment();
              ajax.done(function (data, xhr) {
                retrieveCommentSPA();
                $($('textarea[id^="txtComments"]')[0]).val('');
                $(".blur-filter").removeClass("blur-filter");
                $("#loading-bar-spinner").remove();

              }).fail(function (sender, args) {
                console.log(args);
              });
            }, 1000);
          } else {
            return false;
          }

        });
        $('input#btnAssignReport').click(function () { //button Save Report click event

          var comments = $('#txtComments').val().trim();
          if (comments.length > 0) { //Add Confirmation Message
            var html = document.createElement('div');
            html.innerHTML = '<h2>Comments below will not be saved. If you wish to save the comment click Add Comment first otherwise continue. </h2>\
            <br>\
            <div class="ms-core-form-section"><div class="ms-core-form-bottomButtonBox"><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.OK); return false;">Continue</button><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.cancel); return false;">Cancel</button>\
            </div></div>'
            OpenPopUpPageWithDialogOptions({
              title: "Confirmation",
              html: html,
              dialogReturnValueCallback: function (dialogResult) {
                if (dialogResult == 1) {
                  UpdateNCAssignee('NC-Assigned');
                }
              }
            });
          } else {
            UpdateNCAssignee('NC-Assigned');
          }
        });
      }

    } else {
      $(".sp-peoplepicker-delImage").hide();
      $('#RecepientDiv').css('display', 'block');
      $('#NCAssigneeDiv').css('display', 'block');
      DisableForms("NCInformationDivMain", true);
      $('input[type^="button"]').css("display", 'none');
      // UnableToView("You don't have Permission to View the Item", link);
    }
    showCancelButton(displayname);
  }

  function fnNCDraft(groups, ReportType) {

    var displayname = _spPageContextInfo.userDisplayName;
    var creator = getUserInfoMulti("peoplePickerDiv");

    if (ReportType.toUpperCase() != "BRU") {
      if (groups.includes("Networks Administrator")) {
        $(".sp-peoplepicker-delImage").hide();
        $('#CommentInput').css('display', 'none');
        $('#btnAddReport').css('display', 'none');
        $('#CommentPart').css('display', 'none');
        $('#btnDeleteReport').css('display', 'block');
        $('#btnSaveDraft').css('display', 'block');
        $('#btnSaveReport').css('display', 'block');
        $('#NCAssigneeDiv').css('display', 'none');
        $('#RecepientDiv').css('display', 'block');
        addRemoveAttachmentsButton($('#txtAttachments'));
      } else if (creator.includes(displayname)) {
        $(".sp-peoplepicker-delImage").hide();
        $('#CommentInput').css('display', 'none');
        $('#btnAddReport').css('display', 'none');
        $('#CommentPart').css('display', 'none');
        $('#btnDeleteReport').css('display', 'block');
        $('#btnSaveDraft').css('display', 'block');
        $('#btnSaveReport').css('display', 'block');

        $('#NCAssigneeDiv').css('display', 'none');
        $('#RecepientDiv').css('display', 'block');
        DisableForms("peoplePickerDiv", true);
        addRemoveAttachmentsButton($('#txtAttachments'));
      } else {
        $(".sp-peoplepicker-delImage").hide();
        DisableForms("NCInformationDivMain", true);
        $('input[type^="button"]').css("display", 'none');
        $('#NCAssigneeDiv').css('display', 'none');
        $('#RecepientDiv').css('display', 'block');
      }

    } else {
      if (creator.includes(displayname)) {
        swapDivs('peoplePickerDiv2', 'peoplePickerDiv4');
        $('#peoplePickerDiv2').parent('div').css('display', 'none');
        $(".sp-peoplepicker-delImage").hide();
        $('#RecepientDiv').css('display', 'none');
        $('#NCAssigneeDiv').css('display', 'block');
        $('#CommentInput').css('display', 'none');
        $('#btnAddReport').css('display', 'none');
        $('#CommentPart').css('display', 'none');
        $('#btnDeleteReport').css('display', 'block');
        $('#btnSaveDraft').css('display', 'none');
        $('#btnSaveReport').css('display', 'none');
        $('#NCAssigneeDiv').css("display", "block");
        $('#RecepientDiv').css('display', 'none');
        DisableForms("peoplePickerDiv", true);
        addRemoveAttachmentsButton($('#txtAttachments'));
        $('#btnSaveReport').val('Create Report');
        $('#btnSaveReport').after('<input class="ms-Button ms-Button--primary" id="btnChallenge" type="button" value="Save as Draft" style="font-size: small; font-family: inherit; color: #4b3832; font-weight: 600; background-color: white; border-color: #4b3832; border-radius: 5px;width:99px;font-size:11px;float: right;">');
        $('#btnSaveReport').after('<input class="ms-Button ms-Button--primary" id="btnAssignReport" type="button" value="Send to Assignee" style="font-size: 9px; white-space: normal;font-family: inherit; color: white; font-weight: 600; background-color: #4b3832; border-color: #4b3832; border-radius: 5px;width:99px;float: right;">');
        $('input#btnChallenge').click(function () { //button Save Report click event
          UpdateNCAssignee('NC-Draft');
        });
        $('input#btnAssignReport').click(function () { //button Save Report click event

          var comments = $('#txtComments').val().trim();
          if (comments.length > 0) { //Add Confirmation Message
            var html = document.createElement('div');
            html.innerHTML = '<h2>Comments below will not be saved. If you wish to save the comment click Add Comment first otherwise continue. </h2>\
            <br>\
            <div class="ms-core-form-section"><div class="ms-core-form-bottomButtonBox"><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.OK); return false;">Continue</button><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.cancel); return false;">Cancel</button>\
            </div></div>'
            OpenPopUpPageWithDialogOptions({
              title: "Confirmation",
              html: html,
              dialogReturnValueCallback: function (dialogResult) {
                if (dialogResult == 1) {
                  UpdateNCAssignee('NC-Assigned');
                }
              }
            });
          } else {
            UpdateNCAssignee('NC-Assigned');
          }
        });
      } else if (groups.includes("Networks Administrator")) {
        swapDivs('peoplePickerDiv2', 'peoplePickerDiv4');
        $('#peoplePickerDiv2').parent('div').css('display', 'none');
        $(".sp-peoplepicker-delImage").hide();
        $('#RecepientDiv').css('display', 'none');
        $('#NCAssigneeDiv').css('display', 'block');
        $('#CommentInput').css('display', 'none');
        $('#btnAddReport').css('display', 'none');
        $('#CommentPart').css('display', 'none');
        $('#btnDeleteReport').css('display', 'block');
        $('#btnSaveDraft').css('display', 'none');
        $('#btnSaveReport').css('display', 'none');
        $('#NCAssigneeDiv').css("display", "block");
        $('#RecepientDiv').css('display', 'none');
        addRemoveAttachmentsButton($('#txtAttachments'));
        $('#btnSaveReport').val('Create Report');
        $('#btnSaveReport').after('<input class="ms-Button ms-Button--primary" id="btnChallenge" type="button" value="Save as Draft" style="font-size: small; font-family: inherit; color: #4b3832; font-weight: 600; background-color: white; border-color: #4b3832; border-radius: 5px;width:99px;font-size:11px;float: right;">');
        $('#btnSaveReport').after('<input class="ms-Button ms-Button--primary" id="btnAssignReport" type="button" value=Send to Assignee" style="font-size: 9px; white-space: normal;font-family: inherit; color: white; font-weight: 600; background-color: #4b3832; border-color: #4b3832; border-radius: 5px;width:99px;float: right;">');
        $('input#btnChallenge').click(function () { //button Save Report click event
          UpdateNCAssignee('NC-Draft');
        });
        $('input#btnAssignReport').click(function () { //button Save Report click event
          var comments = $('#txtComments').val().trim();
          if (comments.length > 0) { //Add Confirmation Message
            var html = document.createElement('div');
            html.innerHTML = '<h2>Comments below will not be saved. If you wish to save the comment click Add Comment first otherwise continue. </h2>\
            <br>\
            <div class="ms-core-form-section"><div class="ms-core-form-bottomButtonBox"><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.OK); return false;">Continue</button><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.cancel); return false;">Cancel</button>\
            </div></div>'
            OpenPopUpPageWithDialogOptions({
              title: "Confirmation",
              html: html,
              dialogReturnValueCallback: function (dialogResult) {
                if (dialogResult == 1) {
                  UpdateNCAssignee('NC-Assigned');
                }
              }
            });
          } else {
            UpdateNCAssignee('NC-Assigned');
          }

        });
      } else {
        swapDivs('peoplePickerDiv2', 'peoplePickerDiv4');
        $('#peoplePickerDiv2').parent('div').css('display', 'none');
        $(".sp-peoplepicker-delImage").hide();
        $('#NCAssigneeDiv').css("display", "block");
        $('#RecepientDiv').css('display', 'none');
        DisableForms("NCInformationDivMain", true);
        $('input[type^="button"]').css("display", 'none');
        //UnableToView("You don't have Permission to View the Item", link);
      }
    }
  }

  function fnNCForReview(groups) {
    var displayname = _spPageContextInfo.userDisplayName;
    var actor = getUserInfoMulti("peoplePickerDiv2");

    if (actor.includes(displayname) || groups.includes("Networks Administrator")) {
      $(".sp-peoplepicker-delImage").hide();
      DisableForms("NCInformationDiv", true);
      DisableForms("NCReport", true);
      $('#CommentInput').css('display', 'block');
      $('#btnAddReport').css('display', 'none');
      $('#CommentPart').css('display', 'block');
      $('#btnDeleteReport').css('display', 'none');
      $('#btnSaveDraft').css('display', 'none');
      $('#btnSaveReport').css('display', 'none');
      $('#NCAssigneeDiv').css("display", "none");
      $('#RecepientDiv').css('display', 'block');
      $('#btnSaveReport').after('<a class="tooltipbutton expand" data-title="Returns report to Auditor for revision"><input class="ms-Button ms-Button--primary" id="btnReturnToCreator" type="button" value="Return NC Report" style="font-size: 10px; width:10em;font-family: inherit; color: white; font-weight: 600; background-color: #4b3832; border-color: #4b3832; border-radius: 5px;width:103px;font-size:10px;float: right; " ></a>');
      $('#btnSaveReport').after('<a class="tooltipbutton expand" data-title="Sends the report to recipients"><input class="ms-Button ms-Button--primary" id="btnIssueReport" type="button" value="Issue Report" style="font-size: 10px; width:10em; font-family: inherit; color: white; font-weight: 600; background-color: #4b3832; border-color: #4b3832; border-radius: 5px;width:99px;font-size:11px;float: right; " ></a>');

      $('input#btnReturnToCreator').click(function () { //button Save Report click event
        CommentFlagValue = false;

        var comments = $('#txtComments').val().trim();
        if (comments.length > 0) { //Add Confirmation Message
          var html = document.createElement('div');
          html.innerHTML = '<h2>Comments below will not be saved. If you wish to save the comment click Add Comment first otherwise continue. </h2>\
          <br>\
          <div class="ms-core-form-section"><div class="ms-core-form-bottomButtonBox"><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.OK); return false;">Continue</button><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.cancel); return false;">Cancel</button>\
          </div></div>'
          OpenPopUpPageWithDialogOptions({
            title: "Confirmation",
            html: html,
            dialogReturnValueCallback: function (dialogResult) {
              if (dialogResult == 1) {
                SavingItemByStatus('NC-For Revision');
              }
            }
          });
        } else {
          SavingItemByStatus('NC-For Revision');
        }
      });

      $('input#btnAddComment').click(function () { //button Save Report click event
        var validation = {
          Fields: {
            txtComments: {
              required: true
            }
          },
          FieldType: {
            txtComments: {
              type: "textarea"
            }
          },
          message: {
            txtComments: {
              required: "This field is required",
            }
          }

        }
        if (Utility.Validate(validation)) {
          $('#s4-bodyContainer').addClass('blur-filter');
          $('#s4-workspace').after('<div id="loading-bar-spinner" class="spinner"><div class="spinner-icon"></div></div>');
          setTimeout(function () {
            var ajax = updateComment();
            ajax.done(function (data, xhr) {
              retrieveCommentSPA();
              $($('textarea[id^="txtComments"]')[0]).val('');
              $(".blur-filter").removeClass("blur-filter");
              $("#loading-bar-spinner").remove();

            }).fail(function (sender, args) {
              console.log(args);
            });
          }, 1000);
        } else {
          return false;
        }

      });

      $('input#btnIssueReport').click(function () { //button Save Report click event
        CommentFlagValue = false;
        var ID = getQueryString("ID");
        var comments = $('#txtComments').val().trim();
        if (comments.length > 0) { //Add Confirmation Message
          var html = document.createElement('div');
          html.innerHTML = '<h2>Comments below will not be saved. If you wish to save the comment click Add Comment first otherwise continue. </h2>\
            <br>\
            <div class="ms-core-form-section"><div class="ms-core-form-bottomButtonBox"><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.OK); return false;">Continue</button><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.cancel); return false;">Cancel</button>\
            </div></div>'
          OpenPopUpPageWithDialogOptions({
            title: "Confirmation",
            html: html,
            dialogReturnValueCallback: function (dialogResult) {
              if (dialogResult == 1) {
                SavingItemByStatus('NC-Issued');
              }
            }
          });
        } else {
          SavingItemByStatus('NC-Issued');
        }

      });
    }
    else {
      DisableForms("NCInformationDivMain", true);
      $(".sp-peoplepicker-delImage").hide();
      $('#NCAssigneeDiv').css("display", "none");
      $('#RecepientDiv').css('display', 'block');
      $('input[type^="button"]').css("display", 'none');
    }
    showCancelButton(displayname);
  }

  function fnNCForRevision(groups) {
    var displayname = _spPageContextInfo.userDisplayName;
    var actor = getUserInfoMulti("peoplePickerDiv");
    if (actor.includes(displayname) || groups.includes("Networks Administrator")) {
      $(".sp-peoplepicker-delImage").hide();
      $('#CommentInput').css('display', 'block');
      $('#btnAddReport').css('display', 'none');
      $('#CommentPart').css('display', 'block');
      $('#btnDeleteReport').css('display', 'none');
      $('#btnSaveDraft').css('display', 'none');
      $('#btnSaveReport').css('display', 'none');
      $('#NCAssigneeDiv').css("display", "none");
      $('#RecepientDiv').css('display', 'block');
      addRemoveAttachmentsButton($('#txtAttachments'));
      //$('#btnSaveReport').after('<input class="ms-Button ms-Button--primary" id="btnAddComment" type="button"   style="font-size: 11px; width: 9em; font-family: inherit; color: #4b3832; font-weight: 600; background-color: white; border-color: #4b3832; border-radius: 5px;float: right;">');
      $('#btnSaveReport').after('<a class="tooltipbutton expand" data-title="Save report and issue Later"><input class="ms-Button ms-Button--primary" id="btnSaveReport" type="button" value="Save as Draft" style="font-size: 11px; width:9em; font-family: inherit; color: white; font-weight: 600; background-color: #4b3832; border-color: #4b3832; border-radius: 5px;float: right; " ></a>');
      $('#btnSaveReport').after('<a class="tooltipbutton expand" data-title="Sends the report to Lead Auditor"><input class="ms-Button ms-Button--primary" id="btnUpdateReport" type="button" value="Update Report" style="font-size: 11px; width:9em; font-family: inherit; color: white; font-weight: 600; background-color: #4b3832; border-color: #4b3832; border-radius: 5px;float: right; " ></a>');

      $('input#btnCloseReport').click(function () { //button Save Report click event
        var comments = $('#txtComments').val().trim();
        if (comments.length > 0) { //Add Confirmation Message
          var html = document.createElement('div');
          html.innerHTML = '<h2>Comments below will not be saved. If you wish to save the comment click Add Comment first otherwise continue. </h2>\
          <br>\
          <div class="ms-core-form-section"><div class="ms-core-form-bottomButtonBox"><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.OK); return false;">Continue</button><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.cancel); return false;">Cancel</button>\
          </div></div>'
          OpenPopUpPageWithDialogOptions({
            title: "Confirmation",
            html: html,
            dialogReturnValueCallback: function (dialogResult) {
              if (dialogResult == 1) {
                SavingItemByStatus("Closed");
              }
            }
          });
        } else {
          SavingItemByStatus("Closed");
        }
      });
      $('input#btnAddComment').click(function () { //button Save Report click event
        var validation = {
          Fields: {
            txtComments: {
              required: true
            }
          },
          FieldType: {
            txtComments: {
              type: "textarea"
            }
          },
          message: {
            txtComments: {
              required: "This field is required",
            }
          }

        }
        if (Utility.Validate(validation)) {
          $('#s4-bodyContainer').addClass('blur-filter');
          $('#s4-workspace').after('<div id="loading-bar-spinner" class="spinner"><div class="spinner-icon"></div></div>');
          setTimeout(function () {
            var ajax = updateComment();
            ajax.done(function (data, xhr) {
              retrieveCommentSPA();
              $($('textarea[id^="txtComments"]')[0]).val('');
              $(".blur-filter").removeClass("blur-filter");
              $("#loading-bar-spinner").remove();

            }).fail(function (sender, args) {
              console.log(args);
            });
          }, 1000);
        } else {
          return false;
        }

      });

      $('input#btnSaveReport').click(function () { //button Save Report click event
        CommentFlagValue = true;

        var comments = $('#txtComments').val().trim();
        if (comments.length > 0) { //Add Confirmation Message
          var html = document.createElement('div');
          html.innerHTML = '<h2>Comments below will not be saved. If you wish to save the comment click Add Comment first otherwise continue. </h2>\
          <br>\
          <div class="ms-core-form-section"><div class="ms-core-form-bottomButtonBox"><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.OK); return false;">Continue</button><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.cancel); return false;">Cancel</button>\
          </div></div>'
          OpenPopUpPageWithDialogOptions({
            title: "Confirmation",
            html: html,
            dialogReturnValueCallback: function (dialogResult) {
              if (dialogResult == 1) {
                SavingItemByStatus('NC-For Revision');
              }
            }
          });
        } else {
          SavingItemByStatus('NC-For Revision');
        }
      });

      $('input#btnUpdateReport').click(function () { //button Save Report click event
        CommentFlagValue = false;

        var comments = $('#txtComments').val().trim();
        if (comments.length > 0) { //Add Confirmation Message
          var html = document.createElement('div');
          html.innerHTML = '<h2>Comments below will not be saved. If you wish to save the comment click Add Comment first otherwise continue. </h2>\
          <br>\
          <div class="ms-core-form-section"><div class="ms-core-form-bottomButtonBox"><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.OK); return false;">Continue</button><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.cancel); return false;">Cancel</button>\
          </div></div>'
          OpenPopUpPageWithDialogOptions({
            title: "Confirmation",
            html: html,
            dialogReturnValueCallback: function (dialogResult) {
              if (dialogResult == 1) {
                SavingItemByStatus('NC-For Review');
              }
            }
          });
        } else {
          SavingItemByStatus('NC-For Review');
        }
      });
    } else {
      DisableForms("NCInformationDivMain", true);
      $(".sp-peoplepicker-delImage").hide();
      $('#NCAssigneeDiv').css("display", "none");
      $('#RecepientDiv').css('display', 'block');
      $('input[type^="button"]').css("display", 'none');
    }
    showCancelButton(displayname);
  }

  function DisableForms(DivId, isDisabled) {
    $("#" + DivId + " :input").attr("disabled", isDisabled);
    if (isDisabled) {
      $("#" + DivId).find("textarea").attr('readonly', 'readonly');
      $("#" + DivId).find("textarea").attr("disabled", false);
      $("#" + DivId).find("textarea").css("background-color", "#f4f4f4")
    }

  }

  function DisablePicker(PickerId, isDisabled) {
    if (isDisabled) {
      $("#" + DivId).css("pointer-events", "none");
      $("#" + DivId + " :input").attr("disabled", isDisabled);
    } else {
      $("#" + DivId + " :input").attr("disabled", isDisabled);
    }
  }


  function retrieveToPeoplePicker(peoplePickerId, name) {
    var form = $("div[id='" + peoplePickerId + "']");
    var userField = form.find("input[id$='" + peoplePickerId + "_TopSpan_EditorInput']").get(0);
    var peoplepicker = SPClientPeoplePicker.PickerObjectFromSubElement(userField);
    peoplepicker.AddUserKeys(name);
  }

  function getQueryString(key) {
    var regex = new RegExp('[\\?&amp;]' + key + '=([^&amp;#]*)');
    var qs = regex.exec(window.location.href);
    return qs[1];
  }

  function formatDate(date) {
    var date = new Date(date);
    var mm = (date.getMonth() + 1);
    mm = (mm < 10) ? '0' + mm : mm;
    var dd = date.getDate();
    dd = (dd < 10) ? '0' + dd : dd;
    return mm + '/' + dd + '/' + date.getFullYear();
  }

  callback(true);
}


function getFileExists(fileUrl, complete, error) {
  var ctx = SP.ClientContext.get_current();
  var file = ctx.get_web().getFileByServerRelativeUrl(fileUrl);
  ctx.load(file);
  ctx.executeQueryAsync(function () {
    complete(true);
  },
    function (sender, args) {
      if (args.get_errorTypeName() === "System.IO.FileNotFoundException") {
        complete(false);
      } else {
        error(args);
      }
    });
}


function createFolder(parentFolder, subFolder) {
  var clientContext;
  var oWebsite;
  var oWebsite1;
  var oList;
  var itemCreateInfo;
  var parentFolderURL;
  var url;
  var folder;


  clientContext = new SP.ClientContext.get_current();
  oWebsite = clientContext.get_web();
  oList = oWebsite.get_lists().getByTitle(parentFolder);
  url = clientContext.get_url() + "/" + parentFolder + "/"

  //check folder if doesn't exist will error if not existing
  folder = clientContext.get_web().getFolderByServerRelativeUrl(url);

  //change context to  url of clientid
  oWebsite1 = clientContext.get_web(url);

  itemCreateInfo = new SP.ListItemCreationInformation();
  itemCreateInfo.set_underlyingObjectType(SP.FileSystemObjectType.folder);
  itemCreateInfo.set_leafName(subFolder);
  this.oListItem = oList.addItem(itemCreateInfo);
  this.oListItem.update();

  clientContext.load(this.oListItem);
  clientContext.executeQueryAsync(
    Function.createDelegate(this, successHandler),
    Function.createDelegate(this, errorHandler)
  );


  function successHandler() {
    console.log('success: ' + url);
  }

  function errorHandler(msg, args) {
    console.log('fail: ' + url + " " + args.get_message());
  }
}


function getIcon(file) {
  switch (file.replace(/^.*\./, '')) {
    case "xlsx":
    case "xlsm":
    case "xltx":
    case "xltm":
    case "xls":
    case "xlt":
    case "xlm":
      return "/_layouts/15/images/icxls.png";
    case "doc":
    case "dot":
    case "wbk":
    case "docx":
    case "docm":
    case "dotx":
    case "dotm":
    case "docb":
      return "/_layouts/15/images/icdocx.png";
    case "ppt":
    case "pot":
    case "pps":
    case "pptx":
    case "pptm":
    case "potx":
    case "potm":
    case "ppam":
    case "ppsx":
    case "ppsm":
    case "sldx":
    case "sldm":
      return "/_layouts/15/images/icpptx.png";
    case "pdf":
      return "/_layouts/15/images/icpdf.png";
    case "png":
    case "jpeg":
    case "jpg":
    case "png":
    case "gif":
      return "/_layouts/15/images/icjpg.gif";
    default:
      fileTypeError = true;
      return "/_layouts/15/images/icgen.gif";

  }
}


var oListItem;
var index;
var fileUploaderCounter = 0;
var fileAttachmentCounter = 0;
var fileAttachmentUpload = 0;
var isFinish = 0;
var TempReportStatus = "";;

function TagForAuditCreate(itemList) { //save items to list copied from util
  var clientContext = new SP.ClientContext.get_current();
  var oList = clientContext.get_web().get_lists().getById(Application["List"]["NCReport"]);
  for (var i = 0; i < itemList.length; i++) {
    TempReportStatus = itemList[i]["Status"];
    index = i;
    var oListItem = oList.getItemById(itemList[i]["ID"]);
    for (var column in itemList[i]) {
      if (column.toUpperCase() != "ID") {
        oListItem.set_item(column, itemList[i][column]);
      }
    }
    oListItem.update();
    clientContext.load(oListItem);

  }
  clientContext.executeQueryAsync(onQuerySucceeded, onQueryFailed);

  function onQuerySucceeded() {
    var folder = $('#txtReportId').val();
    var fileAttachmentCounterAudit = 0;
    var fileAttachmentUploadAudit = 0;
    var fileArray = [];


    for (var i = 0; i < $("input#txtAuditFile").length; i++) { // upload files to the folder
      fileArray = $("input#txtAuditFile")[i].files;
      if (fileArray != undefined || file != null) {
        for (var j = 0; j < fileArray.length; j++) {
          fileAttachmentCounterAudit++; //get the number of attachments;
        }
      }
    }

    console.log("Number of attachments: " + fileAttachmentCounterAudit);

    if (fileAttachmentCounterAudit != 0) {
      createFolder("AuditFile", $('#txtNCNumber').val()); //create a new folder for the new item 
      for (var myIndex = 0; myIndex < $("input[id^='txtAuditFile']").length; myIndex++) {
        fileArray = $("input[id^='txtAuditFile']")[myIndex].files;
        reportid = $($('input[id^="txtNCNumber"]')[myIndex]).val()
        if (fileArray != undefined || file != null) {
          for (var j = 0; j < fileArray.length; j++) {
            $pnp.sp.web.getFolderByServerRelativeUrl(Application["SiteName"] + "/AuditFile/" + $('#txtNCNumber').val() + "/").files.add(fileArray[j].name, fileArray[j], true).then((result) => {
              result.file.listItemAllFields.get().then((listItemAllFields) => {
                fileAttachmentUploadAudit++;
                if (fileAttachmentUploadAudit == fileAttachmentCounterAudit) {
                  console.log("Next Page . . .");

                  if (TempReportStatus.toUpperCase() == "NC-CHALLENGED") {
                    setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["IQAEmail"]), 2000);
                    setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["BRUEmail"]), 2000);
                  } else if (TempReportStatus.toUpperCase() == "NC-ASSIGNED") {
                    setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["IQAEmail"]), 2000);
                    setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["BRUEmail"]), 2000);
                  } else if (TempReportStatus.toUpperCase() == "CAP-FOR REVISION") {
                    setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["IQAEmail"]), 2000);
                    setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["BRUEmail"]), 2000);
                  } else if (TempReportStatus.toUpperCase() == "CAP-FOR REVIEW") {
                    setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["IQAEmail"]), 2000);
                    setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["BRUEmail"]), 2000);
                  } else if (TempReportStatus.toUpperCase() == "CAP-FOR AUDITORS ASSESSMENT") {
                    setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["IQAEmail"]), 2000);
                    setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["BRUEmail"]), 2000);
                  } else if (TempReportStatus.toUpperCase() == "CAP-FOR APPROVAL") {
                    setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["IQAEmail"]), 2000);
                    setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["BRUEmail"]), 2000);
                  } else if (TempReportStatus.toUpperCase() == "CAP-FOR IMPLEMENTATION") {
                    setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["IQAEmail"]), 2000);
                    setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["BRUEmail"]), 2000);
                  } else if (TempReportStatus.toUpperCase() == "CAP-FOR FINAL EVALUATION") {
                    setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["IQAEmail"]), 2000);
                    setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["BRUEmail"]), 2000);
                  } else if (TempReportStatus.toUpperCase() == "CAP-RESOLVED") {
                    setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["IQAEmail"]), 2000);
                    setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["BRUEmail"]), 2000);
                  }

                  setTimeout(function () {

                    if ($('#txtReportType').val().toUpperCase() == "BRU") {
                      window.location = BRUAllItemLink;
                    } else {
                      window.location = IQAAllItemLink;
                    }
                  }, 2000);
                } else {
                  console.log("Continue . . .");
                }
              });
            });

          }
        }
      }

    } else {
      if (TempReportStatus.toUpperCase() == "NC-CHALLENGED") {
        setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["IQAEmail"]), 2000);
        setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["BRUEmail"]), 2000);
      } else if (TempReportStatus.toUpperCase() == "NC-ASSIGNED") {
        setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["IQAEmail"]), 2000);
        setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["BRUEmail"]), 2000);
      } else if (TempReportStatus.toUpperCase() == "CAP-FOR REVIEW") {
        setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["IQAEmail"]), 2000);
        setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["BRUEmail"]), 2000);
      } else if (TempReportStatus.toUpperCase() == "CAP-FOR REVISION") {
        setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["IQAEmail"]), 2000);
        setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["BRUEmail"]), 2000);
      } else if (TempReportStatus.toUpperCase() == "CAP-FOR AUDITORS ASSESSMENT") {
        setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["IQAEmail"]), 2000);
        setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["BRUEmail"]), 2000);
      } else if (TempReportStatus.toUpperCase() == "CAP-FOR APPROVAL") {
        setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["IQAEmail"]), 2000);
        setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["BRUEmail"]), 2000);
      } else if (TempReportStatus.toUpperCase() == "CAP-FOR IMPLEMENTATION") {
        setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["IQAEmail"]), 2000);
        setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["BRUEmail"]), 2000);
      } else if (TempReportStatus.toUpperCase() == "CAP-FOR FINAL EVALUATION") {
        setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["IQAEmail"]), 2000);
        setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["BRUEmail"]), 2000);
      } else if (TempReportStatus.toUpperCase() == "CAP-RESOLVED") {
        setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["IQAEmail"]), 2000);
        setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["BRUEmail"]), 2000);
      }
      setTimeout(function () {

        if ($('#txtReportType').val().toUpperCase() == "BRU") {
          window.location = BRUAllItemLink;
        } else {
          window.location = IQAAllItemLink;
        }
      }, 5000);
    }
  }

  function onQueryFailed(sender, args) {
    console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
  }

}

function createListItem(itemList) { //save items to list copied from util
  var clientContext = new SP.ClientContext.get_current();
  var oList = clientContext.get_web().get_lists().getById(Application["List"]["NCReport"]);
  for (var i = 0; i < itemList.length; i++) {
    TempReportStatus = itemList[i]["Status"];
    index = i;
    var oListItem = oList.getItemById(itemList[i]["ID"]);
    for (var column in itemList[i]) {
      if (column.toUpperCase() != "ID") {
        oListItem.set_item(column, itemList[i][column]);
      }
    }
    oListItem.update();
    clientContext.load(oListItem);

  }
  clientContext.executeQueryAsync(onQuerySucceeded, onQueryFailed);

  function onQuerySucceeded() {

    var folder = $('#txtReportId').val();

    var fileArray = [];

    if (fileAttachmentCounter <= 0) {
      for (var i = 0; i < $("input#txtAttachments").length; i++) { // upload files to the folder
        fileArray = $("input#txtAttachments")[i].files;
        if (fileArray != undefined || file != null) {
          for (var j = 0; j < fileArray.length; j++) {
            fileAttachmentCounter++; //get the number of attachments;
          }
        }
      }
    }


    console.log("Number of attachments: " + fileAttachmentCounter);

    if (fileAttachmentCounter != 0) {
      createFolder("Attachments", $('#txtReportId').val()); //create a new folder for the new item 
      for (var myIndex = 0; myIndex < $("input[id^='txtAttachments']").length; myIndex++) {
        fileArray = $("input[id^='txtAttachments']")[myIndex].files;
        reportid = $($('input[id^="txtNCNumber"]')[myIndex]).val()
        if (fileArray != undefined || file != null) {
          for (var j = 0; j < fileArray.length; j++) {
            $pnp.sp.web.getFolderByServerRelativeUrl(Application["SiteName"] + "/Attachments/" + $('#txtReportId').val() + "/").files.add(fileArray[j].name, fileArray[j], true).then((result) => {
              result.file.listItemAllFields.get().then((listItemAllFields) => {
                fileAttachmentUpload++;
                if (fileAttachmentUpload == fileAttachmentCounter) {
                  console.log("Next Page . . .");
                  if (TempReportStatus.toUpperCase() == "NC-CHALLENGED") {
                    setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["IQAEmail"]), 2000);
                    setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["BRUEmail"]), 2000);
                  } else if (TempReportStatus.toUpperCase() == "CANCELLED") {
                    setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["IQAEmail"]), 2000);
                    setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["BRUEmail"]), 2000);
                  } else if (TempReportStatus.toUpperCase() == "NC-FOR REVIEW") {
                    setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["IQAEmail"]), 2000);
                    setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["BRUEmail"]), 2000);
                  } else if (TempReportStatus.toUpperCase() == "NC-FOR REVISION") {
                    setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["IQAEmail"]), 2000);
                    setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["BRUEmail"]), 2000);
                  } else if (TempReportStatus.toUpperCase() == "NC-ISSUED") {
                    setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["IQAEmail"]), 2000);
                    setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["BRUEmail"]), 2000);
                  } else if (TempReportStatus.toUpperCase() == "NC-ASSIGNED") {
                    setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["IQAEmail"]), 2000);
                    setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["BRUEmail"]), 2000);
                  } else if (TempReportStatus.toUpperCase() == "CAP-FOR REVISION") {
                    setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["IQAEmail"]), 2000);
                    setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["BRUEmail"]), 2000);
                  } else if (TempReportStatus.toUpperCase() == "CAP-FOR REVIEW") {
                    setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["IQAEmail"]), 2000);
                    setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["BRUEmail"]), 2000);
                  } else if (TempReportStatus.toUpperCase() == "CAP-FOR AUDITORS ASSESSMENT") {
                    setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["IQAEmail"]), 2000);
                    setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["BRUEmail"]), 2000);
                  } else if (TempReportStatus.toUpperCase() == "CAP-FOR APPROVAL") {
                    setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["IQAEmail"]), 2000);
                    setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["BRUEmail"]), 2000);
                  } else if (TempReportStatus.toUpperCase() == "CAP-FOR IMPLEMENTATION") {
                    setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["IQAEmail"]), 2000);
                    setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["BRUEmail"]), 2000);
                  } else if (TempReportStatus.toUpperCase() == "CAP-FOR FINAL EVALUATION") {
                    setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["IQAEmail"]), 2000);
                    setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["BRUEmail"]), 2000);
                  } else if (TempReportStatus.toUpperCase() == "CAP-RESOLVED") {
                    setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["IQAEmail"]), 2000);
                    setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["BRUEmail"]), 2000);
                  } else if (TempReportStatus.toUpperCase() == "CLOSED") {
                    setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["IQAEmail"]), 2000);
                    setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["BRUEmail"]), 2000);
                  }
                  setTimeout(function () {

                    if ($('#txtReportType').val().toUpperCase() == "BRU") {
                      if (TempReportStatus.toUpperCase() != "NC-DRAFT") {
                        window.location = BRUAllItemLink;
                      } else {
                        window.location = BRUDraftLink;
                      }
                    } else {
                      if (TempReportStatus.toUpperCase() != "NC-DRAFT") {
                        window.location = IQAAllItemLink;
                      } else {
                        window.location = IQADraftLink;
                      }
                    }
                  }, 2000);
                } else {
                  console.log("Continue . . .");
                }
              });
            });

          }
        }
      }

    } else {
      if (TempReportStatus.toUpperCase() == "NC-CHALLENGED") {
        setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["IQAEmail"]), 2000);
        setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["BRUEmail"]), 2000);
      } else if (TempReportStatus.toUpperCase() == "CANCELLED") {
        setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["IQAEmail"]), 2000);
        setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["BRUEmail"]), 2000);
      } else if (TempReportStatus.toUpperCase() == "NC-FOR REVIEW") {
        setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["IQAEmail"]), 2000);
        setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["BRUEmail"]), 2000);
      } else if (TempReportStatus.toUpperCase() == "NC-FOR REVISION") {
        setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["IQAEmail"]), 2000);
        setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["BRUEmail"]), 2000);
      } else if (TempReportStatus.toUpperCase() == "NC-ISSUED") {
        setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["IQAEmail"]), 2000);
        setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["BRUEmail"]), 2000);
      } else if (TempReportStatus.toUpperCase() == "NC-ASSIGNED") {
        setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["IQAEmail"]), 2000);
        setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["BRUEmail"]), 2000);
      } else if (TempReportStatus.toUpperCase() == "CAP-FOR REVISION") {
        setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["IQAEmail"]), 2000);
        setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["BRUEmail"]), 2000);
      } else if (TempReportStatus.toUpperCase() == "CAP-FOR REVIEW") {
        setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["IQAEmail"]), 2000);
        setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["BRUEmail"]), 2000);
      } else if (TempReportStatus.toUpperCase() == "CAP-FOR AUDITORS ASSESSMENT") {
        setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["IQAEmail"]), 2000);
        setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["BRUEmail"]), 2000);
      } else if (TempReportStatus.toUpperCase() == "CAP-FOR APPROVAL") {
        setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["IQAEmail"]), 2000);
        setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["BRUEmail"]), 2000);
      } else if (TempReportStatus.toUpperCase() == "CAP-FOR IMPLEMENTATION") {
        setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["IQAEmail"]), 2000);
        setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["BRUEmail"]), 2000);
      } else if (TempReportStatus.toUpperCase() == "CAP-FOR FINAL EVALUATION") {
        setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["IQAEmail"]), 2000);
        setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["BRUEmail"]), 2000);
      } else if (TempReportStatus.toUpperCase() == "CAP-RESOLVED") {
        setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["IQAEmail"]), 2000);
        setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["BRUEmail"]), 2000);
      } else if (TempReportStatus.toUpperCase() == "CLOSED") {
        setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["IQAEmail"]), 2000);
        setTimeout(startWorkflow($('input[id^="txtCounterId"]').val(), Application["WorkFlow"]["BRUEmail"]), 2000);
      }
      setTimeout(function () {

        if ($('#txtReportType').val().toUpperCase() == "BRU") {
          if (TempReportStatus.toUpperCase() != "NC-DRAFT") {
            window.location = BRUAllItemLink;
          } else {
            window.location = BRUDraftLink;
          }
        } else {
          if (TempReportStatus.toUpperCase() != "NC-DRAFT") {
            window.location = IQAAllItemLink;
          } else {
            window.location = IQADraftLink;
          }
        }
      }, 3000);
    }
  }

  function onQueryFailed(sender, args) {
    console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
  }

}

function initializePeoplePicker2(peoplePickerElementId, GroupId) {
  var schema = {};
  schema['PrincipalAccountType'] = 'User,DL,SecGroup,SPGroup';
  schema['SearchPrincipalSource'] = 15;
  schema['ResolvePrincipalSource'] = 15;
  schema['AllowMultipleValues'] = true;
  schema['MaximumEntitySuggestions'] = 50;
  if (GroupId < 0) {
    schema['SharePointGroupID'] = GroupId;;
  }

  SPClientPeoplePicker_InitStandaloneControlWrapper(peoplePickerElementId, null, schema);
}

function initializePeoplePicker(peoplePickerElementId, GroupId) {
  var schema = {};
  schema['PrincipalAccountType'] = 'User,DL,SecGroup,SPGroup';
  schema['SearchPrincipalSource'] = 15;
  schema['ResolvePrincipalSource'] = 15;
  schema['AllowMultipleValues'] = false;
  schema['MaximumEntitySuggestions'] = 50;
  if (GroupId < 0) {
    schema['SharePointGroupID'] = GroupId;
  }

  SPClientPeoplePicker_InitStandaloneControlWrapper(peoplePickerElementId, null, schema);
}

function getUserInfoMulti(peoplePickerId) {
  var peoplePicker = this.SPClientPeoplePicker.SPClientPeoplePickerDict[peoplePickerId + '_TopSpan'];
  var users = peoplePicker.GetAllUserInfo();
  var userInfo = '';
  var name = "";
  var array = [];
  for (var i = 0; i < users.length; i++) {
    var user = users[i];
    name = user["DisplayText"];
    array.push(name);

  }
  return array;
}

function getUserInfo(peoplePickerId) {
  var peoplePicker = this.SPClientPeoplePicker.SPClientPeoplePickerDict[peoplePickerId + '_TopSpan'];
  var users = peoplePicker.GetAllUserInfo();
  var userInfo = '';
  var name = '';
  for (var i = 0; i < users.length; i++) {
    var user = users[i];
    for (var userProperty in user) {
      userInfo += userProperty + ':  ' + user[userProperty] + '<br>';
      name = user["DisplayText"];
    }
  }
  console.log("name: " + name);
  return name;
}

function SetCurrentUsernameToPeoplePicker() {
  var ctx = SP.ClientContext.get_current();
  var website = ctx.get_web();
  var currentUser = website.get_currentUser();
  ctx.load(currentUser);
  ctx.executeQueryAsync(function (sender, args) {
    var loginName = currentUser.get_loginName();
    var form = $("div[id='peoplePickerDiv']");

    var userField = form.find("input[id$='peoplePickerDiv_TopSpan_EditorInput']").get(0);
    var peoplepicker = SPClientPeoplePicker.PickerObjectFromSubElement(userField);
    peoplepicker.AddUserKeys(loginName);
  },
    function (sender, args) {
      console.log('request failed ' + args.get_message() + '\n' + args.get_stackTrace());
    });
}
var g_AllTerms;

function getCategoryTerms(txtInput) {
  var clientContext = SP.ClientContext.get_current();
  var taxonomySession = SP.Taxonomy.TaxonomySession.getTaxonomySession(clientContext);
  var termStores = taxonomySession.get_termStores();
  var termStore = taxonomySession.getDefaultSiteCollectionTermStore();
  //GUID of Term Set from which to get the Terms.  
  var termSet;

  termSet = termStore.getTermSet(Application["TermStore"]["Source"]["IQA"]);

  var terms = termSet.getAllTerms();
  clientContext.load(terms);
  clientContext.executeQueryAsync(function onSuccess() {
    var title = [];
    var txtTitle = [];
    var enumerator = terms.getEnumerator();
    while (enumerator.moveNext()) {
      var spTerm = enumerator.get_current();
      var items2 = spTerm.get_pathOfTerm().split(';');
      if (items2[1] != null && items2[2] != null) {
        txtTitle.push({
          "Label": items2[2],
          "category": items2[1]
        });
      }

    }

    var itemSorted = txtTitle.sort(custom_sort);

    var query = $('input[id^="txtSource"]').val().trim();

    function filterItems(query) {
      return itemSorted.filter(function (el) {
        return el["category"].toLowerCase().indexOf(query.toLowerCase()) > -1;
      })
    }

    itemSorted = filterItems(query);

    function custom_sort(a, b) {
      if (a.category < b.category) return -1;
      if (a.category > b.category) return 1;
      return 0;
    }

    InitializeInput(itemSorted, txtInput);

  }, function onFailure(args, msg) {
    console.log('Error: ' + msg.get_message());
  });
}



function getSourceTerms() {
  var clientContext = SP.ClientContext.get_current();
  var taxonomySession = SP.Taxonomy.TaxonomySession.getTaxonomySession(clientContext);
  var termStores = taxonomySession.get_termStores();
  var termStore = taxonomySession.getDefaultSiteCollectionTermStore();
  //GUID of Term Set from which to get the Terms.  
  var termSet = termStore.getTermSet(Application["TermStore"]["ClauseOfStandard"]);
  var terms = termSet.getAllTerms();
  clientContext.load(terms, 'Include(IsRoot, Labels, TermsCount, CustomSortOrder, Id, IsAvailableForTagging, Name, PathOfTerm, Parent, TermSet.Name)');
  clientContext.executeQueryAsync(function onSuccess() {

    var termsEnumerator = terms.getEnumerator(),
      tree = {
        term: terms,
        children: []
      };

    var data = [];
    while (termsEnumerator.moveNext()) {
      var currentTerm = termsEnumerator.get_current();
      var currentTermPath = currentTerm.get_pathOfTerm().split(';');
      var children = tree.children;

      for (var i = 0; i < currentTermPath.length; i++) {
        var foundNode = false;
        for (var j = 0; j < children.length; j++) {
          if (children[j].name === currentTermPath[i]) {
            foundNode = true;
            break;
          }
        }

        var term = foundNode ? children[j] : {
          name: currentTermPath[i],
          children: []
        };

        if (i === currentTermPath.length - 1) {
          term.term = currentTerm;
          term.title = currentTerm.get_name();
          term.guid = currentTerm.get_id().toString();
        }

        if (foundNode) {
          children = term.children;
        } else {
          children.push(term);

          if (i !== currentTermPath.length - 1) {
            children = term.children;
          }
        }
      }
    }


    var html = '';

    // term rendering
    for (var i = 0; i < tree.children.length; i++) {
      html += renderTerm(tree.children[i]);
    }

    // Append the create HTML to the bottom of the page
    var list = document.createElement('ul');
    list.setAttribute("id", "tree");
    list.innerHTML = html;

    $('input#txtNonconformance').after(list);

    function renderTerm(term) {

      var html = '<li><div>' + term.title + '</div>';

      if (term.children && term.children.length) {
        html += '<ul>';

        for (var i = 0; i < term.children.length; i++) {
          html += renderTerm2(term.children[i]);
        }

        html += '</ul>';
      }


      return html + '</li>';
    }

    function renderTerm2(term) {

      var html = '<li><div>' + term.title + '</div>';

      if (term.children && term.children.length) {
        html += '<ul>';

        for (var i = 0; i < term.children.length; i++) {
          html += renderTerm(term.children[i]);
        }

        html += '</ul>';
      }


      return html + '</li>';
    }

    // setTimeout(createTreeView(), 1000);
    setTimeout(createTreeView($("input[id^='txtNonconformance']")[0]), 1000);


  }, function onFailure(args, msg) {
    alert('Error: ' + msg.get_message());
  });

}

function getSingleTerms(termName, input) {
  var clientContext = SP.ClientContext.get_current();
  var taxonomySession = SP.Taxonomy.TaxonomySession.getTaxonomySession(clientContext);
  var termStores = taxonomySession.get_termStores();
  //Name of the Term Store from which to get the Terms.  
  // var termStore = termStores.getByName("Taxonomy_NXoiZ8+XvIL1gqXB/1MIJQ==");  
  var termStore = taxonomySession.getDefaultSiteCollectionTermStore();

  //GUID of Term Set from which to get the Terms.  
  var termSet = termStore.getTermSet(termName);
  var terms = termSet.getAllTerms();
  clientContext.load(terms);
  clientContext.executeQueryAsync(function onSuccess() {
    var title = [];
    var txtTitle = [];
    var enumerator = terms.getEnumerator();
    while (enumerator.moveNext()) {
      var spTerm = enumerator.get_current();
      var items2 = spTerm.get_pathOfTerm().split(';');
      if (items2[0] != null && items2[1] != null) {
        txtTitle.push({
          "Label": items2[1],
          "category": items2[0]
        });
      }
    }
    var itemSorted = txtTitle.sort(custom_sort);

    function custom_sort(a, b) {
      if (a.category < b.category) return -1;
      if (a.category > b.category) return 1;
      return 0;
    }

    InitializeInput(itemSorted, input);

  }, function onFailure(args, msg) {
    console.log('Error: ' + msg.get_message());
  });
}


function getTerms() {
  var clientContext = SP.ClientContext.get_current();
  var taxonomySession = SP.Taxonomy.TaxonomySession.getTaxonomySession(clientContext);
  var termStores = taxonomySession.get_termStores();
  var termStore = taxonomySession.getDefaultSiteCollectionTermStore();
  var termSet = termStore.getTermSet(Application["TermStore"]["AreasAudited"]);
  var terms = termSet.getAllTerms();
  clientContext.load(terms);
  clientContext.executeQueryAsync(function onSuccess() {
    var title = [];
    var txtTitle = [];
    var enumerator = terms.getEnumerator();
    while (enumerator.moveNext()) {
      var spTerm = enumerator.get_current();
      var items2 = spTerm.get_pathOfTerm().split(';');
      if (items2[0] != null && items2[1] != null) {
        txtTitle.push({
          "Label": items2[1],
          "category": items2[0]
        });
      }
    }
    var itemSorted = txtTitle.sort(custom_sort)

    function custom_sort(a, b) {
      if (a.category < b.category) return -1;
      if (a.category > b.category) return 1;
      return 0;
    }
    InitializeInput(itemSorted, "txtAreaAudited");

  }, function onFailure(args, msg) {
    alert('Error: ' + msg.get_message());
  });
}

function getSourceTerms2(Initiator) {
  var clientContext = SP.ClientContext.get_current();
  var taxonomySession = SP.Taxonomy.TaxonomySession.getTaxonomySession(clientContext);
  var termStores = taxonomySession.get_termStores();
  var termStore = taxonomySession.getDefaultSiteCollectionTermStore();
  //GUID of Term Set from which to get the Terms.  
  var termSet;
  if (Initiator.toUpperCase() == "BRU") {

    termSet = termStore.getTermSet(Application["TermStore"]["Source"]["BRU"]);
  } else {
    termSet = termStore.getTermSet(Application["TermStore"]["Source"]["IQA"]);
  }
  var terms = termSet.getAllTerms();
  clientContext.load(terms);
  clientContext.executeQueryAsync(function onSuccess() {
    var title = [];
    var txtTitle = [];
    var enumerator = terms.getEnumerator();
    while (enumerator.moveNext()) {
      var spTerm = enumerator.get_current();
      var items2 = spTerm.get_pathOfTerm().split(';');
      if (items2[0] != null && items2[1] != null && items2[2] == null) {
        txtTitle.push({
          "Label": items2[1],
          "category": items2[0]
        });
      }
    }


    // txtTitle.filter(onlyUnique);
    var itemSorted = txtTitle.sort(custom_sort)

    function custom_sort(a, b) {
      if (a.category < b.category) return -1;
      if (a.category > b.category) return 1;
      return 0;
    }


    InitializeInput(itemSorted, "txtSource");

  }, function onFailure(args, msg) {
    console.log('Error: ' + msg.get_message());
  });

}

function InitializeInput(data, inputId) {
  $(function () {
    $.widget("custom.catcomplete", $.ui.autocomplete, {
      _create: function () {
        this._super();
        this.widget().menu("option", "items", "> :not(.ui-autocomplete-category)");
      },
      _renderMenu: function (ul, items) {
        var that = this,
          currentCategory = "";
        $.each(items, function (index, item) {
          var li;
          if (item.category != currentCategory) {
            ul.append("<li class='ui-autocomplete-category'>" + item.category + "</li>");
            ul.addClass("ms-Dropdown-items");
            ul.addClass("width", "424px");
            currentCategory = item.category;
          }
          li = that._renderItemData(ul, item).addClass(item.category);
          if (item.category) {
            li.attr("aria-label", item.category + " : " + item.label);
            li.addClass("ms-Dropdown-item");
          }
        });
      }
    });
    var items = [];
    for (var i = 0; i < data.length; i++) {
      if (data[i]["Label"] != "") {
        items.push({
          label: data[i]["Label"],
          category: data[i]["category"]
        });
      }
    }
    if (inputId.toUpperCase() != "TXTAREAAUDITED" || inputId.toUpperCase() != "TXTAUDITOR" || inputId.toUpperCase() != "TXTRISKCATEGORY") {
      $("#" + inputId).catcomplete({
        minLength: 0,
        delay: 0,
        source: items,
        messages: {
          noResults: '',
          results: function () { }
        },
        change: function (ev, ui) {
          if (!ui.item) {
            $(this).val('');
          }
        }
      }).click(function () {
        $(this).catcomplete("search");
        $('.ui-autocomplete').css('max-width', $(this).width() + "px");
      });
    } else {
      $("#" + inputId).catcomplete({
        minLength: 0,
        delay: 0,
        source: items,
        messages: {
          noResults: '',
          results: function () { }
        }
      }).click(function () {
        $(this).catcomplete("search");
        $('.ui-autocomplete').css('max-width', $(this).width() + "px");
      });
    }
  });


}

function InitializeComponents() { //Initialize HTML

  /*Initialize fabric components*/

  if ($.fn.DatePicker) {
    $('.ms-DatePicker').DatePicker({
      format: 'mm/dd/yyyy'
    });
  }
  InitiateAllPeople();
  $('#btnDateCreatedToday').click();

}

function InitiateAllPeople() {
  var xmlhttp = new XMLHttpRequest();

  xmlhttp.open("GET", _spPageContextInfo.webAbsoluteUrl + '/_api/web/currentuser?$expand=Groups');

  xmlhttp.setRequestHeader("Accept", "application/json;odata=verbose");

  xmlhttp.onreadystatechange = function () {
    if (xmlhttp.readyState == XMLHttpRequest.DONE) {
      console.log("disabling ribbon function");
      if (xmlhttp.status == 200) {
        var response = xmlhttp.responseText;
        var jsonitem = JSON.parse(response);
        var isSiteAdmin = jsonitem["d"]["IsSiteAdmin"]
        var groups = jsonitem["d"]["Groups"]["results"]

        var isGroupAdmin = groups.some(function (g) {
          return g["Title"] == "Networks Administrator";
        })


        if (isSiteAdmin || isGroupAdmin) {
          initializePeoplePicker('peoplePickerDiv', -1);
          initializePeoplePicker('peoplePickerDiv2', -1);
          initializePeoplePicker('peoplePickerDiv3', -1);
          initializePeoplePicker('peoplePickerDiv4', -1);
          initializePeoplePicker2('peopleCC', -1);
          $('div[id $= "_TopSpan"]').addClass('ms-TextField-field');
        } else {
          initializePeoplePicker('peoplePickerDiv', 0);
          initializePeoplePicker('peoplePickerDiv2', 0);
          initializePeoplePicker('peoplePickerDiv3', 0);
          initializePeoplePicker('peoplePickerDiv4', 0);
          initializePeoplePicker2('peopleCC', -1);
          $('div[id $= "_TopSpan"]').addClass('ms-TextField-field');
        }

      } else {
        console.log('Error: ' + xmlhttp.statusText)
      }
    }
  }

  xmlhttp.send();
}

function InitializeComponentsEvents() { //add events to the elements
    
  
  $('#RelatedFindingsDiv > a')
  $("a#addCC").on("click", function () {
    $('div#divCC').toggle();
    $('#peopleCC').find('div > div[id $= "_TopSpan"]').removeClass('ms-TextField-field');
    $('#peopleCC').find('div > div[id $= "_TopSpan"]').addClass('ms-TextField--textFieldUnderlined ');
  });

  $('input[id^="txtSource"]').focusout(function () {
    for (var i = 0; i < $('input[id^="txtCategory"]').length; i++) {
      if (i == 0) {
        getCategoryTerms("txtCategory");
      } else {
        var inputid = "txtCategory_" + i;
        console.log(inputid);
        getCategoryTerms(inputid);
      }
    }

  });

  $('#txtFileComment').change(function () {
    var renderItems = true;
    var inputs = $(this);
    var sFileName;
    var _validFileExtensions = ValidFiles;
    FileLoop:
    for (var i = 0; i < $(this).get(0).files.length; i++) {
      var blnValid = false;
      sFileName = $(this).get(0).files[i].name;
      Extension:
      for (var j = 0; j < _validFileExtensions.length; j++) {
        var sCurExtension = _validFileExtensions[j];
        var sFileExtension = getFileExtension3(sFileName)
        if (sFileExtension.toLowerCase() === sCurExtension.toLowerCase()) {
          blnValid = true;
        }
      }

      if (!blnValid) {
        alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
        inputs.replaceWith(inputs.val('').clone(true));
        renderItems = false;
        break FileLoop;
      }

    }

    if (renderItems) {
      var layout = "";
      $('#txtComments').parent('div').siblings('div[id="temp"]').find('div[id^="NewdummyLink"]').remove();
      //$(this).siblings('div[id^="NewdummyLink"]').remove();

      for (var i = 0; i < $(this).get(0).files.length; i++) {
        var fileName = $(this).get(0).files[i].name;

        var link = Application["SiteName"] + "/CommentFiles/" + $('input[id^="txtNCNumber"]').val() + "/" + fileName;
        layout += '<div id="NewdummyLink">';
        layout += '<span class="ms-newdocument-iconouter"><img class="ms-newdocument-icon" src="/_layouts/15/images/spcommon.png?rev=44#ThemeKey=" alt="new" title="new"></span><span><a href="' + link + '" target="_blank"><img class="ms-asset-icon ms-rtePosition-4" src="' + getIcon(fileName) + '">' + fileName + '</a></span>';
        layout += '</div>';
      }
      //$(this).next('div').find('div[id="Attachments"]').prepend(layout);
      $('#txtComments').parent('div').siblings('div[id="temp"]').find('div[id="Attachments"]').prepend(layout);
    }

    function getFileExtension3(filename) {
      return filename.slice((filename.lastIndexOf(".") - 1 >>> 0) + 1);
    }

  });


  $('#txtAttachments').change(function () {
    var renderItems = true;
    var inputs = $(this);
    var sFileName;
    var _validFileExtensions = ValidFiles;
    FileLoop:
    for (var i = 0; i < $(this).get(0).files.length; i++) {
      var blnValid = false;
      sFileName = $(this).get(0).files[i].name;
      Extension:
      for (var j = 0; j < _validFileExtensions.length; j++) {
        var sCurExtension = _validFileExtensions[j];
        var sFileExtension = getFileExtension3(sFileName)
        if (sFileExtension.toLowerCase() === sCurExtension.toLowerCase()) {
          blnValid = true;
        }
      }

      if (!blnValid) {
        alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
        inputs.replaceWith(inputs.val('').clone(true));
        renderItems = false;
        break FileLoop;
      }

    }

    if (renderItems) {
      var layout = "";
      $(this).next('div').find('div').find('div[id^="NewdummyLink"]').remove();
      //$(this).siblings('div[id^="NewdummyLink"]').remove();

      for (var i = 0; i < $(this).get(0).files.length; i++) {
        var fileName = $(this).get(0).files[i].name;

        var link = Application["SiteName"] + "/Attachments/" + $('input[id^="txtReportId"]').val() + "/" + fileName;
        layout += '<div id="NewdummyLink">';
        layout += '<span class="ms-newdocument-iconouter"><img class="ms-newdocument-icon" src="/_layouts/15/images/spcommon.png?rev=44#ThemeKey=" alt="new" title="new"></span><span><a href="' + link + '" target="_blank"><img class="ms-asset-icon ms-rtePosition-4" src="' + getIcon(fileName) + '">' + fileName + '</a></span>';
        layout += '</div>';
      }
      //$(this).next('div').find('div[id="Attachments"]').prepend(layout);
      $(this).next('div').find('div[id="Attachments"]').prepend(layout);
    }

    function getFileExtension3(filename) {
      return filename.slice((filename.lastIndexOf(".") - 1 >>> 0) + 1);
    }


  });

  $('input#btnDeleteReport').click(function () {
    var html = document.createElement('div');
    html.innerHTML = '<h2>Are you sure you want to delete this item?</h2>\
        <br>\
        <div class="ms-core-form-section"><div class="ms-core-form-bottomButtonBox"><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.OK); return false;">Continue</button><button href="#" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.cancel); return false;">Cancel</button>\
        </div></div>'
    OpenPopUpPageWithDialogOptions({
      title: "Confirmation",
      html: html,
      dialogReturnValueCallback: function (dialogResult) {
        if (dialogResult == 1) {
          DeleteCurrentItem($('#txtCounterId').val());
        }
      }
    });

  });
  $('input#btnSaveDraft').click(function () { //button Draft click event
    SavingItemByStatus('NC-Draft');
  });

  $('input#btnSaveReport').click(function () { //button Save Report click event
    SavingItemByStatus('NC-For Review');
  });
}
$(document).ready(function () {
  InitializeComponents();
  InitializeComponentsEvents();
  retrieveItem(function (result) {
    if (result) {
      console.log("Callback retrieve: " + result);
      setTimeout(function () {
        autosize(document.getElementById("txtDescription"));
        autosize(document.getElementById("txtCAPComments"));
        autosize(document.getElementById("txtComments"));
        $('#txtCAPComments').height('100');
        $("textarea").each(function (textarea) {
          $(this).height(this.scrollHeight);
        });
      }, 2000);
    }
  });

});
