var Application;
(function (Application) {

  
  
  var AppConfig = {
    "SiteName": "/sites/nccardev/networks",
    "Series":{
      "Name": "NC",
      "Start": "000000"
    },
    "List": {
      "NCReport": "08140B32-ECA3-405B-9148-F758FBD31C7B",
      "BRUStatus": "99047A7A-50B4-4E78-B2BB-0A1E53A25078",
      "IQAStatus": "DC326F6D-AAFD-4B83-BD15-5D520FEB1219"
    },
    "Link":{
      "BRU": "https://meralco.sharepoint.com/sites/nccardev/networks/Lists/NCReport/BRUAllItems.aspx",
      "IQA": "https://meralco.sharepoint.com/sites/nccardev/networks/Lists/NCReport/IQAAllItems.aspx",
      "BRUDraft": "https://meralco.sharepoint.com/sites/nccardev/networks/Lists/NCReport/BRUDraft.aspx",
      "IQADraft": "https://meralco.sharepoint.com/sites/nccardev/networks/Lists/NCReport/IQADraft.aspx"
    },
    "TermStore":{
      "AreasAudited": "09b39306-65ec-4529-a6a5-0ce34f9f0fbf",
      "ClauseOfStandard": "6d0c9821-e1fb-4821-b838-c5ee6d542182",
      "Source": {
            "BRU": "948723bc-38c3-4159-a0e9-49acbf2af07d",
            "IQA": "c2b08dbb-67e0-46a3-b693-7edad84f6608"
        },
      "Category": "5a01622b-3e79-4d25-b606-9a39a43a9046",
      "Auditor": "2b3036e3-f084-431d-b435-b292485bfdf3",
      "RiskCategory": "4bb3693f-e34d-45ec-a034-414f80d1aeeb"
    },
    "WorkFlow":{

      "IQAEmail":"C150D4C5-EBF0-464C-A9F9-E30F458EBF38",
      "BRUEmail":"3BC00E36-2DD3-4091-B239-7B4C53B8A145"
    }
   
  }
  Application.AppConfig = AppConfig;
  
  var ValidFiles =  [".doc",".docx",".csv",".pdf",".jpg",".bmp",".xlsx",".xls", ".png"];  
  Application.ValidFiles = ValidFiles;

})(Application || (Application = {}));

