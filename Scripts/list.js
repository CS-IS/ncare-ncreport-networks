$(document).ready(function () {
    ExecuteOrDelayUntilScriptLoaded(registerRenderer, 'clienttemplates.js');
    var regexpTD = new RegExp("^\\s*<([a-zA-Z]*)(.|\\s)*/\\1?>\\s*$");
    $("td").ready(function(){
    
        TextToHTML(document.getElementsByTagName("TD"),regexpTD);
            
    });

    $('#CSRListViewControlDivWPQ2').find('span[id^="WPQ2_ListTitleViewSelectorMenu_Container"]').css('display', 'none');
    
    function registerRenderer() {
        var ctxForm = {};
        ctxForm.Templates = {}; 
        ctxForm.Templates = {
            Fields : {
                'Subject': {
                    View : function (ctx) {
                        var url = String.format('{0}?ID={1}', _spPageContextInfo.webAbsoluteUrl + "/Lists/NCReport/EditForm.aspx", ctx.CurrentItem.ID);
                        return String.format('<a href="{0}" onclick="EditItem2(event, \'{0}\');return false;">{1}</a>', url, ctx.CurrentItem.Subject);
                    }
                },
            }
        };
        SPClientTemplates.TemplateManager.RegisterTemplateOverrides(ctxForm);
    }
   

function TextToHTML(NodeSet, HTMLregexp) {

    var CellContent = "";
    var i=0;

    while (i < NodeSet.length){

    try {
            
        CellContent = NodeSet[i].innerText || NodeSet[i].textContent;
            
        if (HTMLregexp.test(CellContent)) {
                    
            NodeSet[i].innerHTML = CellContent;
            
        }
    
    } 

    catch(err){



    }

    i=i+1;

    }
}

}); 